

FROM scratch
FROM mhart/alpine-node:6.12.0

RUN npm install -g npm --prefix=/usr/local
RUN ln -s -f /usr/local/bin/npm /usr/bin/npm

CMD [ "/bin/sh" ]


# Override the base log level (info).
ENV NPM_CONFIG_LOGLEVEL warn



# Install all dependencies of the current project.
COPY package.json package.json
COPY semantic.json semantic.json

COPY npm-shrinkwrap.json npm-shrinkwrap.json
RUN npm install gulp-header --save-dev
RUN npm install --no-optional

# Copy all local files into the image.
COPY . .


# Build for production.
RUN npm run build --production


# Install and configure `serve`.
RUN npm install -g serve
CMD serve -s build
EXPOSE 5000
