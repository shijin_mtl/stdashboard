

const getInitialState = () => (
  {
    success: false,
    type:""
  }
)

function viralNewsReducer(state, action) {
  switch(action.type) {
    case 'SOURCE_REQUEST': {
      let data =action
      return Object.assign({}, state, {
        loading: true,
        type:"newspaper"
      })
    }
    case 'SOURCE_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        type:"newspaper",
        data
      })
    }
    case 'SOURCE_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        type:"newspaper",
        error,
      })
    }

    case 'ARTICLE_REQUEST': {
      let data =action
      return Object.assign({}, state, {
        loading: true,
        type:"article"
      })
    }
    case 'ARTICLE_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        type:"article",
        data
      })
    }
    case 'ARTICLE_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        type:"article",
        error,
      })
    }

    case 'MAIN_REQUEST': {
      let data =action
      return Object.assign({}, state, {
        loading: true,
        type:"main"
      })
    }
    case 'MAIN_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        type:"main",
        data
      })
    }
    case 'MAIN_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        type:"main",
        error,
      })
    }

    case 'VOLUME_REQUEST': {
      let data =action
      return Object.assign({}, state, {
        loading: true,
        type:"volume"
      })
    }
    case 'VOLUME_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        type:"volume",
        data
      })
    }
    case 'VOLUME_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        type:"volume",
        error,
      })
    }


        case 'S_VOLUME_REQUEST': {
          return Object.assign({}, state, {
            loading: true,
            type:"source_volume"
          })
        }
        case 'S_VOLUME_SUCCESS': {
          let data = action.data
          return Object.assign({}, state, {
            loading: false,
            success: true,
            type:"source_volume",
            data
          })
        }
        case 'S_VOLUME_FAILED': {
          const { error } = action
          return Object.assign({}, state, {
            loading: false,
            success: false,
            type:"source_volume",
            error,
          })
        }




    default:
      return getInitialState()
  }
}

export default viralNewsReducer
