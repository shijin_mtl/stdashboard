

const getInitialState = () => (
  {
    main_graph: false,
    pie_graph: '',
    password: '',
    error: '',
  }
)

function dataReducer(state, action) {
  switch(action.type) {
    case 'DATA_REQUEST': {
      let data =action
      return Object.assign({}, state, {
        loading: true,
        type:"total",
        data
      })
    }
    case 'DATA_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        type:"total",
        data
      })
    }
    case 'DATA_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        type:"total",
        error,
      })
    }



    case 'FB_REQUEST': {
      return Object.assign({}, state, {
        loading: true,
        type:"facebook"
      })
    }

    case 'FB_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        type:"facebook",
        data
      })
    }

    case 'FB_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        type:"facebook",
        error,
      })
    }


    case 'TW_REQUEST': {
      return Object.assign({}, state, {
        loading: true,
        type:"twitter"
      })
    }

    case 'TW_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        type:"twitter",
        data
      })
    }

    case 'TW_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        type:"twitter",
        error,
      })
    }


    case 'NEWS_REQUEST': {
      return Object.assign({}, state, {
        loading: true,
        type:"news"
      })
    }

    case 'NEWS_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        type:"news",
        data
      })
    }

    case 'NEWS_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        type:"news",
        error,
      })
    }


    default:
      return getInitialState()
  }
}

export default dataReducer
