// import {
//   LOGOUT,
// } from '../actions/logoutAction.js'



const getInitialState = () => (
  {
    success: false,
  }
)

function logoutReducer(state, action) {
  switch(action.type) {
    case 'LOGOUT_SUCCESS': {
      return Object.assign({}, state, {
        success: true,
      })
    }
    case 'LOGOUT_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        success: false,
        error,
      })
    }
    default:
      return getInitialState()
  }
}

export default logoutReducer
