

const getInitialState = () => (
  {
    success: false,
    username: '',
    password: '',
    error: '',
  }
)

function loginReducer(state, action) {
  switch(action.type) {
    case 'LOGIN_REQUEST': {
      let data =action
      return Object.assign({}, state, {
        loading: true,
        data
      })
    }
    case 'LOGIN_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        data
      })
    }
    case 'LOGIN_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        error,
      })
    }
    case 'GOOGLE_REQUEST': {
      let data =action
      return Object.assign({}, state, {
        loading: true,
        data
      })
    }
    case 'GOOGLE_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        data
      })
    }
    case 'GOOGLE_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        error,
      })
    }

    default:
      return getInitialState()
  }
}

export default loginReducer
