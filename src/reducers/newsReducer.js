

const getInitialState = () => (
  {
    success: false,
    username: '',
    password: '',
    error: '',
  }
)

function newsReducer(state, action) {
  switch(action.type) {
    case 'NEWS_REQUEST': {
      let data =action
      return Object.assign({}, state, {
        loading: true,
        type:"news",
        data
      })
    }
    case 'NEWS_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        type:"news",
        data
      })
    }
    case 'NEWS_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        type:"news",
        error,
      })
    }

      case 'ENTITY_REQUEST': {
        let data =action
        return Object.assign({}, state, {
          loading: true,
          type:"entity",
          data
        })
      }
      case 'ENTITY_SUCCESS': {
        let data = action.data
        return Object.assign({}, state, {
          loading: false,
          success: true,
          type:"entity",
          data
        })
      }
      case 'ENTITY_FAILED': {
        const { error } = action
        return Object.assign({}, state, {
          loading: false,
          success: false,
          type:"entity",
          error,
        })
      }


      case 'READ_REQUEST': {
        let data =action
        return Object.assign({}, state, {
          loading: true,
          type:"read",
          data
        })
      }
      
      case 'READ_SUCCESS': {
        let data = action.data
        return Object.assign({}, state, {
          loading: false,
          success: true,
          type:"read",
          data
        })
      }

      case 'READ_FAILED': {
        const { error } = action
        return Object.assign({}, state, {
          loading: false,
          success: false,
          type:"read",
          error,
        })
      }






    default:
      return getInitialState()
  }
}

export default newsReducer
