

const getInitialState = () => (
  {
    success: false,
  }
)

function compareReducer(state, action) {
  switch(action.type) {
    case 'SCORE_REQUEST': {
      let data =action
      return Object.assign({}, state, {
        loading: true,
        type:"score",
        data
      })
    }
    case 'SCORE_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        type:"score",
        data
      })
    }
    case 'SCORE_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        type:"score",
        error,
      })
    }

    default:
      return getInitialState()
  }
}

export default compareReducer
