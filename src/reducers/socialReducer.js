

const getInitialState = () => (
  {
    success: false,
    type:""
  }
)

function socialReducer(state, action) {
  switch(action.type) {
    case 'MAIN_REQUEST': {
      let data =action
      return Object.assign({}, state, {
        loading: true,
        success:false,
        type:"main",
        data
      })
    }
    case 'MAIN_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        type:"main",
        data
      })
    }
    case 'MAIN_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        type:"main",
        error,
      })
    }

    case 'DETAIL_REQUEST': {
      let data =action
      return Object.assign({}, state, {
        loading: true,
        success:false,
        type:"detail",
        data
      })
    }
    case 'DETAIL_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        type:"detail",
        data
      })
    }
    case 'DETAIL_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        type:"detail",
        error,
      })
    }

    case 'POST_REQUEST': {
      let data =action
      return Object.assign({}, state, {
        loading: true,
        success:false,
        type:"post",
        data
      })
    }
    case 'POST_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        type:"post",
        data
      })
    }
    case 'POST_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        type:"post",
        error,
      })
    }


    case 'SOCIAL_REQUEST': {
      let data =action
      return Object.assign({}, state, {
        loading: true,
        success:false,
        type:"social",
        data
      })
    }
    case 'SOCIAL_SUCCESS': {
      let data = action.data
      return Object.assign({}, state, {
        loading: false,
        success: true,
        type:"social",
        data
      })
    }
    case 'SOCIAL_FAILED': {
      const { error } = action
      return Object.assign({}, state, {
        loading: false,
        success: false,
        type:"social",
        error,
      })
    }




    default:
      return getInitialState()
  }
}

export default socialReducer
