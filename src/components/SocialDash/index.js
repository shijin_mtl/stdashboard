import React, { Component } from 'react'
import { Grid,Image,Icon,Segment,Button } from 'semantic-ui-react'
import LineCard from "./LineCard"
import BarCard from "./BarCard"
import Timeline from "./Timeline"
import { socialStore,mainFetch,detailFetch,postFetch } from '../../store/socialStore'
import { totalStore,totalFetch,fbFetch,twFetch } from '../../store/totalStore'


import fbImage from '../../public/img/fb.png'
import twImage from '../../public/img/tw2.png'



class SocialDash extends Component {
  constructor(props) {
    super(props)
    this.store = socialStore()
    this.storeTotal = totalStore()
    this.state = {
      loading: false,
      fb_main:[],
      tw_main:[],
      fb_detail:[],
      tw_detail:[],
      post:[],
      fb_latest:"",
      fb_rank_change:"",
      fb_score_change:"",
      tw_score_change:"",
      fb_list:[],
      tw_list:[],
      facebook:{
        likes:0,
        like_change:0,
        followers:0,
        followers_change:0,
        post:0,
        post_change:0,
        shares:0,
        share_change:0,
      },
      twitter:{
        likes:0,
        like_change:0,
        followers:0,
        followers_change:0,
        post:0,
        post_change:0,
        shares:0,
        share_change:0,
      },
      postShow:false,
      postText:"Show Posts"
    }
  }



  componentWillMount() {
    this.store.subscribe(this.onProcess.bind(this))
    this.storeTotal.subscribe(this.onTotalProcess.bind(this))
    this.store.dispatch(mainFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"type":'facebook'}))
    this.store.dispatch(mainFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"type":'twitter'}))
    this.store.dispatch(detailFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"type":'facebook'}))
    this.store.dispatch(detailFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"type":'twitter'}))
    this.store.dispatch(postFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"type":'twitter'}))
    this.storeTotal.dispatch(totalFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end}))
    this.storeTotal.dispatch(fbFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end}))
    this.storeTotal.dispatch(twFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end}))
  }

   changeFinder = (data) =>
      {var first = 0
      var last = 0
      var count = 0
      for (var key in data){
          if (count === 0)
              {
                first = data[key]
                count = 1
              }
          last = data[key]
        }
      var change = last - first
      return change
    }


  onTotalProcess=()=>{
    const state = this.storeTotal.getState()
    if(state.success){
      if(state.type==="facebook" && state.data.facebook !== undefined)
      {
          this.setState({
            facebook:{
              likes:state.data.facebook[0].facebook_post_likes.sum,
              like_change:this.changeFinder(state.data.facebook[0].facebook_post_likes.scores),
              followers:state.data.facebook[2].facebook_followers.sum,
              followers_change:this.changeFinder(state.data.facebook[2].facebook_followers.scores),
              post:state.data.facebook[3].facebook_post_number.sum,
              post_change:this.changeFinder(state.data.facebook[3].facebook_post_number.scores),
              shares:state.data.facebook[1].facebook_shares.sum,
              share_change:this.changeFinder(state.data.facebook[1].facebook_shares.scores),
            }
          })

      }

      else if (state.type==="twitter" && state.data.twitter !== undefined)
      {


        this.setState({
          twitter:{
            likes:state.data.twitter[0].twitter_favorites.sum,
            like_change:this.changeFinder(state.data.twitter[0].twitter_favorites.scores),
            followers:state.data.twitter[3].twitter_followers.sum,
            followers_change:this.changeFinder(state.data.twitter[3].twitter_followers.scores),
            post:state.data.twitter[1].twitter_tweets.sum,
            post_change:this.changeFinder(state.data.twitter[1].twitter_tweets.scores),
            shares:state.data.twitter[2].twitter_retweets.sum,
            share_change:this.changeFinder(state.data.twitter[2].twitter_retweets.scores),
          }
        })
      }


      else {
        var tw_score, fb_score
        if(state.data.facebook){
          var count = 0
          var tw_last = 0
          var fb_last = 0
          var tw_first = 0
          var fb_first = 0

          for (var key in state.data.facebook.ranks)
          {
                fb_score = state.data.facebook.scores[key]
                tw_score = state.data.twitter.scores[key]

                if(count === 0)
                {
                    tw_first = state.data.twitter.scores[key]
                    fb_first = state.data.facebook.scores[key]
                    count = 1
                }

                tw_last = state.data.twitter.scores[key]
                fb_last = state.data.facebook.scores[key]


          }



          this.setState({
            fb_latest:state.data.facebook.latest_rank,
            fb_rank_change:state.data.facebook.rank_change,
            tw_latest:state.data.twitter.latest_rank,
            tw_rank_change:state.data.twitter.rank_change,
            fb_score:fb_score,
            tw_score:tw_score,
            fb_score_change:fb_last - fb_first,
            tw_score_change:tw_last -tw_first,
            fb_list:state.data.facebook.ranks,
            tw_list:state.data.twitter.ranks,
          })
        }
      }
    }
  }


  componentDidMount() {

  }



  componentWillReceiveProps(nextProps)
  {
    this.setState({
      date:nextProps.date,
      })
      this.store.dispatch(mainFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"type":'facebook'}))
      this.store.dispatch(mainFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"type":'twitter'}))
      this.store.dispatch(detailFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"type":'facebook'}))
      this.store.dispatch(detailFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"type":'twitter'}))
      this.store.dispatch(postFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"type":'twitter'}))
      this.storeTotal.dispatch(totalFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end}))
      this.storeTotal.dispatch(fbFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end}))
      this.storeTotal.dispatch(twFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end}))
  }

  onProcess(response){
    const state = this.store.getState()
    if(state.success)
      {
        if(state.data.twitter){
          this.setState({
            tw_main:state.data.twitter,
          })
        }
        else if(state.data.facebook){
          this.setState({
            fb_main:state.data.facebook
          })
        }
        else if(state.type === "post")
        {
          this.setState({
            post:state.data.posts
          })
        }
        else {
          var detail_data = state.data.data
          if(detail_data.facebook_followers){
            this.setState({
              fb_detail:detail_data
            })
          }
          if(detail_data.twitter_followers){
            this.setState({
              tw_detail:detail_data
            })
          }
        }


      }
      }

      ShowPost=()=>{
        this.setState({
          postShow:!this.state.postShow,
          postText:this.state.postShow?"Show Posts":"Hide Posts"
        })
      }


  render() {

    var line1 = {"title":"Facebook Posts","data":this.state.fb_detail.facebook_post_number,"text":"Shows Facebook posts over selected period of time"}
    var line2 = {"title":"Facebook Shares","data":this.state.fb_detail.facebook_shares,"text":"Shows Facebook shares over selected period of time"}
    var line3 = {"title":"Facebook Likes","data":this.state.fb_detail.facebook_post_likes,"text":"Shows Facebook likes over selected period of time"}
    var line4 = {"title":"Twitter Posts","data":this.state.tw_detail.twitter_tweets,"text":"Shows Twitter tweets over selected period of time" }
    var line5 = {"title":"Twitter Retweets","data":this.state.tw_detail.twitter_retweets, "text":"Shows Twitter Retweets over selected period of time"}
    var line6 = {"title":"Twitter Favourites","data":this.state.tw_detail.twitter_favorites, "text":"Shows Twitter Favourites over selected period of time"}


    var fb = {"title":"Facebook ST Score","data":this.state.fb_main}
    var tw = {"title":"Twitter ST Score","data":this.state.tw_main}



    return (
      <div className="side-pad beta">
        <Grid stackable columns='two' style={{"max-height": "37em"}}>
        <Grid.Column width={7}>
        <Segment>
        <Grid.Row>
        <div className="og-div"><span className="card-span">Social Media Followers</span></div>
        </Grid.Row>
          <Grid.Row>
          <Grid stackable columns='two' className={"main-pad"}>
            <Grid.Column>
            <Grid.Row>
              <Image src={fbImage} size='tiny' style={{"width":"4em","margin": "auto"}} />
            </Grid.Row>
            <div style={{'font-size': '4rem', 'text-align': 'center'}}><div className="og-div hike1 omega-pad-2"><span className="digit-span-card" style={{"font-size":"2.5rem"}}>{ Math.round((this.state.facebook.followers/1000000) * 100) / 100}M</span> <span style= {{'color':this.state.facebook.followers_change < 0?'red':"green",'display':this.state.facebook.followers_change ===0?'none':""}}><Icon name={this.state.facebook.followers_change < 0?'caret down':"caret up"}/>{ Math.abs(Math.round(this.state.facebook.followers_change/1000 * 100) / 100) }K</span></div></div>
            </Grid.Column>
            <Grid.Column>
            <Image src={twImage} size='tiny' style={{"width":"4em","margin": "auto"}} />
            <div style={{'font-size': '4rem', 'text-align': 'center'}}><div className="og-div hike1 omega-pad-2"><span className="digit-span-card" style={{"font-size":"2.5rem"}}>{ Math.round((this.state.twitter.followers/1000000) * 100) / 100}M</span> <span style= {{'color':this.state.twitter.followers_change < 0?'red':"green",'display':this.state.twitter.followers_change ===0?'none':""}}><Icon name={this.state.twitter.followers_change < 0?'caret down':"caret up"}/>{ Math.abs(Math.round(this.state.twitter.followers_change/1000 * 100) / 100) }K</span></div></div>
            </Grid.Column>
          </Grid>
          </Grid.Row>
          <div>
            <Grid stackable columns='two'>
              <Grid.Column className={'border'}>
              <Grid.Row>
              <div style={{'font-size': '3rem', 'text-align': 'center'}}><div className="og-div hike2 omega-pad-2"><span className="digit-span-card" style={{"font-size":"2rem"}}>{this.state.facebook.post}</span> <span style= {{'color':this.state.facebook.post_change < 0?'red':"green",'display':this.state.facebook.post_change ===0?'none':""}}><Icon name={this.state.facebook.post_change < 0?'caret down':"caret up"}/> { Math.abs(Math.round(this.state.facebook.post_change * 100) / 100) }</span></div><div className="social-font">Posts</div></div>
              </Grid.Row>
              <br/>
              <Grid.Row>
              <div style={{'font-size': '3rem', 'text-align': 'center'}}><div className="og-div hike2 omega-pad-2"><span className="digit-span-card" style={{"font-size":"2rem"}}>{ Math.round((this.state.facebook.likes/1000) * 100) / 100}K</span> <span style= {{'color':this.state.facebook.like_change < 0?'red':"green",'display':this.state.facebook.like_change ===0?'none':""}}><Icon name={this.state.facebook.like_change< 0?'caret down':"caret up"}/> { Math.abs(Math.round(this.state.facebook.like_change/1000 * 100) / 100) }K</span></div><div className="social-font">Likes</div></div>
              </Grid.Row>
              <br/>
              <Grid.Row>
              <div style={{'font-size': '3rem', 'text-align': 'center'}}><div className="og-div hike2 omega-pad-2"><span className="digit-span-card" style={{"font-size":"2rem"}}>{this.state.facebook.shares}</span> <span style= {{'color':this.state.facebook.share_change < 0?'red':"green",'display':this.state.facebook.share_change ===0?'none':""}}><Icon name={this.state.facebook.share_change < 0?'caret down':"caret up"}/> { Math.abs(Math.round(this.state.facebook.share_change * 100) / 100) }</span></div><div className="social-font">Shares</div></div>
              </Grid.Row>
              <br/>
              </Grid.Column>
              <Grid.Column>
              <div style={{'font-size': '3rem', 'text-align': 'center'}}><div className="og-div hike2 omega-pad-2"><span className="digit-span-card" style={{"font-size":"2rem"}}>{this.state.twitter.post}</span> <span style= {{'color':this.state.twitter.post_change < 0?'red':"green",'display':this.state.twitter.post_change ===0?'none':""}}><Icon name={this.state.twitter.post_change < 0?'caret down':"caret up"}/> { Math.abs(Math.round(this.state.twitter.post_change * 100) / 100) }</span></div><div className="social-font">Tweets</div></div>
              <br/>
              <div style={{'font-size': '3rem', 'text-align': 'center'}}><div className="og-div hike2 omega-pad-2"><span className="digit-span-card" style={{"font-size":"2rem"}}>{ Math.round((this.state.twitter.likes/1000) * 100) / 100}K  </span> <span style= {{'color':this.state.twitter.like_change < 0?'red':"green",'display':this.state.twitter.like_change===0?'none':""}}><Icon name={this.state.twitter.post_change < 0?'caret down':"caret up"}/> { Math.abs(Math.round(this.state.twitter.like_change/1000 * 100) / 100) }K</span></div><div className="social-font">Favourites</div></div>
              <br/>
              <div style={{'font-size': '3rem', 'text-align': 'center'}}><div className="og-div hike2 omega-pad-2"><span className="digit-span-card" style={{"font-size":"2rem"}}>{this.state.twitter.shares}</span> <span style= {{'color':this.state.twitter.share_change < 0?'red':"green",'display':this.state.twitter.share_change ===0?'none':""}}><Icon name={this.state.twitter.share_change < 0?'caret down':"caret up"}/> { Math.abs(Math.round(this.state.twitter.share_change * 100) / 100) }</span></div><div className="social-font">Retweets</div></div>
              </Grid.Column>
            </Grid>
          </div>

          </Segment>
        </Grid.Column>
        <Grid.Column width={9}>
        <Segment>
          <Grid.Row>
              <BarCard format={fb} tw_format = {tw} fb_rank={this.state.fb_list} tw_rank={this.state.fb_list}/>
          </Grid.Row>
        </Segment>
        </Grid.Column>
        </Grid>
        <Grid stackable >
        <Grid.Column width={6}>
        <Segment>
        <Grid.Row style={{'padding-top': '0rem','height': '50%'}} className={"border-row"}>
        <div className="og-div"><span className="card-span"> ST Rank</span></div>
          <Grid stackable columns='two'>
              <Grid.Column className={"border-col"}>
               <Image src={fbImage} size='tiny' style={{"width":"2em"}} />
               <div style={{'font-size': '8rem', 'text-align': 'center'}}><div className="og-div hike3 omega-pad-2"><span className="digit-span-card" style={{"font-size":"4em"}}>{this.state.fb_latest}</span> <span style= {{'color':this.state.fb_rank_change < 0?'red':"green",'display':this.state.fb_rank_change ===0?'none':""}}><Icon name={this.state.fb_rank_change < 0?'caret down':"caret up"}/> { Math.abs(Math.round(this.state.fb_rank_change * 100) / 100) }</span></div></div>
              </Grid.Column>
              <Grid.Column>
               <Image src={twImage} size='tiny' style={{"width":"2em"}} />
               <div style={{'font-size': '8rem', 'text-align': 'center'}}><div className="og-div hike3 omega-pad-2"><span className="digit-span-card" style={{"font-size":"4em"}}>{this.state.tw_latest}</span> <span style= {{'color':this.state.tw_rank_change < 0?'red':"green",'display':this.state.tw_rank_change ===0?'none':""}}><Icon name={this.state.tw_rank_change < 0?'caret down':"caret up"}/> { Math.abs(Math.round(this.state.tw_rank_change * 100) / 100) }</span></div></div>
              </Grid.Column>
          </Grid>
        </Grid.Row>
        <Grid.Row style={{'padding-top': '0rem'}}>
        <div className="og-div"><span className="card-span"> ST Score</span></div>
        <Grid stackable columns='two'>
        <Grid.Row >
            <Grid.Column width={2} style={{ "padding-left": "0rem"}} >
             <Image src={fbImage} size='tiny' style={{"width":"2em"}} />
            </Grid.Column>
            <Grid.Column width={9} style={{"padding-left": "0rem"}}>
            <div className="ui red active indicating progress" data-percent={this.state.fb_score}><div className="bar" style={{"width": this.state.fb_score+"%","background-color":"#465792"}}></div></div>
            </Grid.Column>
            <Grid.Column width={5} style={{ "padding-left": "0rem"}}>
             <div className="og-div hike omega-pad-2"> <span className="digit-span-card" style={{"font-size":"1.5rem"}}>{this.state.fb_score}</span> <span style= {{'color':this.state.fb_score_change  < 0?'red':"green",'display':this.state.fb_score_change ===0?'none':""}}><Icon name={this.state.fb_score_change < 0?'caret down':"caret up"}/> {Math.abs(Math.round(this.state.fb_score_change * 100) / 100) }</span></div>
            </Grid.Column>
        </Grid.Row>
        <Grid.Row >
            <Grid.Column width={2} style={{ "padding-left": "0rem"}}>
            <Image src={twImage} size='tiny' style={{"width":"2em"}} />
            </Grid.Column>
            <Grid.Column width={9}  style={{"padding-left": "0rem"}}>
            <div className="ui red active indicating progress" data-percent={this.state.tw_score}><div className="bar" style={{"width": this.state.tw_score+"%","background-color":"#26bdf6"}}></div></div>
            </Grid.Column>
            <Grid.Column width={5} style={{"padding-left": "0rem"}}>
             <div className="og-div hike omega-pad-2"><span className="digit-span-card" style={{"font-size":"1.5rem"}}>{this.state.tw_score}</span><span style= {{'color':this.state.tw_score_change  < 0?'red':"green",'display':this.state.tw_score_change ===0?'none':""}}><Icon name={this.state.tw_score_change < 0?'caret down':"caret up"}/> {Math.abs(Math.round(this.state.tw_score_change * 100) / 100) }</span></div>
            </Grid.Column>
        </Grid.Row>
        </Grid>
        </Grid.Row>
        </Segment>
        </Grid.Column>
        <Grid.Column  width={5}>
        <Segment>
        <Grid.Row>
            <LineCard format={line4} color={"#26bdf6"} sync={"twitter"}/>
        </Grid.Row>
        <Grid.Row>
            <LineCard format={line5} color={"#26bdf6"} sync={"twitter"}/>
        </Grid.Row>
        <Grid.Row>
            <LineCard format={line6} color={"#26bdf6"} sync={"twitter"}/>
        </Grid.Row>
        </Segment>
        </Grid.Column>
        <Grid.Column width={5}>
        <Segment>
        <Grid.Row>
            <LineCard format={line1} color={"#465792"} sync={"facebook"}/>
        </Grid.Row>
        <Grid.Row>
            <LineCard format={line3} color={"#465792"} sync={"facebook"}/>
        </Grid.Row>
        <Grid.Row>
            <LineCard format={line2} color={"#465792"} sync={"facebook"}/>
        </Grid.Row>
        </Segment>
        </Grid.Column>
        </Grid>
        <br/>
        <div className="filter-div">
          <Button  onClick={this.ShowPost.bind(this)}>{this.state.postText}</Button>
        </div>
        <br/>
        {this.state.postShow?<Timeline data={this.state.post}/>:""}
        {this.state.postShow}
      </div>
    )
  }
}

export default SocialDash;
