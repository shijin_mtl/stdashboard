import React, { Component } from 'react'
import { Grid } from 'semantic-ui-react'
import {  Area,Line, XAxis, YAxis, Tooltip, ResponsiveContainer, ComposedChart} from 'recharts';
import moment from 'moment'






class BarCard extends Component {
  render() {

    var list = []
    var tw_list = []


    if(this.props.format.data !==[] && this.props.format.data !== undefined && this.props.fb_rank.length !== 0 && this.props.tw_rank.length !== 0)
    {

      for (let key in this.props.format.data.scores) {
            list.push({ date: moment(key, "YYYY-MM-DD").format("MMM D, YYYY"), Score: this.props.format.data.scores[key],Rank:this.props.fb_rank[key]})
      }

      for (let key in this.props.tw_format.data.scores) {
            tw_list.push({ date: moment(key, "YYYY-MM-DD").format("MMM D, YYYY"), Score: this.props.tw_format.data.scores[key],Rank:this.props.tw_rank[key]})
      }

      console.log(list,tw_list);

    }



    return (
            <Grid.Column>
            <Grid.Row style={{"min-height": "18em"}}>
              <ResponsiveContainer  aspect={4.0/0.8} width={"100%"}>

                <ComposedChart data={list} syncId="anyId"
                        margin={{ top:10, right: 0, left: 0, bottom: 10}}
                      >
                      <defs>
                    <linearGradient id="MyBarGradient" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="10%" stopColor="#465792" />
                    <stop offset="95%" stopColor="#465792" />
                    </linearGradient>
                  </defs>
                    <XAxis dataKey="date" hide={true} stroke="black" />
                    <YAxis dataKey="Score"  hide={true} yAxisId={1}/>
                    <YAxis dataKey="Rank"  hide={true} yAxisId={2} />
                    <Tooltip  cursor={false} />

                    <Area
                      type="monotone"
                      dataKey="Score"
                      name="Facebook ST Score"
                      stroke="black"
                      strokeWidth="0"
                      fillOpacity="1"
                      fill="url(#MyBarGradient)"
                      background={{ fill: '#465792' }}
                      yAxisId={1}
                    />

                    <Line type='monotone' dataKey='Rank' strokeWidth={2} stroke={"#f9da11"} yAxisId={2} name="Facebook ST Rank" />

                  </ComposedChart>

              </ResponsiveContainer>
              </Grid.Row>
              <Grid.Row >
              <ResponsiveContainer width={"100%"} aspect={4.0/0.8}>
                <ComposedChart data={tw_list} syncId="anyId"
                        margin={{ top:10, right: 0, left: 0, bottom:10 }}
                      >
                      <defs>
                    <linearGradient id="MyBarGradient" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="10%" stopColor="#26bdf6" />
                    <stop offset="95%" stopColor="#26bdf6" />
                    </linearGradient>
                  </defs>
                    <XAxis dataKey="date" stroke="black" />
                    <YAxis dataKey="Score"  hide={true} yAxisId={1}/>
                    <YAxis dataKey="Rank"  hide={true} yAxisId={2} reversed={true}/>
                    <Tooltip  cursor={false} />
                    <Area
                      type="monotone"
                      dataKey="Score"
                      stroke="black"
                      name="Twitter ST Score"
                      strokeWidth="0"
                      fillOpacity="1"
                      fill="#26bdf6"
                      background={{ fill: '#26bdf6' }}
                      yAxisId={1}
                    />

                    <Line type='monotone' dataKey='Rank' strokeWidth={2} stroke={"#f9da11"} yAxisId={2}  name="Twitter ST Rank"/>

                  </ComposedChart>
              </ResponsiveContainer>
              </Grid.Row>
            </Grid.Column>
    )
  }
}

export default BarCard;
