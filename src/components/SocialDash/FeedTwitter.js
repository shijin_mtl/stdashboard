import React, { Component } from 'react'
import { Feed,Icon,Segment,Menu } from 'semantic-ui-react'
import { socialStore,getSocial} from '../../store/socialStore'
import {  LineChart, Line, XAxis, Tooltip, ResponsiveContainer} from 'recharts';
import moment from 'moment'





class FeedTwitter extends Component {

  constructor(props) {
    super(props)
    this.store = socialStore()
    this.state = {
      loading: false,
      content:"",
      post:this.props.data.content,
      active:false
    }
  }

  componentWillMount() {
    this.store.subscribe(this.onProcess.bind(this))

  }


  onProcess(response){
    const state = this.store.getState()
    if(state.success)
      {
        let list = []
        if(state.data.length !== 0)
        {
          for (let key in state.data) {
                list.push({ Date: moment(state.data[key].date, "YYYY-MM-DD").format("MMM D, YYYY"), Likes:state.data[key].likes,Shares:state.data[key].shares,Comments:state.data[key].comments},)
          }
        }
        else {
          ;
        }

        var id = (Math.random().toString(16).substring(1))

        this.setState({
          content:[
            <div>
            <ResponsiveContainer width='100%' aspect={4.0/.5} key>
              <LineChart data={list} syncId={id}
                margin={{ top: 10, right: 30, left: 30, bottom: 30 }}>
                  <Tooltip />
                  <XAxis dataKey="Date"   />
                  <Line type='monotone' dataKey='Likes' name="Favourites" strokeWidth={2}/>
                </LineChart>
            </ResponsiveContainer>
            <ResponsiveContainer width='100%' aspect={4.0/.5} key>
              <LineChart data={list} syncId={id}
                margin={{ top: 10, right: 30, left: 30, bottom: 30 }}>
                  <Tooltip />
                  <XAxis dataKey="Date"   />
                  <Line type='monotone' dataKey='Shares' name="Retweets"  strokeWidth={2}/>
                </LineChart>
            </ResponsiveContainer>

            <ResponsiveContainer width='100%' aspect={4.0/.5} key>
              <LineChart data={list} syncId={id}
                margin={{ top: 10, right: 30, left: 30, bottom: 30 }}>
                  <Tooltip />
                  <XAxis dataKey="Date"   />
                  <Line type='monotone' dataKey='Comments' name="Comments" strokeWidth={2}/>
                </LineChart>
            </ResponsiveContainer>
            </div>


          ],
          active:! this.state.active
        })
      }
      }



      clickSocial(post,filter){
        if(filter === "post"){
            this.setState({
              content:this.state.post,
              active:"post"
            })
        }
        else {
          this.store.dispatch(getSocial({'post':post,'filter':filter }))
        }

      }



  render() {


    const TabExampleAttachedBottom = () => (
      <div>
        <Segment attached>
        <Feed.Summary className="post-head">
        <Icon name='twitter' className="twitter-post" /> {moment(this.props.data.date_posted.slice(0, 10) , "YYYY-MM-DD").format("MMM D, YYYY")} | Type: {this.props.data.type}
        </Feed.Summary>
        <Feed.Extra text><b>
          {this.state.post}</b>
                </Feed.Extra>
        </Segment>

        <Menu attached='bottom' fluid widths={6}>
          <Menu.Item
            name='section1'
          >
            <span className="social-span">{this.props.data.likes<500?this.props.data.likes:Math.round( this.props.data.likes/1000  ) / 10  +"K"} </span>&nbsp;  Favourites
          </Menu.Item>

          <Menu.Item
            name='section2'
          >
            <span className="social-span">{this.props.data.shares<500?this.props.data.shares:Math.round( this.props.data.shares/1000  ) / 10  +"K"}   </span> &nbsp;Retweets
          </Menu.Item>

          <Menu.Item
            name='section2'
          >
             <span className="social-span">{this.props.data.comments<500?this.props.data.comments:Math.round( this.props.data.comments/1000  ) / 10  +"K"} </span>&nbsp; Comments
          </Menu.Item>
          <Menu.Item
            name='section2'
          >
             <span className="social-span">{this.props.data.engagement_score<500?this.props.data.engagement_score:Math.round( this.props.data.engagement_score/1000  ) / 10  +"K"}</span> &nbsp;Engagement Score
          </Menu.Item>
          <Menu.Item
            name='section2'
            target="blank" href={this.props.data.permalink}

          >
            Link
          </Menu.Item>
          <Menu.Item item icon='bar graph' simple onClick={this.clickSocial.bind(this,this.props.data.post_id,'likes')} active={this.state.active}>

      </Menu.Item>
        </Menu>
        {this.state.active?<Segment attached>{this.state.content}</Segment>:""}
      </div>

    )




    return (
      <Feed>
  {/*<Feed.Event>
    <Feed.Content>

      <Feed.Summary className="post-head">
      <Icon name='facebook' className="fb-post" /> {moment(this.props.data.date_posted.slice(0, 10) , "YYYY-MM-DD").format("MMM D, YYYY")} | Type: {this.props.data.type}
      </Feed.Summary>
      <Feed.Extra text><b>
        {this.state.content}</b>
              </Feed.Extra>
              {this.state.content === this.state.post?<Feed.Extra images>
                {this.props.data.type==="photo"?<a><img alt='img' src={this.props.data.media_link} /></a>:""}
              </Feed.Extra>:""}
              <Button.Group basic stackable>
                  {this.state.active!=="post"?<Button  active={this.state.active==="post"} onClick={this.clickSocial.bind(this,this.props.data.post_id,'post')} >Post</Button>:""}
                  <Button active={this.state.active==="likes"} onClick={this.clickSocial.bind(this,this.props.data.post_id,'likes')}>
                  {this.props.data.likes} Likes</Button>
                  <Button active={this.state.active==="shares"} onClick={this.clickSocial.bind(this,this.props.data.post_id,'shares')}>
                  {this.props.data.shares} Shares</Button>
                  <Button active={this.state.active==="comments"} onClick={this.clickSocial.bind(this,this.props.data.post_id,'comments')} >
                  {this.props.data.comments} Comments</Button>
                  <Button target="blank" href={this.props.data.permalink}><Icon name='external' />
                  Link</Button>
                </Button.Group>



            </Feed.Content>
          </Feed.Event>*/}


            <TabExampleAttachedBottom/>


          <br/>
          </Feed>
            )
          }
        }

export default FeedTwitter;
