import React, { Component } from 'react'
import { Grid } from 'semantic-ui-react'
import {  LineChart, Line, XAxis, Tooltip, ResponsiveContainer} from 'recharts';
import HelpText from "../HelpText"
import moment from 'moment'





class LineCard extends Component {

  render() {
    var list = []
    //
    // data.map((data,index) => {
    //                       list.push({ date: moment(key, "YYYY-MM-DD").format("MMM D, YYYY"), Value: data},)
    //                   });

    // console.log(this.props.format.data);

    if(this.props.format.data !==[] && this.props.format.data !== undefined)
    {
      var keys = Object.keys(this.props.format.data)
      keys.sort(function(a,b){
        return new Date(a) - new Date(b);
      });
      var count = 0

      for (var [key] in this.props.format.data) {
            list.push({ date: moment(keys[count], "YYYY-MM-DD").format("MMM D, YYYY"), Value: this.props.format.data[keys[count]],"id":key},)
            count++
      }
    }


    return (
            <Grid.Column>
              <Grid.Row>
              <div className="og-div"><span className="card-span">{this.props.format.title}<HelpText data={this.props.format.text}/></span></div>
              {/*<div className="og-div hike"><span className="digit-span-card">{this.props.format.value}</span> <span style={{color:this.props.format.color}}>({this.props.format.change})</span></div>*/}
              </Grid.Row>
              <Grid.Row>
              <ResponsiveContainer width='100%' aspect={4.0/.8}>
                <LineChart syncId="anyId"
                    data={list}
                    margin= {{ top: 5, right: 20, bottom: 3, left: 20 }}
                      >
                      <defs>
                    <linearGradient id="MyLineGradient" x1="0" y1="0" x2="0" y2="1">
                      <stop offset="10%" stopColor={this.props.color}/>
                      <stop offset="95%" stopColor={this.props.color} />
                    </linearGradient>
                  </defs>
                    <Tooltip cursor={false} />
                    <XAxis dataKey='date' hide={true} />
                    <Line type='monotone' dataKey='Value' strokeWidth={2} stroke={this.props.color}  />
                  </LineChart>
              </ResponsiveContainer>
              </Grid.Row>
            </Grid.Column>
    )
  }
}

export default LineCard;
