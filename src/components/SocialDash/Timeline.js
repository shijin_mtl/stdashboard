import React, { Component } from 'react'
import { Loader,Grid,Button } from 'semantic-ui-react'
import 'react-vertical-timeline-component/style.min.css';
import FeedFb from './FeedFb'
import FeedTwitter from './FeedTwitter'

class Timeline extends Component {

  constructor(props) {
    super(props)
    this.state = {
      filter: "all",
    }
  }

  filterChange(value){
    this.setState({
      filter:value
    })
  }



  render() {
    var list = <Loader active />
    var opti = {}

    if(this.props.data){
      var a
      list = []
      this.props.data.map((data,index) => {
                            opti[data.post_id] = data
                            return true;
                        });
              for (var key in opti){
                let data = opti[key]
                if(this.state.filter==="all"){
                  list.push(
                  data.network==='facebook'?<Grid.Column><FeedFb data={data} key={key}/></Grid.Column>:<Grid.Column><FeedTwitter data={data} key={key}/></Grid.Column>
                  )
                }
                if(this.state.filter==="facebook")
                {
                  data.network==='facebook'?list.push(<Grid.Column><FeedFb data={data} key={key}/></Grid.Column>):a = "filter"
                }
                if(this.state.filter==="twitter")
                {
                  data.network==='twitter'?list.push(<Grid.Column><FeedTwitter data={data} key={key}/></Grid.Column>):a = "filter"
                }
                console.log(a);

              }
    }

    return (
              <div>
              <div className="filter-div">
              <Button active= {this.state.filter==="all"} onClick={this.filterChange.bind(this,"all")}>All</Button>
              <Button active= {this.state.filter==="facebook"}  onClick={this.filterChange.bind(this,"facebook")}>Facebook</Button>
              <Button active= {this.state.filter==="twitter"} onClick={this.filterChange.bind(this,"twitter")}>Twitter</Button>
              </div>
                <Grid stackable  className="post" columns='one'>
                 {list.length !== 0?list:<div className="post-data">No data to show.</div>}
                </Grid>
              </div>
            )
          }
        }

export default Timeline;
