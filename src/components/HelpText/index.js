import React, { Component } from 'react'
import { Icon, Popup } from 'semantic-ui-react'

class HelpText extends Component {
  render() {
    var dummy = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam'
    return (
      <Popup 
        style={{'text-transform':'none'}}
        trigger={<Icon name='question circle outline' size='small' className="icon-bar" />}
        content= {this.props.data?this.props.data:dummy}
        position='top center'
      />
    )
  }
}

export default HelpText;
