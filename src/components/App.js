import React, { Component } from 'react'
import RouterConfig from './Router';
import { BrowserRouter } from 'react-router-dom'


class App extends Component {
  render() {
    return (
      <div>
      {/*<Header/>*/}
      <BrowserRouter>
      <RouterConfig/>
      </BrowserRouter>
      </div>
    )
  }
}

export default App;
