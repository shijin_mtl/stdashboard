import React, { Component } from 'react'
import { Menu,Image,Dropdown,Icon } from 'semantic-ui-react'
import SubHeader from '../SubHeader'
import logo from '../../public/img/logo.png';
import { DateRangePicker } from 'react-dates'
import { logoutStore, logout } from '../../store/logoutStore'

import moment from 'moment'
import axios from 'axios';
import config from '../../utils/config';



const loginApi = axios.create({
  timeout: 10000,
  withCredentials: true,
  headers:{
  	crossDomain:true
  }
});


const trigger = (
<span>
  <Image avatar src={'https://s3-us-west-2.amazonaws.com/interface-images/'+localStorage.id+'.jpg'} alt="User image" />
</span>
)

const options = [
  {value:'',className:"hide-ds"},
{ key: 'sign-out', text: 'Sign Out', icon: 'sign out',value:'logout' },

]

const read_option = [
  {value:'',className:"hide-ds"},
{ key: 'read', text: 'View Stories',value:'read' },
]

class Header extends Component {

  constructor(props) {
    super(props);
    this.store = logoutStore()
    var subHead
    switch(this.props.history.location.pathname)
     {
    case "/":
        subHead = "dashboard"
        break;
    case "/insights/":
        subHead = "insights"
        break;
    case "/head-to-head/":
        subHead = "head"
        break;
    default:
      break;
    }


    this.state = {
        focusedInput: null,
        startDate: moment(this.props.date.start),
        endDate:moment(this.props.date.end),
        activeItem:subHead?subHead:"dashboard",
    };

    this.onDatesChange = this.onDatesChange.bind(this);
    this.onFocusChange = this.onFocusChange.bind(this);
    }

    componentDidMount() {
      this.store.subscribe(this.onLogoutProcess.bind(this))
    }


    onLogoutProcess = () =>{
      const state = this.store.getState()
      if(state.success){
        this.props.history.push("/login/")
      }
      else {
        alert('logout failed')
      }
    }

    onLogoutClicked = () => {
        this.store.dispatch(logout())
    }


    onDatesChange({ startDate, endDate }) {
        this.props.dateChange(startDate, endDate )
        this.setState({ startDate, endDate });
    }



    checkLogin(){
      loginApi.get(config.server +'is_login/')
      .then(data => {
        if(data.data.status)
        {
            localStorage.saveData = data.data.id
            return true

        }
        else {
        //  this.props.history.push("/login/")
        }
      })
      .catch(e => {
        console.log(e);
        // this.props.history.push("/login/")
        return false
      })
    }


    onFocusChange(focusedInput) {
        this.setState({ focusedInput });
    }


    urlChange = (url) => {
      this.props.history.push(url);
    }


    handleItemClick = (e, { name }) => {
      switch(name) {
      case "read":
          this.props.history.push("/read/")
          break;
    case "dashboard":
        this.props.history.push("/")
        break;
    case "insights":
        this.props.history.push("/insights/")
        break;
    case "head":
        this.props.history.push("/head-to-head/")
        break;
    case "stories":
        this.props.history.push("/read/")
        break;
    default:
      break;
       }
    this.setState({ activeItem: name?name:"dashboard" })
    this.forceUpdate()
  }


  handleChange = (e, { value }) => {
    if(value==='logout')
    {
      this.store.dispatch(logout())
    }
    if(value==='read')
    {
      this.setState({
        activeItem:'read'
      })
      this.props.history.push("/read/")
    }
  }




  render() {

    const { focusedInput } = this.state;
    if (this.props.history.location.pathname === '/login/'){
      return (<div/>)
    }
    else {
      this.checkLogin()
    return (
      <div>
      <Menu stackable>
        <Menu.Item
        onClick={this.handleItemClick}
        name="dashboard"
        >
          <img src={logo}  alt="logo"   />
        </Menu.Item>
        <Menu.Item
          name='dashboard'
          active={this.state.activeItem === 'dashboard'}
          onClick={this.handleItemClick}
        >
          My Scores
        </Menu.Item>
        {/*<Menu.Item
          name='insights'
          active={this.state.activeItem === 'insights'}
          onClick={this.handleItemClick}
        >
        Competitors Insights
        </Menu.Item>*/}

        <Menu.Item
          name='head'
          active={this.state.activeItem === 'head'}
          onClick={this.handleItemClick}
        >
          Head to Head
        </Menu.Item>
        {/*<Menu.Item
          name='read'
          active={this.state.activeItem === 'read'}
          onClick={this.handleItemClick}
        >
          Read News
        </Menu.Item>*/}
        <Menu.Item position={"right"}>
        <DateRangePicker
              onDatesChange={this.onDatesChange}
              onFocusChange={this.onFocusChange}
              focusedInput={focusedInput}
              startDate={this.state.startDate}
              endDate={this.state.endDate}
              startDateId="datepicker_start_home"
              endDateId="datepicker_end_home"
              startDatePlaceholderText="Start Date"
              endDatePlaceholderText="End Date"
              isOutsideRange={() => false}
              displayFormat="YYYY/MM/DD "
          />
          </Menu.Item>
          <Menu.Item>
          <Dropdown trigger={<Icon disabled name='settings' />} options={read_option} pointing='top right' icon={null} onChange={this.handleChange}    />
          </Menu.Item>
        <Menu.Item>
        <Dropdown trigger={trigger} options={options} pointing='top right' icon={null} onChange={this.handleChange}    />
        </Menu.Item>
      </Menu>
      <SubHeader item={this.state.activeItem} urlChange={this.urlChange} path={this.props.location.pathname} history={this.props.history}/>
      </div>
    )
  }
  }
}

export default Header;
