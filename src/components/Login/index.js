import React, { Component } from "react";
import { Dimmer, Loader } from "semantic-ui-react";
import { loginStore, login, googlelogin } from "../../store/loginStore";
/*import { GoogleLogin } from 'react-google-login';*/
/*let googleImg = require('../../public/img/logog.png');*/

class Login extends Component {
  constructor(props) {
    super(props);
    this.store = loginStore();
    this.state = {
      loading: false,
      message: "",
      username: "",
      password: ""
    };
  }

  componentDidMount() {
    this.store.subscribe(this.onLoginProcess.bind(this));
  }

  responseGoogle(response) {
    this.setState({
      message: ""
    });
    this.store.dispatch(
      googlelogin({ access_token: response.tokenObj.access_token })
    );
  }
  errorResponseGoogle(response) {
    this.setState({
      message: "Google login failed"
    });
  }

  onLoginProcess = () => {
    const state = this.store.getState();
    if (state.data && state.data.status) {
      this.setState({
        message: ""
      });
      localStorage.id = state.data.id;
      localStorage.name = state.data.name;
      this.props.history.push("/");
      window.location = "/";
    } else if (!state.data.status && state.loading) {
      this.setState({
        loading: state.loading
      });
    } else {
      this.setState({
        message: "Login failed"
      });
      this.setState({
        loading: state.loading
      });
    }
  };
  onLoginClicked = () => {
    if (this.state.username === "" || this.state.password === "") {
      this.setState({
        message: "Please provide all the fields"
      });
    } else {
      this.store.dispatch(
        login({ username: this.state.username, password: this.state.password })
      );
    }
  };

  handleChange = (key, e) => {
    this.setState({
      [key]: e.target.value
    });
  };

  render() {
    return (
      <div className="bg-blue">
        {this.state.loading ? (
          <Dimmer active inverted>
            <Loader size="large">Loading...</Loader>
          </Dimmer>
        ) : (
          ""
        )}
        <div className="wrapper">
          <div id="formContent">
            <div>
              <img
                src="https://interface.staging.scoretrends.com/assets/logo.png"
                id="icon"
                alt="User Icon"
              />
            </div>
            <form>
              <div className="form-group">
                <input
                  type="text"
                  id="login"
                  className="form-control"
                  name="login"
                  placeholder="Username"
                  value={this.state.username}
                  onChange={this.handleChange.bind(this, "username")}
                />
              </div>
              <div className="form-group">
                <input
                  type="password"
                  id="password"
                  className="form-control"
                  name="login"
                  placeholder="Password"
                  value={this.state.password}
                  onChange={this.handleChange.bind(this, "password")}
                />
              </div>
              <input
                type="button"
                className="ui fluid large blue submit button"
                value="Login"
                onClick={this.onLoginClicked}
              />
            </form>
            <div id="formFooter">
              {/*<GoogleLogin
        clientId="436873590273-9vhlug9ffmohr3re771jeg6un71lhi21.apps.googleusercontent.com"
        onSuccess={this.responseGoogle.bind(this)}
        onFailure={this.errorResponseGoogle.bind(this)}
        className='ui white submit button'
        style={{'padding': '0.5em 1em .5em .7em','background':'#ffffff00'}}
      >
      <img src={googleImg} alt="google" width="20" style={{'verticalAlign':'middle'}} className="img-responsive"/>&nbsp;<span style={{'verticalAlign':'middle','marginLeft': '.7em'}}>Sign in with Google</span>
      </GoogleLogin>*/}
              <b>
                <div style={{ color: "red", padding: "0.5em 1em .5em .7em" }}>
                  {this.state.message}
                </div>
              </b>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;

