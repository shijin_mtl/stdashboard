import React, { Component } from 'react'
import { ResponsiveContainer, LineChart, Line, XAxis, YAxis,
  Tooltip } from 'recharts';
import HelpText from "../HelpText"
import moment from 'moment'


class MainRank extends Component {
  state = {
    activeIndex: 0,
    animation: false,
  };
  render() {
    var list = []

    if(this.props.total.scores !== undefined && this.props.total !== [] )
    {
            for (var key in this.props.total.scores)
            {
                  list.push({ date: moment(key, "YYYY-MM-DD").format("MMM D, YYYY"), Score: this.props.total.scores[key] ,Rank:this.props.rank.ranks[key]},)

            }
    }


    return (
          <div style={{'padding-bottom':'3em'}}>
            <div className="og-div"><span className="rank-span">ST Rank</span> <span><HelpText data={"Shows your ST Rank on "+ moment(this.props.date2.end, "YYYY-MM-DD").format("MMM D, YYYY")+" ,Change is shown in comparison with rank of "+ moment(this.props.date2.start, "YYYY-MM-DD").format("MMM D, YYYY") }/></span></div>
            <ResponsiveContainer width='100%' aspect={4.0/1}>
            <LineChart

                  data={list}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                  <XAxis dataKey='date'/>
                  <YAxis dataKey="Rank" orientation='right'  hide={true} reversed={true}/>
                  <Tooltip cursor={false} />
                  <Line dataKey='Rank' stroke='#387908' strokeWidth={2} dot={true}/>
            </LineChart>
            </ResponsiveContainer>
          </div>

    )
  }
}

export default MainRank;
