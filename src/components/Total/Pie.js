import React, { Component } from 'react'
import { Grid } from 'semantic-ui-react'
import { ResponsiveContainer,Cell,
PieChart,Pie,Sector } from 'recharts';
import HelpText from "../HelpText"
import moment from 'moment'



const COLORS = ['#4267b2','#1DA1F2', '#f45c27', '#9650b0', '#fa9c30'];




                  const renderActiveShape = (props) => {
                    const RADIAN = Math.PI / 180;
                    const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
                      fill, payload, percent } = props;
                    const sin = Math.sin(-RADIAN * midAngle);
                    const cos = Math.cos(-RADIAN * midAngle);
                    const sx = cx + (outerRadius + 10) * cos;
                    const sy = cy + (outerRadius + 10) * sin;
                    const mx = cx + (outerRadius + 30) * cos;
                    const my = cy + (outerRadius + 30) * sin;
                    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
                    const ey = my;
                    const textAnchor = cos >= 0 ? 'start' : 'end';

                    return (
                      <g>
                        <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.name}</text>

                        <Sector
                          cx={cx}
                          cy={cy}
                          innerRadius={innerRadius}
                          outerRadius={outerRadius}
                          startAngle={startAngle}
                          endAngle={endAngle}
                          fill={fill}
                        />

                        <Sector
                          cx={cx}
                          cy={cy}
                          startAngle={startAngle}
                          endAngle={endAngle}
                          innerRadius={outerRadius + 6}
                          outerRadius={outerRadius + 10}
                          fill={fill}
                        />

                        <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none"/>
                        <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none"/>
                        <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={15} textAnchor={textAnchor} fill="#999">
                          {`(${(percent * 100).toFixed(2)}%)`}
                        </text>
                      </g>
                    );
                  };











class PieRank extends Component {
  onPieEnter = (data, index, e) => {
    this.setState({
      activeIndex: index,
    });
  };

  state = {
    activeIndex: 0,
    animation: false,
  };

  render() {


    if(this.props.data.facebook!==[] && this.props.data.twitter.scores !== undefined)
    {
            for (var key in this.props.data.facebook.scores) {
                  var facebook = this.props.data.facebook.scores[key]
                  var twitter = this.props.data.twitter.scores[key]
                  var newspaper = this.props.data.newspaper.scores[key]/2
                  var viral = this.props.data.viralnews.scores[key]
                  var digital = this.props.data.digitalnews.scores[key]
            }
    }

    var pieData = [
      { name: 'Facebook', value: facebook },
      { name: 'Twitter', value: twitter },
      { name: 'Viral News', value: viral },
      { name: 'Digital News', value: digital },
      { name: 'Newspaper', value: newspaper/2 },
    ];




    return (
      <Grid.Column width={5}>
      <div className="og-div"><span className="card-span-small">Score Distribution <HelpText data={"Shows your Score Distribution from "+ moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY") +' to ' + moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY")}/></span></div>
      <ResponsiveContainer width='100%' aspect={4.0/2.7}>
      <PieChart>

    <Pie
      activeIndex={this.state.activeIndex}
      activeShape={renderActiveShape}
      data={pieData}
      innerRadius={40}
      outerRadius={70}
      fill="#8884d8"
      onMouseEnter={this.onPieEnter}
    >

    {
          	pieData.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
          }
    </Pie>

   </PieChart>
          </ResponsiveContainer>
      </Grid.Column>
    )
  }
}

export default PieRank;
