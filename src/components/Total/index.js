import React, { Component } from 'react'
import { Grid,Icon } from 'semantic-ui-react'
import MainRank from "./MainGraph"
import MainRank2 from "./MainGraph2"
import PieRank from "./Pie"
import Cards from "./Card"
import CardD from "./Card_d"
import CardV from "./Card_V"
import HelpText from "../HelpText"
import AreaGraph from "./AreaGraph"
import { ResponsiveContainer,AreaChart,Area} from 'recharts';
import { totalStore,totalFetch,fbFetch,twFetch,newsFetch } from '../../store/totalStore'
import { newspaperStore,getSourceVolume } from '../../store/newspaperStore'
import { digitalNewsStore,getSourceVolume as digitalSource } from '../../store/digitalNewsStore'
import { viralNewsStore,getSourceVolume as viralVolume} from '../../store/viralNewsStore'
import moment from 'moment'



class Total extends Component {
  constructor(props) {
    super(props)
    this.store = totalStore()
    this.news_store = newspaperStore()
    this.digital_store = digitalNewsStore()
    this.viral_store = viralNewsStore()
    this.state = {
      loading: false,
      total:[],
      rank:[],
      facebook:[],
      twitter:[],
      viral:[],
      digital:[],
      newpaper:[],
      rank_total:'',
      rank_change:'',
      news_list:[],
      digital_list:[],
      viral_list:[],
      cycle:{"start":"","end":""}
    }
  }


  componentWillMount() {
    this.store.subscribe(this.onProcess.bind(this))
    this.news_store.subscribe(this.onNews.bind(this))
    this.digital_store.subscribe(this.onDigital.bind(this))
    this.viral_store.subscribe(this.onViral.bind(this))

    this.store.dispatch(totalFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end}))
    this.store.dispatch(fbFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end}))
    this.store.dispatch(twFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end}))

    this.store.dispatch(newsFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,'type':'newspaper'}))
    this.store.dispatch(newsFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,'type':'viralnews'}))
    this.store.dispatch(newsFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,'type':'digitalnews'}))

    this.news_store.dispatch(getSourceVolume({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":"newspaper"}))
    this.digital_store.dispatch(digitalSource({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":"digital_news"}))
    this.viral_store.dispatch(viralVolume({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":"viralnews"}))
  }



  componentDidMount() {

  }


  changeFinder = (data) =>
     {var first = 0
     var last = 0
     var count = 0
     for (var key in data){
         if (count === 0)
             {
               first = data[key]
               count = 1
             }
         last = data[key]
       }
     var change = last - first
     return change
   }




  componentWillReceiveProps(nextProps)
  {
      this.setState({
      date:nextProps.date,
      })
      this.store.dispatch(totalFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end}))
      this.news_store.dispatch(getSourceVolume({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
      "type":"newspaper"}))
      this.digital_store.dispatch(digitalSource({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
      "type":"digital_news"}))
      this.viral_store.dispatch(viralVolume({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
      "type":"viralnews"}))
      this.store.dispatch(fbFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end}))
      this.store.dispatch(twFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end}))
      this.store.dispatch(newsFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,'type':'newspaper'}))
      this.store.dispatch(newsFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,'type':'viralnews'}))
      this.store.dispatch(newsFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,'type':'digitalnews'}))
  }


  onProcess(response){
          const state = this.store.getState()


          if(state.type === "news" && state.success)
          {

            if(state.data.newspaper !== undefined){
              this.setState({
                news_graph:state.data.newspaper
              })
            }

            if(state.data.viralnews !== undefined){
              this.setState({
                viral_graph:state.data.viralnews
              })
            }

            if(state.data.digitalnews !== undefined)
            {


              var count = 0
              var start = ""
              var end = ""

              for (var key in state.data.digitalnews.scores) {
                if(count === 0){
                  start = key
                  count = 1
                }
                end =key



              }

              this.setState({
                digital_graph:state.data.digitalnews,
                cycle:{"start":start,"end":end}
              })



            }

          }


          if(state.type === "twitter"){
            if(Array.isArray(state.data.twitter))
            {
              this.setState({
                tw_likes:state.data.twitter[0].twitter_favorites.sum,
                tw_change_likes:this.changeFinder(state.data.twitter[0].twitter_favorites.scores),
                tw_likes_data:this.graphMap(state.data.twitter[0].twitter_favorites.scores),
                tw_tweets:state.data.twitter[1].twitter_tweets.sum,
                tw_change_tweets:this.changeFinder(state.data.twitter[1].twitter_tweets.scores),
                tw_tweets_data:this.graphMap(state.data.twitter[1].twitter_tweets.scores),
                tw_retweets:state.data.twitter[2].twitter_retweets.sum,
                tw_change_retweets:this.changeFinder(state.data.twitter[2].twitter_retweets.scores),
                tw_tweets_redata:this.graphMap(state.data.twitter[2].twitter_retweets.scores),
                tw_followers:state.data.twitter[3].twitter_followers.sum,
                tw_change_followers:this.changeFinder(state.data.twitter[3].twitter_followers.scores),
                tw_followers_redata:this.graphMap(state.data.twitter[3].twitter_followers.scores),
              })
          }
          }

          if(state.success && !Array.isArray(state.data.facebook))
          {
            this.setState(state.data)
          }

          if(Array.isArray(state.data.facebook))
          {
            this.setState({
              fb_likes:state.data.facebook[0].facebook_post_likes.sum,
              change_likes:this.changeFinder(state.data.facebook[0].facebook_post_likes.scores),
              fb_likes_data:this.graphMap(state.data.facebook[0].facebook_post_likes.scores),
              shares:state.data.facebook[1].facebook_shares.sum,
              share_change:this.changeFinder(state.data.facebook[1].facebook_shares.scores),
              share_data:this.graphMap(state.data.facebook[1].facebook_shares.scores),
              post:state.data.facebook[3].facebook_post_number.sum,
              post_change:this.changeFinder(state.data.facebook[3].facebook_post_number.scores),
              post_data:this.graphMap(state.data.facebook[3].facebook_post_number.scores),
              likes:state.data.facebook[2].facebook_followers.sum,
              likes_change:this.changeFinder(state.data.facebook[2].facebook_followers.scores),
              page_data:this.graphMap(state.data.facebook[2].facebook_followers.scores),
            })
          }
      }

      pieData(type,value){
      }


    onNews()
    {
      let state_data = this.news_store.getState()


      if(state_data.data)
      {
        this.setState({
          news_list:state_data.data.list,
          news_count:state_data.data.count.prev
        })
      }
    }

    onDigital()
    {
      let state_data = this.digital_store.getState()
      if(state_data.data)
      {
        this.setState({
          digital_list:state_data.data.list,
          digital_count:state_data.data.count.prev
        })
      }
    }

    onViral()
    {
      let state_data = this.viral_store.getState()
      if(state_data.data)
      {
        this.setState({
          viral_list:state_data.data.list,
          viral_count:state_data.data.count.prev
        })
      }
    }



    graphMap(array_list)
    {
      var list  = []
      for (var key in array_list) {
            list.push({ date: moment(key, "YYYY-MM-DD").format("MMM D, YYYY"), Count: array_list[key]},)
      }
      return list
    }



  render() {
    if(this.state.total.scores !== undefined && this.state.total !== [] )
    {
            var last = 0
            var first = 0
            var s_last = 0
            var s_first = 0
            var  count = 0
            for (var key in this.state.total.scores)
            {
                  var score = this.state.total.scores[key]
                  var rank  = this.state.rank.ranks[key]
                  if(count ===0 ){
                    s_first = this.state.total.scores[key]
                    first  = this.state.rank.ranks[key]
                    count = 1
                  }
                  s_last = this.state.total.scores[key]
                  last  = this.state.rank.ranks[key]
            }
            var score_change = s_last - s_first
            var rank_change = -(last - first)
    }

    var news = {"title":"Newspaper ST Score","data":this.state.newspaper,"text":'Shows your Newspaper ST Score on '+moment(this.state.cycle.end, "YYYY-MM-DD").format("MMM D, YYYY")+" ,Change is shown in comparison with score of "+moment(this.state.cycle.start, "YYYY-MM-DD").format("MMM D, YYYY")}
    var digital = {"title":"Digital News ST Score","data":this.state.digitalnews,"text":'Shows your Digital News ST Score on'+moment(this.state.cycle.end, "YYYY-MM-DD").format("MMM D, YYYY")+" ,Change is shown in comparison with score of "+moment(this.state.cycle.start, "YYYY-MM-DD").format("MMM D, YYYY")}
    var viral = {"title":"Viral News ST Score","data":this.state.viralnews,'text':'Shows your Viral News ST Score on'+moment(this.state.cycle.end, "YYYY-MM-DD").format("MMM D, YYYY")+" ,Change is shown in comparison with score of "+moment(this.state.cycle.start, "YYYY-MM-DD").format("MMM D, YYYY")}
    var socialfb = {"title":"Facebook ST Score","data":this.state.facebook,'text':'Shows your Facebook ST Score on'+moment(this.state.cycle.end, "YYYY-MM-DD").format("MMM D, YYYY")+" ,Change is shown in comparison with score of "+moment(this.state.cycle.start, "YYYY-MM-DD").format("MMM D, YYYY")}
    var socialtw = {"title":"Twitter ST Score","data":this.state.twitter,'text':'Shows your Twitter ST Score on'+moment(this.state.cycle.end, "YYYY-MM-DD").format("MMM D, YYYY")+" ,Change is shown in comparison with score of "+moment(this.state.cycle.start, "YYYY-MM-DD").format("MMM D, YYYY")}


    if(this.state.facebook !== undefined && this.state.facebook !== [] )
    {
            var list = []
            let last = 0
            let first = 0
            let count = 0
            for (let key in this.state.facebook.scores)
            {
                  list.push({ date: moment(key, "YYYY-MM-DD").format("MMM D, YYYY"), Rank: this.state.facebook.scores[key]},)
                  if(count === 0 ){
                    first = this.state.facebook.ranks[key]
                    count = 1

                  }
                  last = this.state.facebook.ranks[key]
            }
            var fb_rank = this.state.facebook.latest_rank
            var fb_rank_change = (last - first)
            // this.setState({
            //   cycle:{"start":first_date,"end":last_date}
            // })
    }

    var t_list = []
    if(this.state.twitter !== undefined && this.state.twitter !== [] )
    {
            let last = 0
            let first = 0
            let count = 0
            for (let key in this.state.twitter.scores)
            {
                  t_list.push({ date: moment(key, "YYYY-MM-DD").format("MMM D, YYYY"), Rank: this.state.twitter.scores[key]},)

                  if(count === 0 ){
                    first = this.state.twitter.ranks[key]
                    count = 1
                  }
                  last = this.state.twitter.ranks[key]

            }
            var tw_rank = this.state.twitter.latest_rank
            var tw_rank_change =   (last - first)

    }


    return (
      <div className="side-pad stack">

        <Grid stackable celled>
        <Grid.Row>
          <Grid.Column width={10} >
          <Grid.Row>
              <MainRank total={this.state.total} rank={this.state.rank} type={"rank"} date={this.props.date} date2={this.state.cycle}/>
          </Grid.Row>
          <Grid.Row className={"build-row"}>
              <MainRank2 total={this.state.total} rank={this.state.rank} type={"score"} date={this.props.date} date2={this.state.cycle}/>
          </Grid.Row>
          </Grid.Column>
          <Grid.Column width={6} className="pi-div">
          <Grid.Row style={{'height':'30%'}}>
          <Grid columns='two' style={{'height':'100%'}} divided>
          <Grid.Column>
          <div className="og-div"><span className="card-span" style={{"font-size":"1em"}}>ST Rank</span></div>
          <div className="og-div hike omega-pad"><span className="digit-span-card" style={{"font-size":"5em"}}>{rank}</span> <span style= {{'color':rank_change < 0?'red':"green",'display':rank_change ===0?'none':""}}><Icon name={rank_change < 0?'caret down':"caret up"}/> { Math.abs(Math.round(rank_change * 100) / 100) }</span></div>
          </Grid.Column>
          <Grid.Column style={{"border-left": "1px solid #d4d4d5"}}>
          <div className="og-div"><span className="card-span" style={{"font-size":"1em"}}>ST Score</span></div>
          <div className="og-div hike omega-pad"><span className="digit-span-card" style={{"font-size":"3em"}}>{score}</span> <span style={{'color':score_change < 0?'red':"green",'display':score_change ===0?'none':""}}><br/><Icon name={score_change < 0?'caret down':"caret up"}/> { Math.abs(Math.round(score_change * 100) / 100) }</span></div>
          </Grid.Column>
          </Grid>
          </Grid.Row>
          <Grid.Row style={{'height':'70%'}} className={"build-row"} >
              <PieRank data={this.state} date={this.props.date} date2={this.state.cycle}/>
          </Grid.Row>
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <Grid stackable celled>
            <Cards format={news} news={this.state.news_list} graph_data={this.state.news_graph} count= {this.state.news_count} graphMap={this.graphMap.bind(this)} pad={"0em"} color={"#fa9c30"} type={"Newspaper"} key ={"n345"} date={this.props.date} date2={this.state.cycle}/>
        </Grid>
        <Grid stackable celled>
            <CardD format={digital} news={this.state.digital_list} graph_data={this.state.digital_graph}  count= {this.state.digital_count} graphMap={this.graphMap.bind(this)} color={'#9650b0'} type={"Digital News"} key = {"kdf4"} date={this.props.date} date2={this.state.cycle}/>
        </Grid>
        <Grid stackable celled>
            <CardV format={viral} news={this.state.viral_list} graph_data={this.state.viral_graph} count= {this.state.viral_count} graphMap={this.graphMap.bind(this)} color={"#f45c27"} type={"Viral News"} key={"sdf"} date={this.props.date} date2={this.state.cycle}/>
        </Grid>

        <Grid stackable  columns='two' celled >
          <Grid.Row >
          <Grid.Column width={7}>
            <AreaGraph format={socialfb} color={"#4267b2"}/>
          </Grid.Column>
          <Grid.Column width={9} >
          <Grid columns={3} divided>
          <Grid.Column>
          <div className="og-div"><span className="card-span">Rank<HelpText data={'Shows your Facebook ST Rank on '+moment(this.state.cycle.end, "YYYY-MM-DD").format("MMM D, YYYY")+" ,Change is shown in comparison with rank of "+moment(this.state.cycle.start, "YYYY-MM-DD").format("MMM D, YYYY") }/></span></div>
          <div className="og-div hike omega-2"><span className="digit-span-card" >{fb_rank}</span> <span style={{'color':fb_rank_change < 0?'red':"green",'display':fb_rank_change ===0?'none':""}}> <Icon name={fb_rank_change < 0?'caret down':"caret up"}/> {Math.abs(fb_rank_change)}</span></div>
          <ResponsiveContainer width='100%' aspect={4.0/.7}>
          <AreaChart width={200} height={60} data={list}
                margin={{top: 5, right: 0, left: 0, bottom: 5}}>
            <Area type='monotone' dataKey='Rank' stroke='#3b5998' fill='#4267b2' fillOpacity="1" />
          </AreaChart>
          </ResponsiveContainer>
          </Grid.Column>
          <Grid.Column>
          <div className="og-div"><span className="card-span">Page Likes<HelpText data={"Shows your latest page likes count from "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY") +" to "+moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY") }/></span></div>
          <div className="og-div hike omega-2"><span className="digit-span-card">{ Math.round(this.state.likes/1000000 * 100) / 100 } M  </span> <span style={{'color':this.state.likes_change < 0?'red':"green"}}><Icon name={this.state.likes_change < 0?'caret down':"caret up"}/> { this.state.likes_change?this.state.likes_change/1000:0 } K</span></div>
          <ResponsiveContainer width='100%' aspect={4.0/.7}>
          <AreaChart width={200} height={60} data={this.state.page_data}
                margin={{top: 5, right: 0, left: 0, bottom: 5}}>
            <Area type='monotone' dataKey='Count' stroke='#3b5998' fill='#4267b2' fillOpacity="1" />
          </AreaChart>
          </ResponsiveContainer>
          </Grid.Column>
          <Grid.Column>
          <div className="og-div"><span className="card-span">Posts<HelpText data={"Shows total posts made from  "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY") +" to "+moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY") }/></span></div>
          <div className="og-div hike omega-2"><span className="digit-span-card">{this.state.post}</span> <span style={{'color':this.state.post_change < 0?'red':"green",'display':this.state.post_change ===0?'none':""}}><Icon name={this.state.post_change < 0?'caret down':"caret up"}/>  { Math.abs(this.state.post_change) }</span></div>

          <ResponsiveContainer width='100%' aspect={4.0/.7}>
          <AreaChart width={200} height={60} data={this.state.post_data}
                margin={{top: 5, right: 0, left: 0, bottom: 5}}>
            <Area type='monotone' dataKey='Count' stroke='#3b5998' fill='#4267b2' fillOpacity="1"  />
          </AreaChart>
          </ResponsiveContainer>




          </Grid.Column>
          </Grid>
          <Grid columns={3} divided className={"build"}>
          <Grid.Column>
          <div className="og-div"><span className="card-span">Post Likes<HelpText data={"Shows total post likes received from "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY") +" to "+moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY") }/></span></div>
          <div className="og-div hike omega-2"><span className="digit-span-card">{ Math.abs( Math.round(this.state.fb_likes/1000 * 100) / 100) } K  </span> <span style={{'color':this.state.change_likes < 0?'red':"green"}}> <Icon name={this.state.change_likes < 0?'caret down':"caret up"}/>  { Math.abs( Math.round(this.state.change_likes/1000 * 100) / 100) } K  </span></div>
          <ResponsiveContainer width='100%' aspect={4.0/.7}>
          <AreaChart width={200} height={60} data={this.state.fb_likes_data}
                margin={{top: 5, right: 0, left: 0, bottom: 5}}>
            <Area type='monotone' dataKey='Count' stroke='#3b5998' fill='#4267b2'  fillOpacity="1"/>
          </AreaChart>
          </ResponsiveContainer>


          </Grid.Column>
          <Grid.Column>
          <div className="og-div"><span className="card-span">Shares<HelpText data={"Shows total shares received from "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY") +" to "+moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY") }/></span></div>
          <div className="og-div hike omega-2"><span className="digit-span-card">{this.state.shares}</span> <span style={{'color':this.state.share_change < 0?'red':"green"}}><Icon name={this.state.share_change < 0?'caret down':"caret up"}/> { Math.abs(this.state.share_change) }</span></div>
          <ResponsiveContainer width='100%' aspect={4.0/.7}>
          <AreaChart width={200} height={60} data={this.state.share_data}
                margin={{top: 5, right: 0, left: 0, bottom: 5}}>
            <Area type='monotone' dataKey='Count' stroke='#3b5998' fill='#4267b2' fillOpacity="1" />
          </AreaChart>
          </ResponsiveContainer>
          </Grid.Column>
          </Grid>
          </Grid.Column>
          </Grid.Row>
          <Grid.Row className="pad-row" celled>
          <Grid.Column width={7}>
          <AreaGraph format={socialtw} color={"#1DA1F2"}/>
          </Grid.Column>
          <Grid.Column width={9}>
          <Grid columns={3} divided>
          <Grid.Column>
          <div className="og-div"><span className="card-span">Rank<HelpText data={'Shows your Twitter ST Rank on '+moment(this.state.cycle.end, "YYYY-MM-DD").format("MMM D, YYYY")+" ,Change is shown in comparison with score of "+moment(this.state.cycle.start, "YYYY-MM-DD").format("MMM D, YYYY") }/></span></div>
          <div className="og-div hike omega-2"><span className="digit-span-card">{tw_rank}</span> <span style={{'color':tw_rank_change < 0?'red':"green",'display':tw_rank_change ===0?'none':""}}><Icon name={tw_rank_change< 0?'caret down':"caret up"}/> { Math.abs(tw_rank_change) }</span></div>
          <ResponsiveContainer width='100%' aspect={4.0/.7}>
          <AreaChart width={200} height={60} data={t_list}
                margin={{top: 5, right: 0, left: 0, bottom: 5}}>
            <Area type='monotone' dataKey='Rank' stroke='#1DA1F2' fill='#1DA1F2'  fillOpacity="1"/>
          </AreaChart>
          </ResponsiveContainer>
          </Grid.Column>
          <Grid.Column>

          <div className="og-div"><span className="card-span">Followers<HelpText data={'Shows your Followers count  on '+moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY")+" ,Change is shown in comparison with score of "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY") }/></span></div>
          <div className="og-div hike omega-2"><span className="digit-span-card"> { Math.round(this.state.tw_followers/1000000 * 100) / 100 } M </span> <span style={{'color':this.state.tw_change_followers < 0?'red':"green"}}><Icon name={this.state.tw_change_followers< 0?'caret down':"caret up"}/> { Math.abs( Math.round(this.state.tw_change_followers/1000 * 100) / 100) } K</span></div>

          <ResponsiveContainer width='100%' aspect={4.0/.7}>
          <AreaChart width={200} height={60} data={this.state.tw_followers_redata}
                margin={{top: 5, right: 0, left: 0, bottom: 5}}>
            <Area type='monotone' dataKey='Count' stroke='#1DA1F2' fill='#1DA1F2' fillOpacity="1" />
          </AreaChart>
          </ResponsiveContainer>
          </Grid.Column>
          <Grid.Column>
          <div className="og-div"><span className="card-span">Tweets<HelpText data={"Shows total tweets made from "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY") +" to "+moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY") }/></span></div>
          <div className="og-div hike omega-2"><span className="digit-span-card">{this.state.tw_tweets?this.state.tw_tweets:0}</span> <span style={{'color':this.state.tw_change_tweets < 0?'red':"green"}}><Icon name={this.state.tw_change_tweets< 0?'caret down':"caret up"}/> { Math.abs(this.state.tw_change_tweets)}</span></div>
          <ResponsiveContainer width='100%' aspect={4.0/.7}>
          <AreaChart width={200} height={60} data={this.state.tw_tweets_redata}
                margin={{top: 5, right: 0, left: 0, bottom: 5}}>
            <Area type='monotone' dataKey='Count' stroke='#1DA1F2' fill='#1DA1F2' fillOpacity="1"/>
          </AreaChart>
          </ResponsiveContainer>
          </Grid.Column>
          </Grid>
          <Grid columns={3}  divided className={"build"}>
            <Grid.Column >
              <div className="og-div"><span className="card-span">Favorites<HelpText data={"Shows total favorites received from "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY") +" to "+moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY") }/></span></div>
              <div className="og-div hike omega-2"><span className="digit-span-card">{ Math.abs( Math.round(this.state.tw_likes/1000 * 100) / 100) } K </span> <span style={{'color':this.state.tw_change_likes< 0?'red':"green"}}> <Icon name={this.state.tw_change_likes< 0?'caret down':"caret up"}/>  { Math.abs( Math.round(this.state.tw_change_likes/1000 * 100) / 100) } K </span></div>
              <ResponsiveContainer width='100%' aspect={4.0/.7}>
              <AreaChart width={200} height={60} data={this.state.tw_likes_data}
                    margin={{top: 5, right: 0, left: 0, bottom: 5}}>
                <Area type='monotone' dataKey='Count' stroke='#1DA1F2' fill='#1DA1F2' fillOpacity="1" />
              </AreaChart>
              </ResponsiveContainer>
            </Grid.Column>
            <Grid.Column>
              <div className="og-div"><span className="card-span">Retweets<HelpText data={"Shows total retweets received from "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY") +" to "+moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY") }/></span></div>
              <div className="og-div hike omega-2"><span className="digit-span-card">{this.state.tw_retweets}</span> <span style={{'color':this.state.tw_change_retweets < 0?'red':"green"}}><Icon name={this.state.tw_change_retweets< 0?'caret down':"caret up"}/> { Math.abs(this.state.tw_change_retweets) }</span></div>
              <ResponsiveContainer width='100%' aspect={4.0/.7}>
              <AreaChart width={200} height={60} data={this.state.tw_tweets_redata}
                    margin={{top: 5, right: 0, left: 0, bottom: 5}}>
                <Area type='monotone' dataKey='Count' stroke='#1DA1F2' fill='#1DA1F2' fillOpacity="1" />
              </AreaChart>
              </ResponsiveContainer>
            </Grid.Column>
          </Grid>
          </Grid.Column>
          </Grid.Row>

        </Grid>

      </div>
    )
  }
}

export default Total;
