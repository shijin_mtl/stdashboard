import React, { Component } from 'react'
import { Grid,Icon } from 'semantic-ui-react'
import {  AreaChart, XAxis, Tooltip, ResponsiveContainer,Area} from 'recharts';
import HelpText from "../HelpText"
import moment from 'moment'



class AreaGraph extends Component {
  render() {

    var list = []
    if(this.props.format.data !== undefined && this.props.format.data !== [] )
    {
            var last = 0
            var first =  0
            var count =0
            for (var key in this.props.format.data.scores) {
                  list.push({ date: moment(key, "YYYY-MM-DD").format("MMM D, YYYY"), Score: this.props.format.data.scores[key]},)
                  var score = this.props.format.data.scores[key]
                  if(count===0){
                    first = this.props.format.data.scores[key]
                    count = 1
                  }
                  last = this.props.format.data.scores[key]
            }
    }
    var change = last - first





    return (
            <Grid.Column>
              <Grid.Row>
              <div className="og-div"><span className="card-span">{this.props.format.title} <HelpText data={this.props.format.text}/></span></div>
              <div className="og-div hike"><span className="digit-span-card">{score}</span> <span style={{'color':change < 0?'red':"green",'display':change === 0?'none':""}}> <Icon name={change < 0?'caret down':"caret up"}/> { Math.abs(Math.round(change * 100) / 100)}</span></div>
              </Grid.Row>
              <Grid.Row>
              <ResponsiveContainer width='100%'  aspect={4.0/1.2}>
                <AreaChart data={list} syncId="mainGraph"
                        namemargin={{ top:0, right: 0, left: 10, bottom:0 }}
                      >
                        <defs>
                      <linearGradient id="MyGradient" x1="0" y1="0" x2="0" y2="1">
                        <stop offset="10%" stopColor="rgba(0, 136, 254, 0.8)" />
                      </linearGradient>
                    </defs>
                    <XAxis dataKey="date" hide={true} />
                    {/*<YAxis dataKey="Score" />*/}
                    <Tooltip   cursor={false} />
                    <Area
                      type="monotone"
                      dataKey="Score"
                      stroke="#0088FE"
                      strokeWidth="0"
                      fillOpacity="1"
                      fill={this.props.color}
                      dot={false}
                    />
                  </AreaChart>
              </ResponsiveContainer>
              </Grid.Row>
            </Grid.Column>
    )
  }
}

export default AreaGraph;
