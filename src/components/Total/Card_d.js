import React, { Component } from 'react'
import { Grid,Icon } from 'semantic-ui-react'
import {  LineChart, Line, XAxis, Tooltip, ResponsiveContainer,AreaChart,Area} from 'recharts';
import HelpText from "../HelpText"
import moment from 'moment'





class CardD extends Component {

  render() {

    var list = []
    var rank_list = []
    var last =0
    var first = 0
    var count = 0


    var s_last =0
    var s_first = 0
    var s_count = 0


    if(this.props.format.data !== undefined && this.props.format.data !== [] && this.props.format.data.latest_rank )
    {
            for (var key in this.props.format.data.scores) {
                  list.push({ date: moment(key, "YYYY-MM-DD").format("MMM D, YYYY"), Score: this.props.format.data.scores[key]},)
                  var score = this.props.format.data.scores[key]
                  if (s_count === 0)
                  {
                    s_first = this.props.format.data.scores[key]
                    s_count = 1
                  }
                  s_last  = this.props.format.data.scores[key]
            }

            for (let key in this.props.format.data.ranks) {
                  rank_list.push({ date: moment(key, "YYYY-MM-DD").format("MMM D, YYYY"), Rank: this.props.format.data.scores[key]},)
                  if (count === 0)
                  {
                    first = this.props.format.data.ranks[key]
                    count = 1
                  }
                  last  = this.props.format.data.ranks[key]
            }

            var latest = this.props.format.data.latest_rank
            var rank_change =  -(last - first)
    }



    var story_sum = 0
    var story_change = 0
    if(this.props.graph_data)
    {
        story_sum = this.props.graph_data.sum
        story_change = this.props.graph_data.change_in_scores

        var last2  =0
        var first2 = 0
        var count2 = 0
        for (var key2 in this.props.graph_data.scores) {
          if(count2===0){
            first2 = this.props.graph_data.scores[key2]
            count2 = 1
          }
          last2 = this.props.graph_data.scores[key2]

        }
        story_change = last2 - first2
    }
    if(this.props.news)
    {
      // var second_total,last_total
      var keys = Object.keys(this.props.news);
      let last = keys[keys.length-1];
      var total_i = 0
      let count = 0
      this.props.news.map((data,index) => {
                            if(count === 0){
                              // second_total = data.Total
                              count = 1
                            }
                            else if (parseInt(index,10) === parseInt(last,10)) {
                              // last_total = data.Total
                            }
                            total_i = total_i+data.Total
                            return true
                        });
     }


    var vol_change =   this.props.count

    var change = s_last - s_first


    return (
      <Grid.Row style={{'padding-top':this.props.pad}}>
            {/*<Grid.Column width={7} style={{'background':this.props.color}}>*/}
            <Grid.Column width={7}>
              <Grid.Row>
              <div className="og-div" ><span className="card-span">{this.props.format.title}<HelpText data={this.props.format.text}/></span></div>
              <div className="og-div hike" ><span className="digit-span-card" >{score}</span> <span style={{'color':change < 0?'red':"green",'display':change === 0?'none':""}}><Icon name={change < 0?'caret down':"caret up"}/> { Math.abs(Math.round(change * 100) / 100) }</span></div>
              </Grid.Row>
              <Grid.Row>
              <ResponsiveContainer width='100%' aspect={4.0/.7}>
                <LineChart syncId="mainGraph"
                    data={list}
                    margin= {{ top: 5, right: 20, bottom: 0, left: 20 }}
                      >
                    <Tooltip  itemStyle={{ 'color':this.props.color }} cursor={false}/>
                    <XAxis dataKey='date' hide={true}  />
                    <Line type='monotone' dataKey='Score' strokeWidth={3} stroke={this.props.color}  />
                  </LineChart>
              </ResponsiveContainer>
              </Grid.Row>
            </Grid.Column>
            <Grid.Column width={3}>
            <Grid.Row>
            <div className="og-div"><span className="card-span">Rank<HelpText data={'Shows your '+this.props.type+'Rank on '+moment(this.props.date2.end, "YYYY-MM-DD").format("MMM D, YYYY")+" ,Change is shown in comparison with rank of "+moment(this.props.date2.start, "YYYY-MM-DD").format("MMM D, YYYY")}/></span></div>
            <div className="og-div hike omega"><span className="digit-span-card">{latest}</span> <span style={{'color':rank_change < 0?'red':"green",'display':rank_change === 0?'none':""}}><Icon name={rank_change < 0?'caret down':"caret up"}/>{ Math.abs(Math.round(rank_change * 100) / 100) }</span></div>
            <ResponsiveContainer width='100%' aspect={4.0/.7}>
            <AreaChart width={200} height={60} data={rank_list}
                  margin={{top: 5, right: 0, left: 0, bottom: 5}}>
              <Area type='monotone' dataKey='Rank' stroke={this.props.color} fill={this.props.color}  fillOpacity="1"/>
            </AreaChart>
            </ResponsiveContainer>
            </Grid.Row>
            </Grid.Column>
            <Grid.Column width={3}>
            <Grid.Row>
            <div className="og-div"><span className="card-span">Publications<HelpText data={" Showing count of publications from "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")+" to "+moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY")+" in comparison with publications from "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")+" to "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")}/></span></div>
            <div className="og-div hike omega"><span className="digit-span-card">{total_i}</span> <span style={{'color':vol_change < 0?'red':"green",'display':!vol_change?'none':""}}><Icon name={vol_change < 0?'caret down':"caret up"}/> {Math.abs(Math.round(vol_change * 100) / 100) }%</span></div>
            <ResponsiveContainer width='100%' aspect={4.0/.7}>
            <AreaChart width={200} height={60} data={this.props.news}
                  margin={{top: 5, right: 0, left: 0, bottom: 5}}>

              <Area type='monotone' dataKey='Total' stroke={this.props.color} fill={this.props.color} fillOpacity="1" />
            </AreaChart>
            </ResponsiveContainer>
            </Grid.Row>
            </Grid.Column>
            <Grid.Column width={3}>
            <Grid.Row>
            <div className="og-div"><span className="card-span">Stories<HelpText data={" Showing Stories from "+moment(this.props.date2.start, "YYYY-MM-DD").format("MMM D, YYYY")+" to "+moment(this.props.date2.end, "YYYY-MM-DD").format("MMM D, YYYY")+" in comparison with Stories from "+moment(this.props.date2.start, "YYYY-MM-DD").format("MMM D, YYYY")+" to "+moment(this.props.date2.start, "YYYY-MM-DD").format("MMM D, YYYY")}/></span></div>
            <div className="og-div hike omega"><span className="digit-span-card">{story_sum}</span> <span style={{'color': story_change < 0?'red':"green"}}><Icon name={story_change < 0?'caret down':"caret up"}/> { Math.abs(Math.round(story_change * 100) / 100) }</span></div>
            </Grid.Row>
            <ResponsiveContainer width='100%' aspect={4.0/.7}>
            <AreaChart width={200} height={60} data={this.props.graphMap(this.props.graph_data?this.props.graph_data.scores:[])}
                  margin={{top: 5, right: 0, left: 0, bottom: 5}}>
              <Area type='monotone' dataKey='Count' stroke={this.props.color} fill={this.props.color} fillOpacity="1" />
            </AreaChart>
            </ResponsiveContainer>
            </Grid.Column>
            </Grid.Row>
    )
  }
}


export default CardD;
