import React, { Component } from 'react'
import { Form,Grid,Table,Image,Header,Icon,Menu,Sticky,Loader,Radio} from 'semantic-ui-react'
import { newsStore, newsFetch,entityFetch,sourceFetch } from '../../store/newsStore'
// import { newspaperStore,getVolume,getSourceVolume } from '../../store/newspaperStore'

import { compareStore,scoreFetch } from '../../store/compareStore'
import TinyGraph from './TinyGraph'
import TinyGraphRank from './TinyGraphRank'
import moment from 'moment'
import HelpText from "../HelpText"
import { totalStore,totalFetch } from '../../store/totalStore'




// import BarCard from './BarCard'
// import { newspaperStore as newspaperStore2 ,getVolume as getVolume2, getSourceVolume as getSourceVolume2 } from '../../store/newspaperStore'
// import { newspaperStore as newspaperStore3 ,getVolume as getVolume3, getSourceVolume as getSourceVolume3 } from '../../store/newspaperStore'
// import { newspaperStore as newspaperStore4 ,getVolume as getVolume4, getSourceVolume as getSourceVolume4 } from '../../store/newspaperStore'
// import { newspaperStore as newspaperStore6 ,getVolume as getVolume6, getSourceVolume as getSourceVolume6 } from '../../store/newspaperStore'
// import { newspaperStore as newspaperStore7 ,getVolume as getVolume7, getSourceVolume as getSourceVolume7 } from '../../store/newspaperStore'
// import {AreaChart,Area,ResponsiveContainer } from 'recharts';







import Articles from './Article'
// import  People from './Peoples'
// import MainRank from './MainGraph'
// import LineCard from './LineCard'
import AreaCard from './AreaCard'
import PubCard from './PubCard'
import FbSocial from './FbSocial'
import Paper from './TopPaperList'
import People from './TopPeopleList'


// import AreaCardDigital from './AreaCardDigital'
// import AreaViral from './AreaCardViral'

// import Paper from "./TopPaper"





class HeadToHead extends Component {
  constructor(props) {
    super(props)
    this.store = newsStore()
    this.store2 = totalStore()
    // this.storeVolume = newspaperStore()
    // this.storeVolume2 = newspaperStore2()
    // this.storeVolume3 = newspaperStore3()
    // this.storeVolume4 = newspaperStore4()

    // this.storeVolume6 = newspaperStore6()
    // this.storeVolume7 = newspaperStore7()
    this.storeCompare = compareStore()



    this.state = {
      loading: false,
      data:[],
      track: [],
      hasMoreItems:true,
      content:"",
      type:this.props.match.params.type==='3'?"viralnews":(this.props.match.params.type==='2'?'digital_news':"newspaper"),
      b_type:"total",
      sort:"desc",
      entity:[],
      list_text:"",
      source_list:[],
      entity_value:this.props.match.params.entity?[parseInt(this.props.match.params.entity,10)]:[],
      source_value:this.props.match.params.source?[parseInt(this.props.match.params.source,10)]:[],
      list_entity:[],
      current_id:false,
      current_name:"",
      volume:false,
      volume2:false,
      volume3:false,
      volume4:false,
      volume6:false,
      volume7:false,
      source_volume:[],
      source_volume2:[],
      source_volume3:[],
      source_volume4:[],
      source_volume6:[],
      source_volume7:[],
      count:0,
      entity_list:[{'name':localStorage.name,"id":localStorage.id}],
      entity_id_list:[parseInt(localStorage.id,10)],
      rank_list:[],
      search_term:"",
      show_graph:false,
      last_cycle:"",
      first_cyle:""
    }
  }

  componentWillMount() {
    this.store.subscribe(this.onFetchProcess.bind(this))
    this.store2.subscribe(this.onCycleDate.bind(this))

    // this.storeVolume.subscribe(this.onNewsProcess.bind(this))
    // this.storeVolume2.subscribe(this.onNewsProcess2.bind(this))
    // this.storeVolume3.subscribe(this.onNewsProcess3.bind(this))
    // this.storeVolume4.subscribe(this.onNewsProcess4.bind(this))
    // this.storeVolume6.subscribe(this.onNewsProcess6.bind(this))
    // this.storeVolume7.subscribe(this.onNewsProcess7.bind(this))
    this.storeCompare.subscribe(this.onCompareProcess.bind(this))
    this.store.dispatch(entityFetch())
    this.store.dispatch(sourceFetch())
    this.storeCompare.dispatch(scoreFetch({ 'entity':this.state.entity_id_list,"start_date":this.props.date.start,"end_date":this.props.date.end
    }))

    this.store2.dispatch(totalFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"id":this.props.current_id}))

  }


  onCycleDate=()=>{

    const state2 = this.store2.getState()
    if(state2.success && state2.data.rank)
    {
      var count = 0
      var  first = ""
        for (let key in state2.data.rank.ranks){
          if(count===0){
            first = moment(key, "YYYY-MM-DD").format("MMM D, YYYY")
            count = 1
          }
          let date = moment(key, "YYYY-MM-DD").format("MMM D, YYYY")
          this.setState({
            last_cycle:date,
            first_cyle:first
          })
        }

    }
  }

  onCompareProcess(response){
          const comapareState = this.storeCompare.getState()
          var  LoaderExampleInlineCentered = () => (
            <Grid.Column className={'text-center-pad mid font'} style={{'color':'red'}}>
              <Loader active inline='centered' size='mini' style={{    "margin-top": "1%"}} />
            </Grid.Column>
          )
          var temp_load = []
          for (let k=0;k<this.state.count+2;k++){
            temp_load.push(<LoaderExampleInlineCentered/>)
          }
          this.setState({
            graph_score:temp_load
          })

          if(comapareState.success)
          {
              var score_o = []
              for(let o = 0;o<this.state.entity_list.length;o++)
              {
                  for(  var i = comapareState.data[0].series.length; i-- > 0; )
                  {
                    if(this.state.entity_list[o].name === comapareState.data[0].series[i].name){
                      score_o.push(<TinyGraph data={comapareState.data[0].series[i]} key={this.state.entity_list[o].name} show={this.state.show_graph}/> )
                    }
                  }


              }
              this.setState({
                graph_score:score_o
              })
              comapareState.success = false
          }
  }

  onNewsProcess(response){
          const state2 = this.storeVolume.getState()
          switch(state2.type) {
          case "volume":
                if(state2.success)
                {
                  this.setState({
                    volume: state2.data.data
                  })
                }

              break;
          case "source_volume":
                if(state2.success)
                {

                  this.setState({
                    source_volume:state2.data
                  })
                }
              break;
          default:
              ;
      }
  }

  onNewsProcess2(response){
          const state3 = this.storeVolume2.getState()
          switch(state3.type) {
          case "volume":
                if(state3.success)
                {
                  this.setState({
                    volume2:state3.data.data
                  })
                }
              break;

          case "source_volume":
                if(state3.success)
                {

                  this.setState({
                    source_volume2:state3.data
                  })
                }
              break;
          default:
              ;
      }
  }


  onNewsProcess3(response){
          const state4 = this.storeVolume3.getState()
          switch(state4.type) {
          case "volume":
                if(state4.success)
                {
                  this.setState({
                    volume3:state4.data.data
                  })
                }
              break;
          case "source_volume":
                if(state4.success)
                {

                  this.setState({
                    source_volume3:state4.data
                  })
                }
              break;
          default:
              ;
      }
  }


  onNewsProcess4(response){
          const state5 = this.storeVolume4.getState()
          switch(state5.type) {
          case "volume":
                if(state5.success)
                {

                  this.setState({
                    volume4:state5.data.data
                  })
                }
              break;

          case "source_volume":
                if(state5.success)
                {
                  this.setState({
                    source_volume4:state5.data
                  })
                }
              break;
          default:
              ;
      }
  }

  onNewsProcess6(response){
          const state6 = this.storeVolume6.getState()
          switch(state6.type) {
          case "volume":
                if(state6.success)
                {

                  this.setState({
                    volume6:state6.data.data
                  })
                }
              break;

          case "source_volume":
                if(state6.success)
                {
                  this.setState({
                    source_volume6:state6.data
                  })
                }
              break;
          default:
              ;
      }
  }

  onNewsProcess7(response){
          const state7 = this.storeVolume7.getState()
          switch(state7.type) {
          case "volume":
                if(state7.success)
                {

                  this.setState({
                    volume7:state7.data.data
                  })
                }
              break;
          case "source_volume":
                if(state7.success)
                {
                  this.setState({
                    source_volume7:state7.data
                  })
                }
              break;
          default:
              ;
      }
  }

  handleContextRef = contextRef => this.setState({ contextRef })

  componentWillReceiveProps(nextProps)
  {
    // this.storeVolume.dispatch(getVolume({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
    // "type":"newspaper"}))
    // this.storeVolume2.dispatch(getVolume2({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
    // "type":"newspaper","id":this.state.current_id}))
    //
    // this.storeVolume3.dispatch(getVolume3({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
    // "type":"digitalnews"}))
    //
    // this.storeVolume4.dispatch(getVolume4({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
    // "type":"digitalnews","id":this.state.current_id}))
    //
    // this.storeVolume6.dispatch(getVolume6({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
    // "type":"viralnews"}))
    //
    // this.storeVolume7.dispatch(getVolume7({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
    // "type":"viralnews","id":this.state.current_id}))
    //
    //
    //
    //
    // this.storeVolume.dispatch(getSourceVolume({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
    // "type":"newspaper"}))
    //
    // this.storeVolume2.dispatch(getSourceVolume2({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
    // "type":"newspaper","id":this.state.current_id}))
    //
    // this.storeVolume3.dispatch(getSourceVolume3({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
    // "type":"digital_news"}))
    //
    // this.storeVolume4.dispatch(getSourceVolume4({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
    // "type":"digital_news","id":this.state.current_id}))
    //
    //
    //
    // this.storeVolume6.dispatch(getSourceVolume6({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
    // "type":"viralnews"}))
    //
    // this.storeVolume7.dispatch(getSourceVolume7({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
    // "type":"viralnews","id":this.state.current_id}))


    this.storeCompare.dispatch(scoreFetch({ 'entity':this.state.entity_id_list,"start_date":nextProps.date.start,"end_date":nextProps.date.end
    }))

    this.store2.dispatch(totalFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"id":this.state.current_id}))



      this.setState({
      date:nextProps.date,
      track: [],
      list_text:""
      })
  }


  onFetchProcess = () => {
    var list_entity1= []
    const state = this.store.getState()
    if(state.success === true)
    {

      if(state.type === "entity")
      {
        var trim = JSON.parse(JSON.stringify(state.data).split('"id":').join('"value":').split('"name":').join('"text":'));
        this.setState({
          entity:trim
        })
        for (let i = 0; i < trim.length; i++)
        {
          list_entity1.push(
            <Table.Row>
              <Table.Cell>
                <Header as='h4' image>
                  <Image src={"https://s3-us-west-2.amazonaws.com/interface-images/"+trim[i].value+".jpg"} rounded size='mini' />
                </Header>
              </Table.Cell>
              <Table.Cell>
              <Header.Content >
                  {trim[i].text}
              </Header.Content>
              </Table.Cell>
              <Table.Cell>
              <Icon name='plus' size='large'/>
              </Table.Cell>
            </Table.Row>
          )
        }
        this.setState({
          list_entity:list_entity1
        })


      }

      if(state.type === "read")
      {
        this.setState({
          source_list:state.data.list
        })
      }


      if(state.type === "news")
      {
      var tracks=this.state.track
      if(state.data.articles)
      {
          state.data.articles.map((data) => {
                    tracks.push(data)
                    return true;
                });

        this.setState({
          track:tracks,
          hasMoreItems:true
          })
          if(state.data.articles.length === 0){
            this.setState({
              hasMoreItems:false,
              list_text:"No More Articles"
              })
          }
      }
      else
      {
        this.setState({
          hasMoreItems:false
        })
      }
    }
    }
  }


  loadItems(page)
  {
    setTimeout(function () {
      this.setState({
        hasMoreItems:false
      });
      setTimeout(function(){ this.store.dispatch(newsFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"content":this.state.content,"entity":this.state.entity_value,
      "sort":this.state.sort,"type":this.state.type,"start":page,"source":this.state.source_value}))}.bind(this), 1000);
    }.bind(this), 1000);
  }

  contentUpdate = (type,event) =>{

    this.setState({
          [type]:event.target.value
    })
  }

  handleChangeDrop = (event) =>{
    this.setState({
      search_term:event.target.value
    })

  }

  searchClick=()=>{
    this.setState({
    track: [],
    list_text:""
    })
    this.store.dispatch(newsFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"content":this.state.content,"entity":this.state.entity_value,
    "sort":this.state.sort,"type":this.state.type,"start":0,"source":this.state.source_value}))
  }

  changetype = (value)=>{
    this.setState({
      b_type:value
    })
  }

  changeEntity=(id,name)=>{

    var temp =this.state.entity_list
    if (this.state.entity_list.length < 5) {
      this.state.entity_id_list.push(id)
      temp.push({'name':name,'id':id})
      this.setState({
        current_id:id,
        current_name:name,
        count:this.state.count+1,
        entity_list:temp,
        entity_id_list:this.state.entity_id_list
      })
      this.storeCompare.dispatch(scoreFetch({ 'entity':this.state.entity_id_list,"start_date":this.props.date.start,"end_date":this.props.date.end
      }))
    }
  }

removeEntity=(id)=>{

  var index = this.state.entity_id_list.indexOf(id);

  if (index > -1) {
      this.state.entity_id_list.splice(index, 1);
  }

  for(let i = 0;i<this.state.entity_list.length;i++)
  {
    if(this.state.entity_list[i].id===id)
    {
      this.state.entity_list.splice(i, 1);
    }
  }
 var count = this.state.count - 1
  this.setState({
    entity_id_list:this.state.entity_id_list,
    entity_list:this.state.entity_list,
    count:count,
  })

  this.storeCompare.dispatch(scoreFetch({ 'entity':this.state.entity_id_list,"start_date":this.props.date.start,"end_date":this.props.date.end
  }))

}


stickyEdge = (type) =>{
  // this.setState({
  //   class_name:type?"append-mg":""
  // })
  var element = document.getElementById("stickyDiv");
  type?element.classList.add("append-mg"):element.classList.remove("append-mg")
  var elementHead = document.getElementById("headRow");
  type?elementHead.classList.add("img-rectangular"):elementHead.classList.remove("img-rectangular")
}

  graphView=()=>{
    this.setState({
      show_graph:!this.state.show_graph
    })
  }

  render() {

    const { contextRef } = this.state
    var temp = []
    var digi_volume = (
      <AreaCard id={this.state.entity_id_list} date={this.props.date} type={'digitalnews'} key={"digitalnews"} count = {this.state.count}/>
    )

    var viral_volume = (
      <AreaCard id={this.state.entity_id_list} date={this.props.date} type={'viralnews'} key={"viralnews"} count = {this.state.count}/>
    )

    var volume =(
      <AreaCard id={this.state.entity_id_list} date={this.props.date} type={'newspaper'} key={"newspaper"} count = {this.state.count}/>
    )
    var pub_volume = (
      <PubCard id={this.state.entity_id_list} date={this.props.date} type={'viralnews'} key={"viralnews"} count = {this.state.count} cycle={{"start":this.state.first_cyle,"end":this.state.last_cycle}} />
    )
    var pub_news = (
      <PubCard id={this.state.entity_id_list} date={this.props.date} type={'newspaper'} key={"newspaper"} count = {this.state.count} cycle={{"start":this.state.first_cyle,"end":this.state.last_cycle}}/>
    )
    var pub_digi = (
      <PubCard id={this.state.entity_id_list} date={this.props.date} type={'digital_news'} key={"digitalnews"} count = {this.state.count} cycle={{"start":this.state.first_cyle,"end":this.state.last_cycle}}/>
    )
    var news_source = (
      <Paper id={this.state.entity_id_list} date={this.props.date} type={'newspaper'} key={"newspaper12"+this.state.entity_list} history={this.props.history} count = {this.state.count} cycle={{"start":this.state.first_cyle,"end":this.state.last_cycle}}/>
    )

    var digi_source = (
      <Paper id={this.state.entity_id_list} date={this.props.date} type={'digital_news'} key={"digital_news13"} history={this.props.history} count = {this.state.count} cycle={{"start":this.state.first_cyle,"end":this.state.last_cycle}}/>
    )
    var viral_source = (
      <Paper id={this.state.entity_id_list} date={this.props.date} type={'viralnews'} key={"viralnews"} history={this.props.history} count = {this.state.count} cycle={{"start":this.state.first_cyle,"end":this.state.last_cycle}}/>
    )

    var news_people = (
      <People id={this.state.entity_id_list} date={this.props.date} type={'newspaper'} key={"newspaperpeople"} history={this.props.history} count = {this.state.count} cycle={{"start":this.state.first_cyle,"end":this.state.last_cycle}}/>
    )

    var digi_people = (
      <People id={this.state.entity_id_list} date={this.props.date} type={'digital_news'} key={"digital_newspeople"} history={this.props.history} count = {this.state.count} cycle={{"start":this.state.first_cyle,"end":this.state.last_cycle}}/>
    )
    var viral_people = (
      <People id={this.state.entity_id_list} date={this.props.date} type={'viralnews'} key={"viralnewspeople"} history={this.props.history} count = {this.state.count} cycle={{"start":this.state.first_cyle,"end":this.state.last_cycle}}/>
    )


    var fb_data = (
      <FbSocial id={this.state.entity_id_list} date={this.props.date} type={'facebook'} key={"fbdata"} main={"fbdata"} count={this.state.count} cycle={{"start":this.state.first_cyle,"end":this.state.last_cycle}}/>
    )


    var tw_data = (
      <FbSocial id={this.state.entity_id_list} date={this.props.date} type={'twitter'} key={"twdata"} main={"twdata"} count={this.state.count} cycle={{"start":this.state.first_cyle,"end":this.state.last_cycle}}/>
    )


    for (let i = 0; i < this.state.entity.length; i++)
    {
      if(this.state.entity[i].text.includes(this.state.search_term) && !this.state.entity_id_list.includes(this.state.entity[i].value))
      {
        temp.push(
          <Table.Row>
            <Table.Cell>
              <Header as='h4' image>
                <Image src={"https://s3-us-west-2.amazonaws.com/interface-images/"+this.state.entity[i].value+".jpg"} rounded size='mini'  />
              </Header>
            </Table.Cell>
            <Table.Cell>
            <Header.Content >
                {this.state.entity[i].text}
            </Header.Content>
            </Table.Cell>
            <Table.Cell>
            <a style={{'cursor': 'pointer'}}> <Icon name='plus' size='large' onClick={this.changeEntity.bind(this,this.state.entity[i].value,this.state.entity[i].text)} /></a>
            </Table.Cell>
          </Table.Row>
        )
      }
    }


    var head_list = []
    var rank_temp = []
    var total_score = []
    var news_rank = []
    var news_score = []
    var viral_rank = []
    var viral_score = []
    var digi_rank = []
    var digi_score = []
    var fb_rank = []
    var tw_rank = []
    var fb_score = []
    var tw_score = []

    // var volume = []
    // var digi_volume = []
    // var viral_volume = []

    // var pub_volume = []
    // var pub_news = []
    // var pub_digi = []
    // var fb_sect = []
    // var tw_sect = []
    // var fb_post = []

    // var tw_likes = []
    // var tw_shares = []
    // var tw_post_list = []

    // var fb_likes = []
    // var fb_shares = []
    // var fb_post_list = []
    // var news_source = []
    // var digi_source = []
    // var viral_source = []

    // var score_temp= []

    for (let i = 0; i < this.state.entity_list.length; i++)
    {

      head_list.push(
        <Grid.Column className={'text-center'}>
          <div className={'text-center'} >
          <Image src={'https://s3-us-west-2.amazonaws.com/interface-images/'+this.state.entity_list[i].id+'.jpg'} size='tiny' circular  className={'text-center'}/>
          <div className="content">{this.state.entity_list[i].name} &nbsp; {i!==0?<Icon name='remove'   style={{'color':'red'}} onClick={this.removeEntity.bind(this,this.state.entity_list[i].id)}/>:""}</div>
          </div>
        </Grid.Column>
      )

      rank_temp.push(
        <TinyGraphRank id={this.state.entity_list[i].id} date={this.props.date} type={"rank"} key={"rank"+this.state.entity_list[i].id} show={this.state.show_graph}/>
      )

      total_score.push(
        <TinyGraphRank id={this.state.entity_list[i].id} date={this.props.date} type={"total"} key={"totalscore"+this.state.entity_list[i].id} show={this.state.show_graph}/>
      )

      news_rank.push(
        <TinyGraphRank id={this.state.entity_list[i].id} date={this.props.date} type={'newspaperrank'} key={"newspaperrank"+this.state.entity_list[i].id} show={this.state.show_graph}/>
      )
      news_score.push(
        <TinyGraphRank id={this.state.entity_list[i].id} date={this.props.date} type={'newspaperscore'} key={"newspaperscore"+this.state.entity_list[i].id} show={this.state.show_graph}/>
      )

      viral_rank.push(
        <TinyGraphRank id={this.state.entity_list[i].id} date={this.props.date} type={'viralrank'} key={"viralrank"+this.state.entity_list[i].id} show={this.state.show_graph}/>
      )

      viral_score.push(
        <TinyGraphRank id={this.state.entity_list[i].id} date={this.props.date} type={'viralscore'} key={"viral_score"+this.state.entity_list[i].id} show={this.state.show_graph}/>
      )

      digi_rank.push(
        <TinyGraphRank id={this.state.entity_list[i].id} date={this.props.date} type={'digitalrank'} key={"digitalrank"+this.state.entity_list[i].id} show={this.state.show_graph}/>
      )

      digi_score.push(
        <TinyGraphRank id={this.state.entity_list[i].id} date={this.props.date} type={'digitalscore'} key={"digitalscore"+this.state.entity_list[i].id} show={this.state.show_graph}/>
      )

      fb_rank.push(
        <TinyGraphRank id={this.state.entity_list[i].id} date={this.props.date} type={'facebookrank'} key={"facebookrank"+this.state.entity_list[i].id} show={this.state.show_graph} />
      )

      fb_score.push(
        <TinyGraphRank id={this.state.entity_list[i].id} date={this.props.date} type={'facebookscore'} key={"fb_score"+this.state.entity_list[i].id} show={this.state.show_graph} />
      )

      tw_rank.push(
        <TinyGraphRank id={this.state.entity_list[i].id} date={this.props.date} type={'twitterrank'} key={"twitterrank"+this.state.entity_list[i].id} show={this.state.show_graph}/>
      )

      tw_score.push(
        <TinyGraphRank id={this.state.entity_list[i].id} date={this.props.date} type={'twitterscore'} key={"twitterscore"+this.state.entity_list[i].id} show={this.state.show_graph}/>
      )



      // fb_sect.push(
      //   <FbSocial id={this.state.entity_list[i].id} date={this.props.date} type={'facebook'} key={"fb"+this.state.entity_list[i].id} main={"facebook_followers"} count={2}/>
      // )
      // tw_sect.push(
      //   <FbSocial id={this.state.entity_list[i].id} date={this.props.date} type={'twitter'} key={"tw"+this.state.entity_list[i].id} main={"twitter_followers"} count={3}/>
      // )
      //
      // fb_post.push(
      //   <PostSocial id={this.state.entity_list[i].id} date={this.props.date} type={"facebook"} key={"facebook"+this.state.entity_list[i].id}/>
      // )

      // fb_likes.push(
      //   <FbSocial id={this.state.entity_list[i].id} date={this.props.date} type={'facebook'} key={"fblikes"+this.state.entity_list[i].id} main={"facebook_post_likes"} count={0}/>
      // )
      //
      // fb_shares.push(
      //   <FbSocial id={this.state.entity_list[i].id} date={this.props.date} type={'facebook'} key={"fbshares"+this.state.entity_list[i].id} main={"facebook_shares"} count={1}/>
      // )
      //
      // fb_post_list.push(
      //   <FbSocial id={this.state.entity_list[i].id} date={this.props.date} type={'facebook'} key={"fbpost"+this.state.entity_list[i].id} main={"facebook_post_number"} count={3}/>
      // )
      //
      //
      //
      // tw_likes.push(
      //   <FbSocial id={this.state.entity_list[i].id} date={this.props.date} type={'twitter'} key={"twlikes"+this.state.entity_list[i].id} main={"twitter_favorites"} count={0}/>
      // )
      //
      // tw_shares.push(
      //   <FbSocial id={this.state.entity_list[i].id} date={this.props.date} type={'twitter'} key={"twshares"+this.state.entity_list[i].id} main={"twitter_retweets"} count={2}/>
      // )
      //
      // tw_post_list.push(
      //   <FbSocial id={this.state.entity_list[i].id} date={this.props.date} type={'twitter'} key={"twpost"+this.state.entity_list[i].id} main={"twitter_tweets"} count={1}/>
      // )

    }






    var article_list = []
    var duplicate = []
    this.state.track.map((data,index) => {
                          if(! duplicate.includes(data.id)){
                            article_list.push(<Articles data={data}/>)
                            return true;
                          }
                          return true;
                      });
    // var story2 = {"title":"Number of publications that talk about","data":this.state.source_volume.list,'count':this.state.source_volume.count,'data2':this.state.source_volume2.list,'count2':this.state.source_volume2.count}
    // var story3 = {"title":"Number of publications that talk about","data":this.state.source_volume3.list,'count':this.state.source_volume3.count,'data2':this.state.source_volume4.list,'count2':this.state.source_volume4.count}
    // var story4 = {"title":"Number of publications that talk about","data":this.state.source_volume6.list,'count':this.state.source_volume6.count,'data2':this.state.source_volume7.list,'count2':this.state.source_volume7.count}


    return (
      <div ref={this.handleContextRef}>
      <Grid columns={2}  stackable>
           <Grid.Column floated='centered' width={3} className={"filter-box"}>
           <Sticky context={contextRef} pushing  className={'white-fix'}  onStick={this.stickyEdge.bind(this,true)} onUnstick={this.stickyEdge.bind(this,false)}>
           <br/>
          <Form>
             <Form.Group widths='equal'>
               <Form.Input value={this.state.search_term} onChange={this.handleChangeDrop.bind(this)} icon={{ name: 'search', link: true }}
    placeholder='Search...'/>
             </Form.Group>
             <div className={'entity-over'}>
             <Table basic='very'  collapsing style={{ "width": "100%" }}>
               <Table.Body>
               {temp}
               </Table.Body>
             </Table>
             </div>
           </Form>
           </Sticky>
          </Grid.Column>

          {/*{this.state.current_id &&  <Grid.Column floated='centered' width={13}>
            <Grid.Row>
            <div className={"pad-eq"}>
            <Segment attached className={"segmant-hg"}>
            <MainRank  date={this.props.date} type={this.state.b_type}  id={this.state.current_id} current_id={this.state.current_id} current_name={this.state.current_name}/>
            </Segment>
            <Menu attached='bottom' fluid widths={6}>
              <Menu.Item
                name='section1'
                onClick={this.changetype.bind(this,"total")}
                active={this.state.b_type === "total" }
              >
                 Total
              </Menu.Item>
              <Menu.Item
                name='section2'
                onClick={this.changetype.bind(this,"newspaper")}
                active={this.state.b_type === "newspaper" }
              >
                Newspaper
              </Menu.Item>
              <Menu.Item
                name='section2'
                onClick={this.changetype.bind(this,"digitalnews")}
                active={this.state.b_type === "digitalnews" }
              >
                  Digital News
              </Menu.Item>
              <Menu.Item
                name='section2'
                onClick={this.changetype.bind(this,"viralnews")}
                active={this.state.b_type === "viralnews" }
              >
                Viral News
              </Menu.Item>
              <Menu.Item
                name='section2'
                onClick={this.changetype.bind(this,"facebook")}
                active={this.state.b_type === "facebook" }
              >
                Facebook
              </Menu.Item>
              <Menu.Item onClick={this.changetype.bind(this,"twitter")} active={this.state.b_type === "twitter" } >
              Twitter
              </Menu.Item>
            </Menu>
            </div>
            </Grid.Row>
            <Grid.Row>
            <div className={"pad-eq"}>
              {this.state.b_type === "newspaper" && <Segment attached className={"segmant-hg"}>
                <Grid columns={2}>
                  <AreaCard format={this.state.volume } format2={this.state.volume2 } date={this.props.date} type={this.state.type}  id={this.state.current_id} history={this.props.history} current_name={this.state.current_name} main={"newspaper"}/>
                  <BarCard format={story2} current_name={this.state.current_name}/>
                </Grid>
              </Segment>}
              {this.state.b_type === "digitalnews" &&  <Segment attached className={"segmant-hg"}>
                <Grid columns={2}>
                  <AreaCardDigital format={this.state.volume3 } format2={this.state.volume4 } date={this.props.date} type={this.state.type}  id={this.state.current_id} history={this.props.history} current_name={this.state.current_name} main={"digitalnews"}/>
                  <BarCard format={story3} current_name={this.state.current_name}/>
                </Grid>
              </Segment>}
              {this.state.b_type === "viralnews" &&  <Segment attached className={"segmant-hg"}>
                <Grid columns={2}>
                  <AreaViral format={this.state.volume6 } format2={this.state.volume7 } date={this.props.date} type={this.state.type}  id={this.state.current_id} history={this.props.history} current_name={this.state.current_name} main={"viralnews"}/>
                  <BarCard format={story4} current_name={this.state.current_name}/>
                </Grid>
              </Segment>}
            </div>
            {this.state.b_type === "newspaper" &&  <div className={"pad-eq"}>
                <Segment attached className={"segmant-hg"}>
                  <Paper history={this.props.history} date={this.props.date} type={"newspaper"} id={this.state.current_id} current_name={this.state.current_name} />
                </Segment>
              </div>}
            {this.state.b_type === "digitalnews" &&  <div className={"pad-eq"}>
                <Segment attached className={"segmant-hg"}>
                  <Paper history={this.props.history} date={this.props.date} type={"digital_news"} id={this.state.current_id} current_name={this.state.current_name} />
                </Segment>
              </div>}
            {this.state.b_type === "viralnews" &&  <div className={"pad-eq"}>
                <Segment attached className={"segmant-hg"}>
                  <Paper history={this.props.history} date={this.props.date} type={"viralnews"} id={this.state.current_id} current_name={this.state.current_name} />
                </Segment>
              </div>}
            </Grid.Row>
            <Grid.Row>
            <div className={"pad-eq"}>
             {this.state.b_type === "newspaper" && <Segment attached className={"segmant-hg"}>
              <People date={this.props.date} type={this.state.type}  id={this.state.current_id} history={this.props.history} current_name={this.state.current_name}/>
            </Segment>}
            </div>
            </Grid.Row>
            <Grid.Row>
            <div className={"pad-eq"}>
            {this.state.b_type === "newspaper" && <Segment attached className={"segmant-hg"}>
              <LineCard date={this.props.date} type={this.state.type}  id={this.state.current_id} history={this.props.history} current_name={this.state.current_name}/>
            </Segment>}
            </div>
            </Grid.Row>
          </Grid.Column>}*/}



          <Grid.Column floated='centered' width={13}>

          <div className={"beta date-text"}>Showing results from<span> {moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")} to {moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY")}</span> <Radio slider className="beta" label='Show Graph' defaultChecked={this.state.show_graph} onChange={this.graphView.bind(this)}/></div>








          <Sticky context={contextRef} pushing  className={'white-fix'}  onStick={this.stickyEdge.bind(this,true)} onUnstick={this.stickyEdge.bind(this,false)}>


          <Menu attached='bottom' fluid widths={6} >
            <Menu.Item
              name='section1'
              onClick={this.changetype.bind(this,"total")}
              active={this.state.b_type === "total" }
            >
               Total
            </Menu.Item>
            <Menu.Item
              name='section2'
              onClick={this.changetype.bind(this,"newspaper")}
              active={this.state.b_type === "newspaper" }
            >
              Newspaper
            </Menu.Item>
            <Menu.Item
              name='section2'
              onClick={this.changetype.bind(this,"digitalnews")}
              active={this.state.b_type === "digitalnews" }
            >
                Digital News
            </Menu.Item>
            <Menu.Item
              name='section2'
              onClick={this.changetype.bind(this,"viralnews")}
              active={this.state.b_type === "viralnews" }
            >
              Viral News
            </Menu.Item>
            <Menu.Item
              name='section2'
              onClick={this.changetype.bind(this,"facebook")}
              active={this.state.b_type === "facebook" }
            >
              Facebook
            </Menu.Item>
            <Menu.Item onClick={this.changetype.bind(this,"twitter")} active={this.state.b_type === "twitter" } >
            Twitter
            </Menu.Item>
          </Menu>



            <Grid columns={2+this.state.count}  stackable   celled>
              <Grid.Row id="headRow">
                <Grid.Column>
                </Grid.Column>
                  {head_list}
              </Grid.Row>
              </Grid>
            </Sticky>



            <Grid columns={2+this.state.count} celled className={this.state.class_name} id="stickyDiv">



            <Grid.Row>
              <Grid.Column  className={'text-center-pad mid'}>
              ST Rank &nbsp;&nbsp;<HelpText data={'Showing ST Rank of celebrities on ' +this.state.last_cycle+'. Change is shown in comparison with ST rank of '+this.state.first_cyle}/>
              </Grid.Column>
              {rank_temp}
            </Grid.Row>

            <Grid.Row>
              <Grid.Column className={'text-center-pad mid'}>
              ST Score &nbsp;&nbsp;<HelpText data={ 'Showing ST Score of celebrities on  ' +this.state.last_cycle+'. Change is shown in comparison with  ST score of '+this.state.first_cyle}/>
              </Grid.Column>
              {total_score}
            </Grid.Row>

            { (this.state.b_type === "newspaper" || this.state.b_type === "total") &&<Grid.Row>
              <Grid.Column  className={'text-center-pad mid'}>
              Newspaper <br/> <br/>ST Rank &nbsp;&nbsp;<HelpText data={'Showing Newspaper ST Rank of celebrities on  ' +this.state.last_cycle+'. Change is shown in comparison with Newspaper ST rank of '+this.state.first_cyle}/>
              </Grid.Column>
              {news_rank}
            </Grid.Row>}

            {(this.state.b_type === "newspaper" || this.state.b_type === "total") && <Grid.Row>
              <Grid.Column  className={'text-center-pad mid'}>
              Newspaper <br/> <br/>ST Score &nbsp;&nbsp;<HelpText data={'Showing Newspaper ST Score of celebrities on  ' +this.state.last_cycle+'. Change is shown in comparison with Newspaper ST score of '+this.state.first_cyle}/>
              </Grid.Column>
              {news_score}
            </Grid.Row> }


            {(this.state.b_type === "viralnews" || this.state.b_type === "total")  &&  <Grid.Row>
              <Grid.Column  className={'text-center-pad mid'}>
              Viral News <br/> <br/>ST Rank  &nbsp;&nbsp;<HelpText data={'Showing Viral News ST Rank of celebrities on  ' +this.state.last_cycle+'. Change is shown in comparison with Viral News ST rank of '+this.state.first_cyle}/>
              </Grid.Column>
              {viral_rank}
            </Grid.Row>}


            {(this.state.b_type === "viralnews" || this.state.b_type === "total") &&   <Grid.Row>
              <Grid.Column  className={'text-center-pad mid'}>
              Viral News <br/> <br/>ST Score  &nbsp;&nbsp;<HelpText data={'Showing Viral News ST Score of celebrities on  ' +this.state.last_cycle+'. Change is shown in comparison with viral news ST score of '+this.state.first_cyle}/>
              </Grid.Column>
              {viral_score}
            </Grid.Row>}

            {(this.state.b_type === "digitalnews" || this.state.b_type === "total") && <Grid.Row>
              <Grid.Column  className={'text-center-pad mid'}>
              Digital News <br/> <br/>ST Rank &nbsp;&nbsp;<HelpText data={'Showing Digital News ST Rank of celebrities on  ' +this.state.last_cycle+'. Change is shown in comparison with Digital news ST rank of '+this.state.first_cyle}/>
              </Grid.Column>
              {digi_rank}
            </Grid.Row>}

            { (this.state.b_type === "digitalnews" || this.state.b_type === "total") && <Grid.Row>
              <Grid.Column  className={'text-center-pad mid'}>
              Digital News <br/> <br/>ST Score &nbsp;&nbsp;<HelpText data={'Showing Digital News ST Score of celebrities on  ' +this.state.last_cycle+'. Change is shown in comparison with Digital News ST Score  of '+this.state.first_cyle}/>
              </Grid.Column>
              {digi_score}
            </Grid.Row>}

            {(this.state.b_type === "facebook" || this.state.b_type === "total") && <Grid.Row>
              <Grid.Column  className={'text-center-pad mid'}>
              Facebook<br/> <br/>ST Rank &nbsp;&nbsp;<HelpText data={'Showing Facebook ST Rank of celebrities on  ' +this.state.last_cycle+'. Change is shown in comparison with Facebook ST rank of '+this.state.first_cyle}/>
              </Grid.Column>
              {fb_rank}
            </Grid.Row>}

            {(this.state.b_type === "facebook" || this.state.b_type === "total") &&<Grid.Row>
              <Grid.Column  className={'text-center-pad mid'}>
              Facebook<br/> <br/>ST Score &nbsp;&nbsp;<HelpText data={'Showing Facebook ST Score of celebrities on  ' +this.state.last_cycle+'. Change is shown in comparison with facebook ST score of '+this.state.first_cyle}/>
              </Grid.Column>
              {fb_score}
            </Grid.Row>}

            {(this.state.b_type === "twitter" || this.state.b_type === "total") && <Grid.Row>
              <Grid.Column  className={'text-center-pad mid'}>
              Twitter <br/> <br/>ST Rank &nbsp;&nbsp;<HelpText data={'Showing Twitter ST Rank of celebrities on  ' +this.state.last_cycle+'. Change is shown in comparison with twitter ST rank  of '+this.state.first_cyle}/>
              </Grid.Column>
              {tw_rank}
            </Grid.Row>}

            {(this.state.b_type === "twitter" || this.state.b_type === "total") && <Grid.Row>
              <Grid.Column  className={'text-center-pad mid'}>
              Twitter <br/> <br/>ST Score &nbsp;&nbsp;<HelpText data={'Showing Twitter ST Score of celebrities on  ' +this.state.last_cycle+'. Change is shown in comparison with twitter ST score of '+this.state.first_cyle}/>
              </Grid.Column>
              {tw_score}
            </Grid.Row>}







          </Grid>


          {this.state.b_type === "newspaper" &&
            [volume]
          }

          {this.state.b_type === "digitalnews" &&
            [digi_volume]
          }

          {this.state.b_type === "viralnews" &&
            [viral_volume]
          }



          {this.state.b_type === "viralnews" &&
              [pub_volume]
           }

            {this.state.b_type === "newspaper" &&
               [pub_news]
             }

            {this.state.b_type === "digitalnews" &&
              [pub_digi]
            }



            {this.state.b_type === "newspaper" &&
               [news_source]
            }


             {this.state.b_type === "viralnews" &&
                 [viral_source]
              }


              {this.state.b_type === "digitalnews" &&
                [digi_source]
              }


              {this.state.b_type === "newspaper" &&
                 [news_people]
               }


               {this.state.b_type === "viralnews" &&
                   [viral_people]
                }

                {this.state.b_type === "digitalnews" &&
                  [digi_people]
                }

                {this.state.b_type === "facebook" &&
                  [fb_data]
                }


                {this.state.b_type === "twitter" &&
                  [tw_data]
                }

          </Grid.Column>
        </Grid>


      </div>
    )
  }
}

export default HeadToHead;
