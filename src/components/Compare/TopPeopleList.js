import React, { Component } from 'react'
import { Grid,Loader,Icon,Image } from 'semantic-ui-react'

import { newspaperStore, getPeople } from '../../store/newspaperStore'
import HelpText from "../HelpText"
import moment from 'moment'








class People extends Component {


  constructor(props) {
    super(props)
    this.store = newspaperStore()
    this.state = {
      entity_source:[],
      current_main:localStorage.id,
      load:true
    }
  }


  componentWillMount() {
    this.store.subscribe(this.onNewsProcess.bind(this))
    this.store.dispatch(getPeople({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":this.props.type,"id":this.props.id,"multiple":true,"current_id":this.state.current_main}))
  }

  componentWillReceiveProps(nextProps)
  {
      this.setState( {
        entity_source:[],
        current_main:localStorage.id
      })
      this.store.dispatch(getPeople({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
      "type":this.props.type,"id":nextProps.id,"multiple":true,"current_id":this.state.current_main}))
  }

  onNewsProcess(response){
    const state = this.store.getState()

    this.setState({
      load:true
    })

    if(state.success)
    {
      this.setState({
        entity_source:state.data,
        load:false
      })
    }
  }


  peopleClick=(id)=>
  {
    this.props.history.push("/read/"+this.props.id+"/"+id+"/1/")
  }

  changeCurrent=(id)=>{

    this.setState({
      entity_source:[],
      current_main:localStorage.id,
      load:true
    })



    this.setState({current_main:id})
    this.store.dispatch(getPeople({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":this.props.type,"id":this.props.id,"multiple":true,"current_id":id}))
  }





  render() {
    var total_list = []
    var head_list = []

    for(let k in this.props.id)
    {
      head_list.push(
        <Grid.Column className={'text-center-pad mid font'} key={k}>
         <Icon name='sort' size='large' onClick={this.changeCurrent.bind(this,this.props.id[k])}  style={{"color":this.state.current_main===this.props.id[k]?"green":""}}/>
      </Grid.Column>)
    }


    var head = (<Grid.Row style={{"background": "#91badd2b"}}>
      <Grid.Column className={'mid text-center-pad'}>

      <b> Associated Celebrities </b> &nbsp;&nbsp;<HelpText data={'Shows Most Associated celebrities from ' +moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")+' to '+ moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY")}/>
      </Grid.Column>
      {head_list}
      </Grid.Row>)


    if(this.state.entity_source !== [])
    {

      for (var i in this.state.entity_source.list){
        var entity_sect = []
        let temp  = this.state.entity_source.list[i]

        if(this.state.entity_source.main[temp]!==undefined){

          for(let k in this.props.id)
          {
              entity_sect.push(
                <Grid.Column className={'text-center-pad mid font'} key={k}>
                 {this.state.entity_source.main[temp][this.props.id[k]]?this.state.entity_source.main[temp][this.props.id[k]]:0}
              </Grid.Column>)
          }

          total_list.push(<Grid.Row>
                 <Grid.Column className={'text-center-pad mid font'}>

                 <div style={{"width":"10%"}}>

                 <Image src={'https://s3-us-west-2.amazonaws.com/interface-images/'+temp+'.jpg'} avatar />
                 </div>
                 <div style={{"width":"50%"}}>
                 {this.state.entity_source.main[temp]['name']}
                 </div>
                 </Grid.Column>
                 {entity_sect}
          </Grid.Row>)



        }


      }
    }

    const LoaderExampleInlineCentered = () => (
    <Loader active inline='centered' />
    )


    if(this.state.entity_source !==[])
    {
      return (
        <Grid columns={2+this.props.count}  stackable celled>
        {head}
        {total_list.length ===0?<LoaderExampleInlineCentered/>:total_list}
        </Grid>
      )
    }
    else {
      return (
        <Grid.Column className={'text-center-pad'}>
         <LoaderExampleInlineCentered/>
        </Grid.Column>
      )
    }
  }
}


export default People;
