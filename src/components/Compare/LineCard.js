import React, { Component } from 'react'
import { Grid,Table,Icon } from 'semantic-ui-react'
import { newspaperStore,getPlace } from '../../store/newspaperStore'
import { newspaperStore as newspaperStore2 ,getPlace as getPlace2 } from '../../store/newspaperStore'








class LineCard extends Component {



  constructor(props) {
    super(props)
    this.store = newspaperStore()
    this.store2 = newspaperStore2()
    this.state = {
      edition:[],
      edition2:[],
    }
  }


  componentWillMount() {
    this.store.subscribe(this.onNewsProcess.bind(this))
    this.store2.subscribe(this.onNewsProcess2.bind(this))
    this.store.dispatch(getPlace({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":"newspaper"}))
    this.store2.dispatch(getPlace2({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "id":this.props.id}))
  }


  componentWillReceiveProps(nextProps)
  {
      this.setState({
      date:nextProps.date,
      })
      this.store.dispatch(getPlace({ "start_date":nextProps.date.start,"end_date":nextProps.date.end}))
      this.store2.dispatch(getPlace2({ "start_date":this.props.date.start,"end_date":this.props.date.end,
      "id":nextProps.id}))
  }






  onNewsProcess(response){
          const state = this.store.getState()
          switch(state.type) {
              case "place":
                if(state.success)
                {
                  this.setState({
                    edition:state.data.edition
                  })
                }
              break;
          default:
              ;
      }
  }


  onNewsProcess2(response){
          const state2 = this.store2.getState()
          switch(state2.type) {
              case "place":
                if(state2.success)
                {
                  this.setState({
                    edition2:state2.data.edition
                  })
                }
              break;
          default:
              ;
      }
  }

  render() {
  var table =[]
      if(this.state.edition){
        this.state.edition.map((data,index) => {
                              if(data.lat){
                                table.push(<Table.Row>
                                  <Table.Cell><Icon  name='dot circle outline' color="red" />{data.Place} </Table.Cell>
                                  <Table.Cell>{data.Count} Articles</Table.Cell>
                                </Table.Row>)
                              }
                              return true;
                          });
      }


      var table2 =[]
          if(this.state.edition2){
            this.state.edition2.map((data,index) => {
                                  if(data.lat){
                                    table2.push(<Table.Row>
                                      <Table.Cell><Icon  name='dot circle outline' color="red" />{data.Place} </Table.Cell>
                                      <Table.Cell>{data.Count} Articles</Table.Cell>
                                    </Table.Row>)
                                  }
                                  return true;
                              });
          }



    return (
      <div>


      <Grid columns={2}>

            <Grid.Column>
            <div className="og-div"><span className="card-span-small">Regions that talk about you</span></div>
              <Grid.Row>
              </Grid.Row>
              <Grid.Row className="og-card map">
              <Table basic='very'>
                <Table.Body>
                {table}
                </Table.Body>
              </Table>
              </Grid.Row>
            </Grid.Column>

            <Grid.Column>
            <div className="og-div"><span className="card-span-small">Regions that talk about <span  style={{'textTransform':'capitalize'}}>{this.props.current_name}</span> </span></div>
              <Grid.Row>
              </Grid.Row>
              <Grid.Row className="og-card map">
              <Table basic='very'>
                <Table.Body>
                {table2}
                </Table.Body>
              </Table>
              </Grid.Row>
            </Grid.Column>
        </Grid>

        </div>
    )
  }
}

export default LineCard;
