import React, { Component } from 'react'
import { Header,Table,Image,Grid } from 'semantic-ui-react'
import avatar from '../../public/img/sample.png'
import { newspaperStore,getNewsSource } from '../../store/newspaperStore'

import { newspaperStore as newspaperStore2 ,getNewsSource  as getNewsSource2} from '../../store/newspaperStore'




class Paper extends Component {


  constructor(props) {
    super(props)
    this.store = newspaperStore()
    this.store2 = newspaperStore2()
    this.state = {
      newspaper_source:[],
      newspaper_source2:[],
    }
  }


  componentWillMount() {
    this.store.subscribe(this.onNewsProcess.bind(this))
    this.store2.subscribe(this.onNewsProcess2.bind(this))
    this.store.dispatch(getNewsSource({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":this.props.type}))
    this.store2.dispatch(getNewsSource2({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":this.props.type,"id":this.props.id}))
  }

  componentWillReceiveProps(nextProps)
  {
      this.store.dispatch(getNewsSource({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
      "type":this.props.type}))
      this.store2.dispatch(getNewsSource2({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
      "type":this.props.type,"id":this.props.id}))
  }




  onNewsProcess(response)
  {
          const state = this.store.getState()
          switch(state.type) {
            case "newspaper":
                  if(state.success)
                  {
                    this.setState({
                      newspaper_source:state.data.source
                    })
                  }
                break;
          default:
              ;
      }
  }

  onNewsProcess2(response)
  {
          const state2 = this.store2.getState()
          switch(state2.type) {
            case "newspaper":
                  if(state2.success)
                  {
                    this.setState({
                      newspaper_source2:state2.data.source
                    })
                  }
                break;
          default:
              ;
      }
  }



  peopleClick=(id)=>
  {
    this.props.history.push("/read/"+localStorage.saveData+"/"+id+"/1/")
  }

  render() {

    var source_list = []
    if(this.state.newspaper_source)
    {
      this.state.newspaper_source.map((data,index) => {
                            source_list.push(
                              <Table.Row  key={index} >
                                <Table.Cell>
                                <Image  src={data.name !=="Not Available"?'https://s3.ap-south-1.amazonaws.com/newskeeper-sources/'+data.source_id+'.jpg':avatar} rounded size='mini' />
                                </Table.Cell>
                                <Table.Cell className={"border-none"}>
                                  <Header as='h3' image>
                                    <Header.Content style={{'textTransform':'capitalize','fontSize':'1.3em'}}>
                                        {data.name}
                                    </Header.Content>
                                  </Header>
                                </Table.Cell>
                                <Table.Cell style={{'textTransform':'capitalize','fontSize':'1em'}} onClick={this.peopleClick.bind(this,data.source_id)}>
                                    {data.count} Articles
                                </Table.Cell>
                              </Table.Row>
                            )
                            return true
            });
    }


    var source_list2 = []
    if(this.state.newspaper_source2)
    {
      this.state.newspaper_source2.map((data,index) => {
                            source_list2.push(
                              <Table.Row  key={index} >
                                <Table.Cell>
                                <Image  src={data.name !=="Not Available"?'https://s3.ap-south-1.amazonaws.com/newskeeper-sources/'+data.source_id+'.jpg':avatar} rounded size='mini' />
                                </Table.Cell>
                                <Table.Cell className={"border-none"}>
                                  <Header as='h3' image>
                                    <Header.Content style={{'textTransform':'capitalize','fontSize':'1.3em'}}>
                                        {data.name}
                                    </Header.Content>
                                  </Header>
                                </Table.Cell>
                                <Table.Cell style={{'textTransform':'capitalize','fontSize':'1em'}} onClick={this.peopleClick.bind(this,data.source_id)}>
                                    {data.count} Articles
                                </Table.Cell>
                              </Table.Row>
                            )
                            return true
            });
    }

    return(
      <Grid columns={2} className={"pad-eq"}>
        <Grid.Column>
        <div className="og-div"><span className="card-span-small">Publications that talk about you</span></div>
        <Table basic='very' celled collapsing style={{'width':'80%'}}>
        <Table.Body>
          {source_list}
        </Table.Body>
        </Table>
        </Grid.Column>
        <Grid.Column>
        <div className="og-div"><span className="card-span-small">Publications that talk about <span  style={{'textTransform':'capitalize'}}>{this.props.current_name}</span></span></div>
        <Table basic='very' celled collapsing style={{'width':'80%'}}>
        <Table.Body>
          {source_list2}
        </Table.Body>
        </Table>
        </Grid.Column>
      </Grid>
    )

  }
}


export default Paper;
