import React, { Component } from 'react'
import { ResponsiveContainer, LineChart, Line, XAxis, YAxis,
  Tooltip } from 'recharts';
  import { Grid,Image,Icon} from 'semantic-ui-react'

import { totalStore,totalFetch } from '../../store/totalStore'
import { totalStore as totalStore2,totalFetch as totalFetch2 } from '../../store/totalStore'
import moment from 'moment'


class MainRank extends Component {



  constructor(props) {
    super(props)
    this.store = totalStore()
    this.store2 = totalStore2()


    this.state = {
      loading: false,
      total:[],
      rank:[],
      rank2:[],
      total2:[],
      news_score:[],
      news_rank:[],
      news_score2:[],
      news_rank2:[],
      digital_rank:[],
      digital_score:[],
      digital_rank2:[],
      digital_score2:[],
      viral_rank:[],
      viral_score:[],
      viral_rank2:[],
      viral_score2:[],
      facebook_rank:[],
      facebook_score:[],
      twitter_rank:[],
      twitter_score:[],
      facebook_rank2:[],
      facebook_score2:[],
      twitter_rank2:[],
      twitter_score2:[]

    }
  }



  componentDidMount() {
    this.store.subscribe(this.onProcess.bind(this))
    this.store2.subscribe(this.onProcess2.bind(this))
    this.store.dispatch(totalFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end}))
    this.store2.dispatch(totalFetch2({ "start_date":this.props.date.start,"end_date":this.props.date.end,"id":this.props.current_id}))
  }


  componentWillReceiveProps(nextProps)
  {
      this.setState({
      date:nextProps.date,
      })
      this.store.dispatch(totalFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end}))
      this.store2.dispatch(totalFetch2({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"id":nextProps.id}))
  }



    onList=(data,data2,name)=>
    {

      var list = []
      for (var key in data)
      {
          list.push({"Date":moment(key, "YYYY-MM-DD").format("MMM D, YYYY"), [name]:data[key],[name+"2"]:data2[key],"Count":data[key]+data2[key]})
      }
      return list;
    }



  onProcess=()=>{

    const state = this.store.getState()

    if(state.success === true && state.data.rank !== undefined)
    {
        this.setState({
          rank:state.data.rank.ranks,
          rank_change:state.data.rank.change,
          score_change:state.data.total.change,
          total:state.data.total.scores,
          news_rank:state.data.newspaper.ranks,
          news_score:state.data.newspaper.scores,
          digital_rank:state.data.digitalnews.ranks,
          digital_score:state.data.digitalnews.scores,
          viral_rank:state.data.viralnews.ranks,
          viral_score:state.data.viralnews.scores,
          facebook_rank:state.data.facebook.scores,
          facebook_score:state.data.facebook.ranks,
          twitter_rank:state.data.twitter.ranks,
          twitter_score:state.data.twitter.scores
        })

    }

  }

  onProcess2=()=>{

    const state2 = this.store2.getState()
    if(state2.success === true && state2.data.rank !== undefined)
    {
        this.setState({
          rank2:state2.data.rank.ranks,
          total2:state2.data.total.scores,
          rank_change2:state2.data.rank.change,
          score_change2:state2.data.total.change,
          news_rank2:state2.data.newspaper.ranks,
          news_score2:state2.data.newspaper.scores,
          digital_rank2:state2.data.digitalnews.ranks,
          digital_score2:state2.data.digitalnews.scores,
          viral_rank2:state2.data.viralnews.ranks,
          viral_score2:state2.data.viralnews.scores,
          facebook_rank2:state2.data.facebook.scores,
          facebook_score2:state2.data.facebook.ranks,
          twitter_rank2:state2.data.twitter.ranks,
          twitter_score2:state2.data.twitter.scores
        })

    }

  }


  state = {
    activeIndex: 0,
    animation: false,
  };

  render() {

    if(this.state.total !== undefined && this.state.total !== [] )
    {
            var le = Object.keys(this.state.total).length;

            var rank_avg = 0
            var score_avg = 0

            for (let key in this.state.total)
            {
                  var score = this.state.total[key]
                  var rank  = this.state.rank[key]

                  rank_avg = rank_avg+rank
                  score_avg = score_avg+score
            }
            rank_avg = Math.abs(Math.round(rank_avg/le  * 1) / 1)
            score_avg = Math.abs(Math.round(score_avg/le  * 100) / 100)
    }

    if(this.state.total2 !== undefined && this.state.total2 !== [] )
    {
            var le2 = Object.keys(this.state.total2).length;
            var rank_avg2 = 0
            var score_avg2 = 0
            for (let key in this.state.total2)
            {
                  var score2 = this.state.total2[key]
                  var rank2  = this.state.rank2[key]
                  rank_avg2 = rank_avg2+rank2
                  score_avg2 = score_avg2+score2
            }
            rank_avg2 =   Math.abs(Math.round(rank_avg2/le2  * 1) / 1)
            score_avg2 =  Math.abs(Math.round(score_avg2/le2  * 100) / 100)

    }


    return (
          <div style={{'padding-bottom':'3em'}}>
          <Grid.Row>
            <Grid columns={3}  stackable>
            <Grid.Column className={'text-center'}>
            <Grid.Row>
            <Image src={'https://s3-us-west-2.amazonaws.com/interface-images/'+localStorage.id+'.jpg'} size='tiny' circular  className={'text-center'}  style={{    "border": "3px solid #4285f4"}}/>
            You
            </Grid.Row>
            <Grid.Row className={"font-ad"}>
            ST Rank <span>{rank} </span><span style= {{'color':this.state.rank_change < 0?'red':"green",'display':this.state.rank_change ===0?'none':"","font-size":"1em"}}><Icon name={this.state.rank_change < 0?'caret down':"caret up"}/> { Math.abs(Math.round(this.state.rank_change * 100) / 100) }</span> ({rank_avg}avg)  <br/> <br/>
            ST Score <span>{score} </span><span style= {{'color':this.state.score_change < 0?'red':"green",'display':this.state.score_change ===0?'none':"","font-size":"1em"}}><Icon name={this.state.score_change < 0?'caret down':"caret up"}/> { Math.abs(Math.round(this.state.score_change * 100) / 100) }</span> ({score_avg}avg)
            </Grid.Row>
            </Grid.Column>
            <Grid.Column className={'text-center'}>
            <b>Vs</b>
            </Grid.Column>
            <Grid.Column className={'text-center'}>
            <Grid.Row>
            <Image src={'https://s3-us-west-2.amazonaws.com/interface-images/'+this.props.current_id+'.jpg'} size='tiny' circular  className={'text-center'}  style={{    "border": "3px solid #ac57bd"}}/>
            <div style={{'text-transform':"capitalize"}}>{this.props.current_name}</div>
            </Grid.Row>
            <Grid.Row className={"font-ad"}>
            ST Rank <span>{rank2} </span><span style= {{'color':this.state.rank_change2 < 0?'red':"green",'display':this.state.rank_change2 ===0?'none':"","font-size":"1em"}}><Icon name={this.state.rank_change2 < 0?'caret down':"caret up"}/> { Math.abs(Math.round(this.state.rank_change2 * 100) / 100) }</span> ({rank_avg2}avg)   <br/> <br/>
            ST Score <span>{score2} </span><span style= {{'color':this.state.score_change2 < 0?'red':"green",'display':this.state.score_change2 ===0?'none':"","font-size":"1em"}}><Icon name={this.state.score_change2 < 0?'caret down':"caret up"}/> { Math.abs(Math.round(this.state.score_change2 * 100) / 100) }  </span> ({score_avg2}avg)
            </Grid.Row>
            </Grid.Column>
            </Grid>
          </Grid.Row>


          {this.props.type === "total" && <div><div className="og-div">ST Rank</div>
            <ResponsiveContainer width='100%' aspect={4.0/.5} >
            <LineChart
                  data={this.onList(this.state.rank,this.state.rank2,"Rank")}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                  <XAxis dataKey='Date'  hide={true}/>
                  <YAxis dataKey="Count" orientation='right'  hide={true} reversed={true}/>
                  <Tooltip cursor={false} />
                  <Line dataKey='Rank' stroke='#4285f4' strokeWidth={2} dot={true} name={"You"}/>
                  <Line dataKey='Rank2' stroke='#ac57bd' strokeWidth={2} dot={true} name={this.props.current_name}/>
            </LineChart>
            </ResponsiveContainer>
            <div className="og-div">ST Score</div>
            <ResponsiveContainer width='100%' aspect={4.0/.5}>
            <LineChart
                  data={this.onList(this.state.total,this.state.total2,"Score")}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                  <XAxis dataKey='Date'/>
                  <YAxis dataKey="Count" orientation='right'  hide={true} />
                  <Tooltip cursor={false} />
                  <Line dataKey='Score' stroke='#4285f4' strokeWidth={2} dot={true} name={"You"} type='monotone'/>
                  <Line dataKey='Score2' stroke='#ac57bd' strokeWidth={2} dot={true} name={this.props.current_name} type='monotone'/>
            </LineChart>
            </ResponsiveContainer></div>}




            {this.props.type === "newspaper" &&  <div><div className="og-div">ST Rank</div>
            <ResponsiveContainer width='100%' aspect={4.0/.5}>
            <LineChart
                  data={this.onList(this.state.news_rank,this.state.news_rank2,"Rank")}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                  <XAxis dataKey='Date'  hide={true}/>
                  <YAxis dataKey="Count" orientation='right'  hide={true} reversed={true}/>
                  <Tooltip cursor={false} />
                  <Line dataKey='Rank' stroke='#4285f4' strokeWidth={2} dot={true} name={"You"}/>
                  <Line dataKey='Rank2' stroke='#ac57bd' strokeWidth={2} dot={true} name={this.props.current_name}/>
            </LineChart>
            </ResponsiveContainer>



            <div className="og-div">ST Score</div>
            <ResponsiveContainer width='100%' aspect={4.0/.5}>
            <LineChart
                  data={this.onList(this.state.news_score,this.state.news_score2,"Score")}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                  <XAxis dataKey='Date'/>
                  <YAxis dataKey="Count" orientation='right'  hide={true} />
                  <Tooltip cursor={false} />
                  <Line dataKey='Score' stroke='#4285f4' strokeWidth={2} dot={true} name={"You"} type='monotone'/>
                  <Line dataKey='Score2' stroke='#ac57bd' strokeWidth={2} dot={true} name={this.props.current_name} type='monotone' />
            </LineChart>
            </ResponsiveContainer></div>}



            {this.props.type === "digitalnews" &&  <div><div className="og-div">ST Rank</div>
            <ResponsiveContainer width='100%' aspect={4.0/.5}>
            <LineChart
                  data={this.onList(this.state.digital_rank,this.state.digital_rank2,"Rank")}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                  <XAxis dataKey='Date'  hide={true}/>
                  <YAxis dataKey="Count" orientation='right'  hide={true} reversed={true}/>
                  <Tooltip cursor={false} />
                  <Line dataKey='Rank' stroke='#4285f4' strokeWidth={2} dot={true} name={"You"} />
                  <Line dataKey='Rank2' stroke='#ac57bd' strokeWidth={2} dot={true} name={this.props.current_name} />
            </LineChart>
            </ResponsiveContainer>



            <div className="og-div">ST Score</div>
            <ResponsiveContainer width='100%' aspect={4.0/.5}>
            <LineChart
                  data={this.onList(this.state.digital_score,this.state.digital_score2,"Score")}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                  <XAxis dataKey='Date'/>
                  <YAxis dataKey="Count" orientation='right'  hide={true} />
                  <Tooltip cursor={false} />
                  <Line dataKey='Score' stroke='#4285f4' strokeWidth={2} dot={true} name={"You"} type='monotone' />
                  <Line dataKey='Score2' stroke='#ac57bd' strokeWidth={2} dot={true} name={this.props.current_name} type='monotone'/>
            </LineChart>
            </ResponsiveContainer></div>}



            {this.props.type === "viralnews" &&  <div><div className="og-div">ST Rank</div>
            <ResponsiveContainer width='100%' aspect={4.0/.5}>
            <LineChart
                  data={this.onList(this.state.viral_rank,this.state.viral_rank2,"Rank")}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                  <XAxis dataKey='Date'  hide={true}/>
                  <YAxis dataKey="Count" orientation='right'  hide={true} reversed={true}/>
                  <Tooltip cursor={false} />
                  <Line dataKey='Rank' stroke='#4285f4' strokeWidth={2} dot={true} name={"You"} />
                  <Line dataKey='Rank2' stroke='#ac57bd' strokeWidth={2} dot={true} name={this.props.current_name} />
            </LineChart>
            </ResponsiveContainer>

            <div className="og-div">ST Score</div>
            <ResponsiveContainer width='100%' aspect={4.0/.5}>
            <LineChart
                  data={this.onList(this.state.viral_score,this.state.viral_score2,"Score")}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                  <XAxis dataKey='Date'/>
                  <YAxis dataKey="Count" orientation='right'  hide={true} />
                  <Tooltip cursor={false} />
                  <Line dataKey='Score' stroke='#4285f4' strokeWidth={2} dot={true} name={"You"} type='monotone'/>
                  <Line dataKey='Score2' stroke='#ac57bd' strokeWidth={2} dot={true} name={this.props.current_name} type='monotone' />
            </LineChart>
            </ResponsiveContainer> </div>}



            {this.props.type === "facebook" &&  <div> <div className="og-div">ST Rank</div>
            <ResponsiveContainer width='100%' aspect={4.0/.5}>
            <LineChart
                  data={this.onList(this.state.facebook_rank,this.state.facebook_rank2,"Rank")}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                  <XAxis dataKey='Date'  hide={true}/>
                  <YAxis dataKey="Count" orientation='right'  hide={true} reversed={true} />
                  <Tooltip cursor={false} />
                  <Line dataKey='Rank' stroke='#4285f4' strokeWidth={2} dot={true} name={"You"}/>
                  <Line dataKey='Rank2' stroke='#ac57bd' strokeWidth={2} dot={true} name={this.props.current_name}/>
            </LineChart>
            </ResponsiveContainer>

            <div className="og-div">ST Score</div>
            <ResponsiveContainer width='100%' aspect={4.0/.5}>
            <LineChart
                  data={this.onList(this.state.facebook_score,this.state.facebook_score2,"Score")}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                  <XAxis dataKey='Date'/>
                  <YAxis dataKey="Count" orientation='right'  hide={true} />
                  <Tooltip cursor={false} />
                  <Line dataKey='Score' stroke='#4285f4' strokeWidth={2} dot={true} name={"You"} type='monotone'/>
                  <Line dataKey='Score2' stroke='#ac57bd' strokeWidth={2} dot={true} name={this.props.current_name} type='monotone'/>
            </LineChart>
            </ResponsiveContainer> </div>}



          {this.props.type === "twitter" &&  <div>   <div className="og-div">ST Rank</div>
            <ResponsiveContainer width='100%' aspect={4.0/.5}>
            <LineChart
                  data={this.onList(this.state.twitter_rank,this.state.twitter_rank2,"Rank")}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                  <XAxis dataKey='Date'  hide={true}/>
                  <YAxis dataKey="Count" orientation='right'  hide={true} reversed={true}/>
                  <Tooltip cursor={false} />
                  <Line dataKey='Rank' stroke='#4285f4' strokeWidth={2} dot={true} name={"You"}/>
                  <Line dataKey='Rank2' stroke='#ac57bd' strokeWidth={2} dot={true} name={this.props.current_name} />
            </LineChart>
            </ResponsiveContainer>

            <div className="og-div">ST Score</div>
            <ResponsiveContainer width='100%' aspect={4.0/.5}>
            <LineChart
                  data={this.onList(this.state.twitter_score,this.state.twitter_score2,"Score")}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                  <XAxis dataKey='Date'/>
                  <YAxis dataKey="Count" orientation='right'  hide={true} />
                  <Tooltip cursor={false} />
                  <Line dataKey='Score' stroke='#4285f4' strokeWidth={2} dot={true} name={"You"} type='monotone'/>
                  <Line dataKey='Score2' stroke='#ac57bd' strokeWidth={2} dot={true} name={this.props.current_name} type='monotone'/>
            </LineChart>
            </ResponsiveContainer> </div>}





          </div>

    )
  }
}

export default MainRank;
