import React, { Component } from 'react'
import { Grid,Icon} from 'semantic-ui-react'
import {  BarChart, Bar, XAxis, YAxis, Tooltip, ResponsiveContainer} from 'recharts';
import HelpText from "../HelpText"




class BarCard extends Component {

  constructor(props) {
    super(props)
    this.state = {
      main:false,
      help_text:"Change View"
    }
  }
  changeGraph=()=>{
    this.setState({
      main:!this.state.main,
    })
  }


  render() {




    var list = []
    var gold = 0
    var platinum = 0
    var silver = 0
    var bronze = 0
    var total = 0
    var second_total = 0
    var last_total = 0
    var gold2 = 0
    var platinum2 = 0
    var silver2 = 0
    var bronze2 = 0
    var total2 = 0
    var second_total2 = 0
    var last_total2 = 0


    for (let key in this.props.format ){
      var keys = Object.keys(this.props.format);
      var last = keys[keys.length-1];
      let data = this.props.format[key]
      var temp_data = {}
      gold = gold+data.digitalnews_storyvolume_gold
      platinum = platinum+data.digitalnews_storyvolume_platinum
      bronze = bronze+data.digitalnews_storyvolume_bronze
      silver = silver+data.digitalnews_storyvolume_silver
      temp_data['Total'] = data.digitalnews_storyvolume_gold+data.digitalnews_storyvolume_platinum+data.digitalnews_storyvolume_silver+data.digitalnews_storyvolume_bronze
      total = total+temp_data['Total']
      list.push(temp_data)
      if(parseInt(key,10) === parseInt(last-1,10)){
        second_total = temp_data['Total']
      }
      else if (parseInt(key,10) === parseInt(last,10)) {
        last_total = temp_data['Total']
      }
      else {
        ;
      }
    }


    var change =   Math.round(((last_total-second_total)/second_total)*100 * 100) / 100
    for (let key in this.props.format2)
    {
      var keys2 = Object.keys(this.props.format2);
      var last2 = keys2[keys2.length-1];
      let data = this.props.format2[key]
      let temp_data2 = {}
      gold2 = gold2+data.digitalnews_storyvolume_gold
      platinum2 = platinum2+data.digitalnews_storyvolume_platinum
      bronze2 = bronze2+data.digitalnews_storyvolume_bronze
      silver2 = silver2+data.digitalnews_storyvolume_silver
      temp_data2['Total'] = data.digitalnews_storyvolume_gold+data.digitalnews_storyvolume_platinum+data.digitalnews_storyvolume_silver+data.digitalnews_storyvolume_bronze
      total2 = total2+temp_data2['Total']
      list.push(temp_data2)
      if(parseInt(key,10) === parseInt(last2-1,10)){
        second_total2 = temp_data2['Total']
      }
      else if (parseInt(key,10) === parseInt(last2,10)) {
        last_total2 = temp_data2['Total']
      }
      else {
        ;
      }
    }

    var change2 =   Math.round(((last_total2-second_total2)/second_total2)*100 * 100) / 100

    var listing = [{"Type":"Platinum",'Your Count':platinum,"Count2":platinum2,"Max":platinum+platinum2},
                  {"Type":"Gold",'Your Count':gold,"Count2":gold2,"Max":gold+gold2},
                  {"Type":"Silver",'Your Count':silver,"Count2":silver2,"Max":silver+silver2},
                  {"Type":"Bronze",'Your Count':bronze,"Count2":bronze2,"Max":bronze+bronze2}]
    return (
            <Grid.Column>

              <Grid.Row>
              <div className="og-div">
              <span className="card-span-small" >Number of Stories <HelpText data='Shows the number of stories  over the selected period of time'/></span></div>
              </Grid.Row>


              <Grid.Row>
              <Grid columns={2}>
              <Grid.Column>
                <div className="og-div hike"><span className="digit-span-card">{total}</span> <span style={{'color':change < 0?'red':"green",'display':change === 0?'none':""}}><Icon name={change < 0?'caret down':"caret up"}/>{change}%</span></div>
              </Grid.Column>
              <Grid.Column>
                <div className="og-div hike"><span className="digit-span-card">{total2}</span> <span style={{'color':change2 < 0?'red':"green",'display':change2 === 0?'none':""}}><Icon name={change2 < 0?'caret down':"caret up"}/>{change2}%</span></div>
              </Grid.Column>
              </Grid>
              </Grid.Row>

              <Grid.Row>
              <ResponsiveContainer width='100%' aspect={4.0/1}>

                  <BarChart data={listing}
                          namemargin={{ top:0, right: 0, left: 0, bottom:0 }}
                        >
                      <XAxis dataKey="Type" hide={true}/>
                      <YAxis dataKey="Max"  hide={true} />
                      <Tooltip  cursor={false} />
                      <Bar
                        type="monotone"
                        dataKey='Your Count'
                        stroke="blue"
                        strokeWidth="0"
                        fillOpacity=".5"
                        fill="#4285f4"
                        name={"You"}
                      />
                      <Bar
                        type="monotone"
                        dataKey="Count2"
                        stroke="blue"
                        strokeWidth="0"
                        fillOpacity=".5"
                        fill="#ac57bd"
                        name={this.props.current_name}

                      />
                    </BarChart>

              </ResponsiveContainer>
              </Grid.Row>

            </Grid.Column>
    )
  }
}

export default BarCard;
