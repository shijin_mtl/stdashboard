import React, { Component } from 'react'
import { Grid,Icon,Loader} from 'semantic-ui-react'
import { totalStore,fbFetch } from '../../store/totalStore'
import config from '../../utils/config'
import HelpText from "../HelpText"
import moment from 'moment'




class FbSocial extends Component {
  constructor(props) {
    super(props)
    this.storeTotal = totalStore()
    this.state = {
      data:false,
      error:false

    }
  }


  componentWillMount() {
    this.storeTotal.subscribe(this.onTotalProcess.bind(this))
    this.storeTotal.dispatch(fbFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,'id':this.props.id,"type":this.props.type,"multi":true}))

  }


  componentWillReceiveProps(nextProps)
  {
    this.storeTotal.dispatch(fbFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,'id':nextProps.id,"type":nextProps.type,"multi":true}))
  }


  onTotalProcess=()=>{
    const state = this.storeTotal.getState()
    this.setState({
      error:false
    })
    if(state.success){
        this.setState({
            data:state.data,
            error:false
        })
      }
      // if(state.loading){
      //   this.setState({
      //     error:!state.loading
      //   })
      // }
    }

  render() {

    var follower_list = []
    var like_list = []
    var share_list = []
    var post_list = []


    var photo_like = []
    var video_like = []
    var link_like = []
    var status_like =[]


    var photo_share = []
    var video_share = []
    var link_share = []
    var status_share =[]


    var photo_post= []
    var video_post = []
    var link_post = []
    var status_post =[]


    var load_list = []




    for (var i in this.props.id)
    {


      var  LoaderExampleInlineCentered = () => (
        <Grid.Column className={'text-center-pad mid font'} style={{'color':'red'}}>
        { this.state.error && "error" }
        { !this.state.error && <Loader active inline='centered' size='mini' style={{    "margin-top": "1%"}} /> }
      </Grid.Column>
      )

      load_list.push(<LoaderExampleInlineCentered/>)

      if(this.state.data && this.state.data[this.props.id[i]]!==undefined && !this.state.error)
      {
        photo_like.push(<Grid.Column className={'text-center-pad mid font'}>
           {config.numRound(this.state.data[this.props.id[i]]['photo']['likes'])}
        </Grid.Column>)
        video_like.push(<Grid.Column className={'text-center-pad mid font'}>
           {config.numRound(this.state.data[this.props.id[i]]['video']['likes'])}
        </Grid.Column>)
        link_like.push(<Grid.Column className={'text-center-pad mid font'}>
           {config.numRound(this.state.data[this.props.id[i]]['link']['likes'])}
        </Grid.Column>)
        status_like.push(<Grid.Column className={'text-center-pad mid font'}>
           {config.numRound(this.state.data[this.props.id[i]]['status']['likes'])}
        </Grid.Column>)
        photo_share.push(<Grid.Column className={'text-center-pad mid font'}>
           {config.numRound(this.state.data[this.props.id[i]]['photo']['shares'])}
        </Grid.Column>)
        video_share.push(<Grid.Column className={'text-center-pad mid font'}>
           {config.numRound(this.state.data[this.props.id[i]]['video']['shares'])}
        </Grid.Column>)
        link_share.push(<Grid.Column className={'text-center-pad mid font'}>
           {config.numRound(this.state.data[this.props.id[i]]['link']['shares'])}
        </Grid.Column>)
        status_share.push(<Grid.Column className={'text-center-pad mid font'}>
           {config.numRound(this.state.data[this.props.id[i]]['status']['shares'])}
        </Grid.Column>)
        photo_post.push(<Grid.Column className={'text-center-pad mid font'}>
           {config.numRound(this.state.data[this.props.id[i]]['photo']['total'])}
        </Grid.Column>)
        video_post.push(<Grid.Column className={'text-center-pad mid font'}>
           {config.numRound(this.state.data[this.props.id[i]]['video']['total'])}
        </Grid.Column>)
        link_post.push(<Grid.Column className={'text-center-pad mid font'}>
           {config.numRound(this.state.data[this.props.id[i]]['link']['total'])}
        </Grid.Column>)
        status_post.push(<Grid.Column className={'text-center-pad mid font'}>
           {config.numRound(this.state.data[this.props.id[i]]['status']['total'])}
        </Grid.Column>)
        follower_list.push(<Grid.Column className={'text-center-pad'}>
           {config.numRound(this.state.data[this.props.id[i]]['meta']['followers'])}<span style={{'color':this.state.data[this.props.id[i]]['meta']['follower_change'] < 0?'red':"green",'display':this.state.data[this.props.id[i]]['meta']['follower_change'] === 0?'none':""}}>  <Icon name={this.state.data[this.props.id[i]]['meta']['follower_change'] < 0?'caret down':"caret up"}/>&nbsp;
           {config.numRound(this.state.data[this.props.id[i]]['meta']['follower_change'])} {!this.props.type.includes('rank')?"":""}</span>
        </Grid.Column>)
        like_list.push(<Grid.Column className={'text-center-pad'}>
           {config.numRound(this.state.data[this.props.id[i]]['meta']['likes'])}<span style={{'color':this.state.data[this.props.id[i]]['meta']['like_change'] < 0?'red':"green",'display':this.state.data[this.props.id[i]]['meta']['like_change'] === 0?'none':""}}>  <Icon name={this.state.data[this.props.id[i]]['meta']['like_change'] < 0?'caret down':"caret up"}/>&nbsp;
           {config.numRound(this.state.data[this.props.id[i]]['meta']['like_change'])} {!this.props.type.includes('rank')?"":""}</span>
        </Grid.Column>)
        share_list.push(<Grid.Column className={'text-center-pad'}>
           {config.numRound(this.state.data[this.props.id[i]]['meta']['shares'])}<span style={{'color':this.state.data[this.props.id[i]]['meta']['share_change'] < 0?'red':"green",'display':this.state.data[this.props.id[i]]['meta']['share_change'] === 0?'none':""}}>  <Icon name={this.state.data[this.props.id[i]]['meta']['share_change'] < 0?'caret down':"caret up"}/>&nbsp;
           {config.numRound(this.state.data[this.props.id[i]]['meta']['share_change'])} {!this.props.type.includes('rank')?"":""}</span>
        </Grid.Column>)
        post_list.push(<Grid.Column className={'text-center-pad'}>
           {config.numRound(this.state.data[this.props.id[i]]['meta']['total'])}<span style={{'color':this.state.data[this.props.id[i]]['meta']['post_change'] < 0?'red':"green",'display':this.state.data[this.props.id[i]]['meta']['post_change'] === 0?'none':""}}>  <Icon name={this.state.data[this.props.id[i]]['meta']['post_change'] < 0?'caret down':"caret up"}/>&nbsp;
           {config.numRound(this.state.data[this.props.id[i]]['meta']['post_change'])} {!this.props.type.includes('rank')?"":""}</span>
        </Grid.Column>)
      }
    }

    var diff = (moment(this.props.date.end, "YYYY-MM-DD")).diff(moment(this.props.date.start, "YYYY-MM-DD"), 'days')
    var diff_date = moment(this.props.date.start, "YYYY-MM-DD").subtract(diff, "days").format("MMM D, YYYY")
    if(this.props.type === 'facebook'){
      return (
        <Grid columns={2+this.props.count}  stackable celled>
        <Grid.Row>
               <Grid.Column className={'text-center-pad mid'}>
               Followers &nbsp; <HelpText data={'Shows Total Followers count on  ' +this.props.cycle.end +" Change is shown in comparison with count of "+this.props.cycle.start}/>
               </Grid.Column>
               {follower_list.length === 0?load_list:follower_list}
        </Grid.Row>
        <Grid.Row style={{"background": "#91badd2b"}}>
               <Grid.Column className={'text-center-pad mid '}>
               Posts &nbsp; <HelpText data={' Showing total posts from ' +moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")+' to '+ moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY")+" in comparison with posts from "+diff_date+" to "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")}/>
               </Grid.Column>
               {post_list.length === 0?load_list:post_list}
        </Grid.Row>
        <Grid.Row>
               <Grid.Column className={'text-center-pad mid font '}>
               {this.props.type === "twitter"?"Tweets":"Photos"}
               </Grid.Column>
               {photo_post.length === 0?load_list:photo_post}
        </Grid.Row>
        <Grid.Row >
               <Grid.Column className={'text-center-pad mid font '}>
               Videos
               </Grid.Column>
               {video_post.length === 0?load_list:video_post}
        </Grid.Row>
        <Grid.Row >
               <Grid.Column className={'text-center-pad mid font '}>
               Links
               </Grid.Column>
               {link_post.length === 0?load_list:link_post}
        </Grid.Row>
        <Grid.Row >
               <Grid.Column className={'text-center-pad mid font '}>
               Status
               </Grid.Column>
               {status_post.length === 0?load_list:status_post}
        </Grid.Row>
        <Grid.Row style={{"background": "#91badd2b"}}>
               <Grid.Column className={'text-center-pad mid'}>
               Likes &nbsp; <HelpText data={' Showing total likes from ' +moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")+' to '+ moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY")+" in comparison with posts from "+diff_date+" to "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")}/>
               </Grid.Column>
               {like_list.length === 0?load_list:like_list}
        </Grid.Row>

        <Grid.Row >
               <Grid.Column className={'text-center-pad mid font '}>
               Photos
               </Grid.Column>
               {photo_like.length === 0?load_list:photo_like}
        </Grid.Row>
        <Grid.Row >
               <Grid.Column className={'text-center-pad mid font '}>
               Videos
               </Grid.Column>
               {video_like.length === 0?load_list:video_like}
        </Grid.Row>
        <Grid.Row >
               <Grid.Column className={'text-center-pad mid font '}>
               Links
               </Grid.Column>
               {link_like.length === 0?load_list:link_like}
        </Grid.Row>
        <Grid.Row >
               <Grid.Column className={'text-center-pad mid font '}>
               Status
               </Grid.Column>
               {status_like.length === 0?load_list:status_like}
        </Grid.Row>



        <Grid.Row style={{"background": "#91badd2b"}}>
               <Grid.Column className={'text-center-pad mid'}>
               Shares &nbsp;<HelpText data={' Showing total shares from ' +moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")+' to '+ moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY")+" in comparison with shares from "+diff_date+" to "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")}/>
               </Grid.Column>
               {share_list.length === 0?load_list:share_list}
        </Grid.Row>


        <Grid.Row >
               <Grid.Column className={'text-center-pad mid font '}>
               Photos
               </Grid.Column>
               {photo_share.length === 0?load_list:photo_share}
        </Grid.Row>
        <Grid.Row >
               <Grid.Column className={'text-center-pad mid font '}>
               Videos
               </Grid.Column>
               {video_share.length === 0?load_list:video_share}
        </Grid.Row>
        <Grid.Row >
               <Grid.Column className={'text-center-pad mid font '}>
               Links
               </Grid.Column>
               {link_share.length === 0?load_list:link_share}
        </Grid.Row>
        <Grid.Row >
               <Grid.Column className={'text-center-pad mid font '}>
               Status
               </Grid.Column>
               {status_share.length === 0?load_list:status_share}
        </Grid.Row>






        </Grid>
      )}
      else {

        return (
          <Grid columns={2+this.props.count}  stackable celled>


          <Grid.Row>
                 <Grid.Column className={'text-center-pad mid'}>
                 Followers &nbsp; <HelpText data={'Shows Total Followers count on  ' +this.props.cycle.end+" Change is shown in comparison with count of "+this.props.cycle.start}/>
                 </Grid.Column>
                 {follower_list.length === 0?load_list:follower_list}
          </Grid.Row>

          <Grid.Row style={{"background": "#91badd2b"}}>
                 <Grid.Column className={'text-center-pad mid '}>
                 Tweets &nbsp; <HelpText data={' Showing total tweets count from ' +moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")+' to '+ moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY")+" in comparison with tweets from "+diff_date+" to "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")}/>
                 </Grid.Column>
                 {post_list.length === 0?load_list:post_list}
          </Grid.Row>

          <Grid.Row >
                 <Grid.Column className={'text-center-pad mid font '}>
                 {this.props.type === "twitter"?"Tweets":"Photos"}
                 </Grid.Column>
                 {photo_post.length === 0?load_list:photo_post}
          </Grid.Row>
          <Grid.Row >
                 <Grid.Column className={'text-center-pad mid font '}>
                 Videos
                 </Grid.Column>
                 {video_post.length === 0?load_list:video_post}
          </Grid.Row>
          <Grid.Row >
                 <Grid.Column className={'text-center-pad mid font '}>
                 Links
                 </Grid.Column>
                 {link_post.length === 0?load_list:link_post}
          </Grid.Row>
          <Grid.Row >
                 <Grid.Column className={'text-center-pad mid font '}>
                 Status
                 </Grid.Column>
                 {status_post.length === 0?load_list:status_post}
          </Grid.Row>

          <Grid.Row style={{"background": "#91badd2b"}}>
                 <Grid.Column className={'text-center-pad mid '}>
                 Favorites &nbsp; <HelpText data={' Showing total Favorites from ' +moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")+' to '+ moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY")+" in comparison with Favorites from "+diff_date+" to "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")}/>
                 </Grid.Column>
                 {like_list.length === 0?load_list:like_list}
          </Grid.Row>

          <Grid.Row >
                 <Grid.Column className={'text-center-pad mid font '}>
                 Photos
                 </Grid.Column>
                 {photo_like.length === 0?load_list:photo_like}
          </Grid.Row>
          <Grid.Row >
                 <Grid.Column className={'text-center-pad mid font '}>
                 Videos
                 </Grid.Column>
                 {video_like.length === 0?load_list:video_like}
          </Grid.Row>
          <Grid.Row >
                 <Grid.Column className={'text-center-pad mid font '}>
                 Links
                 </Grid.Column>
                 {link_like.length === 0?load_list:link_like}
          </Grid.Row>
          <Grid.Row >
                 <Grid.Column className={'text-center-pad mid font '}>
                 Status
                 </Grid.Column>
                 {status_like.length === 0?load_list:status_like}
          </Grid.Row>



          <Grid.Row style={{"background": "#91badd2b"}}>
                 <Grid.Column className={'text-center-pad mid'}>
                 Retweets  &nbsp;<HelpText data={' Showing total Retweets from ' +moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")+' to '+ moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY")+" in comparison with Retweets from "+diff_date+" to "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")}/>
                 </Grid.Column>
                 {share_list.length === 0?load_list:share_list}
          </Grid.Row>


          <Grid.Row >
                 <Grid.Column className={'text-center-pad mid font '}>
                 Photos
                 </Grid.Column>
                 {photo_share.length === 0?load_list:photo_share}
          </Grid.Row>
          <Grid.Row >
                 <Grid.Column className={'text-center-pad mid font '}>
                 Videos
                 </Grid.Column>
                 {video_share.length === 0?load_list:video_share}
          </Grid.Row>
          <Grid.Row >
                 <Grid.Column className={'text-center-pad mid font '}>
                 Links
                 </Grid.Column>
                 {link_share.length === 0?load_list:link_share}
          </Grid.Row>
          <Grid.Row >
                 <Grid.Column className={'text-center-pad mid font '}>
                 Status
                 </Grid.Column>
                 {status_share.length === 0?load_list:status_share}
          </Grid.Row>






          </Grid>
        )



      }
  }
}


export default FbSocial;
