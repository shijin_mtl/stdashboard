import React, { Component } from 'react'
import { Grid,Icon } from 'semantic-ui-react'
import {AreaChart,Area,ResponsiveContainer } from 'recharts';

class TinyGraph extends Component {

  render() {
    var temp_list = []
    var last = 0

    var second_key
    var second_last = 0

    for(let i = 0;i<this.props.data.data.length;i++){
      temp_list.push({"Score":this.props.data.data[i]})
      last = this.props.data.data[i]

      second_last = this.props.data.data[second_key]
      second_key = i

    }

    var change = 0
    if(second_last !==0){
      change =   Math.round(((last-second_last)/second_last)*100 * 100) / 100
    }


    // var  LoaderExampleInlineCentered = () => (
    //   <Grid.Column className={'text-center-pad mid font'} style={{'color':'red'}}>
    //     <Loader active inline='centered' size='mini' style={{    "margin-top": "1%"}} />
    //   </Grid.Column>
    // )



    return (
      <Grid.Column className={'text-center-pad'}>
       {this.props.show && <ResponsiveContainer width='100%' aspect={4.0/0.8} >
        <AreaChart data={temp_list}
              margin={{top: 0, right: 0, left: 0, bottom: 0}}>
          <Area type='monotone' dataKey='Score' stroke='#8884d8' fill='#8884d8' />
        </AreaChart>
      </ResponsiveContainer>}
      {last} <span style={{'color':change < 0?'red':"green",'display':change === 0?'none':""}}>  <Icon name={change < 0?'caret down':"caret up"}/>&nbsp; {Math.abs(change)}%</span>
      </Grid.Column>
    )
  }
}

export default TinyGraph;
