import React, { Component } from 'react'
import {Grid,Table,Header,Image } from 'semantic-ui-react'
import avatar from '../../public/img/sample.png'
import { newspaperStore, getPeople } from '../../store/newspaperStore'
import { newspaperStore as newspaperStore2, getPeople as getPeople2 } from '../../store/newspaperStore'





class People extends Component {


  constructor(props) {
    super(props)
    this.store = newspaperStore()
    this.store2 = newspaperStore2()
    this.state = {
      people_source:[],
      people_source2:[],
    }
  }


  componentWillMount() {
    this.store.subscribe(this.onNewsProcess.bind(this))
    this.store2.subscribe(this.onNewsProcess2.bind(this))
    this.store.dispatch(getPeople({ "start_date":this.props.date.start,"end_date":this.props.date.end}))
    this.store2.dispatch(getPeople2({ "start_date":this.props.date.start,"end_date":this.props.date.end,"id":this.props.current_id}))
  }


  componentWillReceiveProps(nextProps)
  {
      this.setState({
      date:nextProps.date,
      })
      this.store.dispatch(getPeople({ "start_date":nextProps.date.start,"end_date":nextProps.date.end}))
      this.store2.dispatch(getPeople2({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"id":nextProps.id}))
  }




  onNewsProcess(response){
          const state = this.store.getState()
          switch(state.type) {
          case "people":
                if(state.success)
                {
                  this.setState({
                    people_source:state.data.persons
                  })
                }
              break;
          default:
              ;
      }
  }

  onNewsProcess2(response){
          const state2 = this.store2.getState()
          switch(state2.type) {
          case "people":
                if(state2.success)
                {
                  this.setState({
                    people_source2:state2.data.persons
                  })
                }
              break;
          default:
              ;
      }
  }



peopleClick=(id)=>
{
  this.props.history.push("/read/"+id)
}

  render() {
    var table = [

      <Table.Row>
        <Table.Cell style={{ 'text-align': 'left','font-weight': 'bold' }}>
          Celebrity
        </Table.Cell>
        <Table.Cell style={{ 'text-align': 'left','font-weight': 'bold' }}>
          Articles
        </Table.Cell>
      </Table.Row>

  ]
  var table2 = [

    <Table.Row>
      <Table.Cell style={{ 'text-align': 'left','font-weight': 'bold' }}>
        Celebrity
      </Table.Cell>
      <Table.Cell style={{ 'text-align': 'left','font-weight': 'bold' }}>
        Articles
      </Table.Cell>
    </Table.Row>

]




      this.state.people_source.map((data,index) => {
                            table.push(<Table.Row>
                              <Table.Cell>
                                <Header as='h3' image>
                                  <Image src={data.name !=="Not Available"?'https://s3-us-west-2.amazonaws.com/interface-images/'+data.nub_id+'.jpg':avatar} className={"img-sm"} rounded size='mini' />
                                  <Header.Content style={{'textTransform':'capitalize','fontSize':'1.3em'}}>
                                      {data.name}
                                  </Header.Content>
                                </Header>
                              </Table.Cell>
                              <Table.Cell style={{'textTransform':'capitalize','fontSize':'1em'}} onClick={this.peopleClick.bind(this,data.nub_id)}>
                                  {data.count} Articles
                              </Table.Cell>
                            </Table.Row>)
                            return true;
                        });



          this.state.people_source2.map((data,index) => {
                                table2.push(<Table.Row>
                                  <Table.Cell>
                                    <Header as='h3' image>
                                      <Image src={data.name !=="Not Available"?'https://s3-us-west-2.amazonaws.com/interface-images/'+data.nub_id+'.jpg':avatar} rounded size='mini' />
                                      <Header.Content style={{'textTransform':'capitalize','fontSize':'1.3em'}}>
                                          {data.name}
                                      </Header.Content>
                                    </Header>
                                  </Table.Cell>
                                  <Table.Cell style={{'textTransform':'capitalize','fontSize':'1em'}} onClick={this.peopleClick.bind(this,data.nub_id)}>
                                      {data.count} Articles
                                  </Table.Cell>
                                </Table.Row>)
                                return true;
                            });

    return (
      <div>

        <Grid columns={2}>
        <Grid.Column >
        <div className="og-div"><span className="card-span-small">People who are mostly associated with you</span></div>
          <Table basic='very' celled collapsing style={{'width':'100%'}}>
          <Table.Body>
            {table}
          </Table.Body>
          </Table>
        </Grid.Column>
        <Grid.Column>
        <div className="og-div"><span className="card-span-small">People who are mostly associated with <span  style={{'textTransform':'capitalize'}}>{this.props.current_name}</span></span></div>
        <Table basic='very' celled collapsing style={{'width':'100%'}}>
        <Table.Body>
        {table2}
        </Table.Body>
        </Table>
        </Grid.Column>
        </Grid>
      </div>
    )
  }
}

export default People;
