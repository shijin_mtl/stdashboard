import React, { Component } from 'react'
import { Grid,Loader,Icon } from 'semantic-ui-react'

import { newspaperStore, getNewsSource } from '../../store/newspaperStore'
import HelpText from "../HelpText"
import moment from 'moment'







class Paper extends Component {


  constructor(props) {
    super(props)
    this.store = newspaperStore()
    this.state = {
      newspaper_source:[],
      current_main:localStorage.id
    }
  }


  componentWillMount() {
    this.store.subscribe(this.onNewsProcess.bind(this))
    this.store.dispatch(getNewsSource({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":this.props.type,"id":this.props.id,"multiple":true,"current_id":this.state.current_main}))
  }

  componentWillReceiveProps(nextProps)
  {
      this.store.dispatch(getNewsSource({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
      "type":this.props.type,"id":nextProps.id,"multiple":true,"current_id":this.state.current_main}))
  }

  onNewsProcess(response){
    const state = this.store.getState()
    if(state.success)
    {
      this.setState({
        newspaper_source:state.data
      })
    }
  }


  peopleClick=(id)=>
  {
    this.props.history.push("/read/"+this.props.id+"/"+id+"/1/")
  }

  changeCurrent=(id)=>{
    this.setState({current_main:id})
    this.store.dispatch(getNewsSource({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":this.props.type,"id":this.props.id,"multiple":true,"current_id":id}))
  }





  render() {



    var total_list = []

    var head_list = []

    for(let k in this.props.id)
    {
      head_list.push(
        <Grid.Column className={'text-center-pad  font'} key={k}>
         <Icon name='sort' size='large' onClick={this.changeCurrent.bind(this,this.props.id[k])}  style={{"color":this.state.current_main===this.props.id[k]?"green":""}}/>
      </Grid.Column>)
    }


    var head = (<Grid.Row style={{"background": "#91badd2b"}}>
      <Grid.Column className={'text-center-pad mid'}>
      <b>Top Sources</b> &nbsp;&nbsp;<HelpText data={'Shows Top  Publications from ' +moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")+' to '+ moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY")}/>
      </Grid.Column>
      {head_list}
      </Grid.Row>)


    if(this.state.newspaper_source !== [])
    {


      for (var i in this.state.newspaper_source.list){
        var entity_sect = []

        let temp  = this.state.newspaper_source.list[i]


          for(let k in this.props.id)
          {

            entity_sect.push(
              <Grid.Column className={'text-center-pad mid font'} key={k}>
               {this.state.newspaper_source.main[temp][this.props.id[k]]?this.state.newspaper_source.main[temp][this.props.id[k]]:0}
            </Grid.Column>)
          }

        total_list.push(<Grid.Row>
               <Grid.Column className={'text-center-pad mid font'}>
               {this.state.newspaper_source.main[temp]['name']}
               </Grid.Column>
               {entity_sect}
        </Grid.Row>)
      }
    }

    const LoaderExampleInlineCentered = () => (
    <Loader active inline='centered' />
    )

    if(this.state.newspaper_source)
    {
      return (
        <Grid columns={2+this.props.count}  stackable celled>
        {head}
        {total_list.length ===0?<LoaderExampleInlineCentered/>:total_list}
        </Grid>
      )
    }
    else {
      return (
        <Grid.Column className={'text-center-pad'}>
         <LoaderExampleInlineCentered/>
        </Grid.Column>
      )
    }
  }
}


export default Paper;
