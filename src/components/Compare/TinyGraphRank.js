import React, { Component } from 'react'
import { Grid,Loader,Icon } from 'semantic-ui-react'
import {AreaChart,Area,ResponsiveContainer } from 'recharts';
import { totalStore,totalFetch } from '../../store/totalStore'





class TinyGraphRank extends Component {


  constructor(props) {
    super(props)
    this.store = totalStore()
    this.state = {
      data: [],
      error:false
    }
  }

  componentWillMount() {
    this.store.subscribe(this.onProcess.bind(this))
    this.store.dispatch(totalFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"id":this.props.id}))
  }

  componentWillReceiveProps(nextProps)
  {
    this.store.dispatch(totalFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"id":this.props.id}))
  }

  onProcess(response){

          const state = this.store.getState()
          if(state.success && state.data.rank)
          {

              if(this.props.type === "rank")
              {
                this.setState({
                  data:state.data.rank.ranks
                })
              }

              if(this.props.type === 'newspaperrank')
              {
                this.setState({
                  data:state.data.newspaper.ranks
                })
              }
              if(this.props.type === 'newspaperscore')
              {
                this.setState({
                  data:state.data.newspaper.scores
                })
              }

              if(this.props.type === 'viralrank')
              {
                this.setState({
                  data:state.data.viralnews.ranks
                })
              }

              if(this.props.type === 'viralscore')
              {
                this.setState({
                  data:state.data.viralnews.scores
                })
              }

              if(this.props.type === 'digitalrank')
              {
                this.setState({
                  data:state.data.digitalnews.ranks
                })
              }

              if(this.props.type === 'digitalscore')
              {
                this.setState({
                  data:state.data.digitalnews.scores
                })
              }

              if(this.props.type === 'facebookrank')
              {
                this.setState({
                  data:state.data.facebook.ranks
                })
              }

              if(this.props.type === 'facebookscore')
              {
                this.setState({
                  data:state.data.facebook.scores
                })
              }

              if(this.props.type === 'twitterrank')
              {
                this.setState({
                  data:state.data.twitter.ranks
                })
              }

              if(this.props.type === 'twitterscore')
              {
                this.setState({
                  data:state.data.twitter.scores
                })
              }

              if(this.props.type === 'total')
              {
                this.setState({
                  data:state.data.total.scores
                })
              }



          }

          if(state.error){
            this.setState({
              error:true
            })
          }


  }



  render() {


    const LoaderExampleInlineCentered = () => (
    <Loader active inline='centered' />
)

    var temp_list = []
    var last = 0
    // var second_key
    var second_last = 0

    for(let key in this.state.data){
        temp_list.push({'Date':key,"Rank":this.state.data[key]})
        last = this.state.data[key]
        if(second_last === 0)
        {
          second_last = this.state.data[key]
        }
        // second_key = key
    }

    var change = 0
    if(last-second_last !== 0 && second_last !==0){
      change =   this.props.type.includes('rank')?-(last-second_last):Math.round(((last-second_last)/second_last)*100 * 100) / 100
    }

    if(this.state.data.length === 0){
      return (
        <Grid.Column className={'text-center-pad'} style={{'color':'red','font-size': '1.3em'}}>
        { this.state.error && "error" }
        { !this.state.error && <LoaderExampleInlineCentered/> }
        </Grid.Column>
      )
    }

    else
    {
      return (
        <Grid.Column className={'text-center-pad'}>
         {this.props.show && <ResponsiveContainer width='100%' aspect={4.0/.8} >
          <AreaChart width={200} height={60} data={temp_list}
                margin={{top: 0, right: 0, left: 0, bottom: 0}}>
            <Area type= {!this.props.type.includes('rank')?'monotone':""} dataKey='Rank' stroke='#8884d8' fill='#8884d8' />
          </AreaChart>
        </ResponsiveContainer>}
        {last}<span style={{'color':change < 0?'red':"green",'display':change === 0?'none':""}}>  <Icon name={change < 0?'caret down':"caret up"}/>&nbsp; {Math.abs(change)}{!this.props.type.includes('rank')?"%":""}</span>
        </Grid.Column>
      )
    }

  }
}

export default TinyGraphRank;
