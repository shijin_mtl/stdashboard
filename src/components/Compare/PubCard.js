import React, { Component } from 'react'
import { Grid,Icon,Loader} from 'semantic-ui-react'
import { newspaperStore,getSourceVolume } from '../../store/newspaperStore'
import HelpText from "../HelpText"
import moment from 'moment'








class PubCard extends Component {
  constructor(props) {
    super(props)
    this.storeVolume = newspaperStore()
    this.state = {
      main:false,
      help_text:"Change View",
      source_list:[],
      error:false
    }
  }


  componentWillMount() {
    this.storeVolume.subscribe(this.onNewsProcess.bind(this))
    this.storeVolume.dispatch(getSourceVolume({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":this.props.type,'id':this.props.id,"multiple":true}))
  }


  componentWillReceiveProps(nextProps)
  {
    this.storeVolume.dispatch(getSourceVolume({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
    "type":nextProps.type,'id':nextProps.id,"multiple":true}))
  }



  onNewsProcess(response)
  {
          const state2 = this.storeVolume.getState()

          this.setState({
            error:false
          })

          if(state2.error){
            this.setState({
              error:true
            })
          }

          switch(state2.type) {

          case "volume":
                if(state2.success)
                {
                  this.setState({
                    volume: state2.data.data
                  })
                }
              break;
          case "source_volume":
                if(state2.success)
                {
                  this.setState({
                    source_list:state2.data,
                  })
                }
              break;
          default:
              ;
      }
  }


  render() {
      var load_list = []

    var total_list = []

    var platinum_list = []
    var gold_list = []
    var silver_list = []
    var bronze_list = []
    for (var i in this.props.id) {
      var second_total = 0
      var last_total = 0



     var  LoaderExampleInlineCentered = () => (
       <Grid.Column className={'text-center-pad mid font'} style={{'color':'red'}}>
       { this.state.error && "error" }
       { !this.state.error && <Loader active inline='centered' size='mini' style={{    "margin-top": "1%"}} /> }
     </Grid.Column>
     )


     load_list.push(<LoaderExampleInlineCentered/>)



      if(this.state.source_list[this.props.id[i]]){
        try {
          var length = this.state.source_list[this.props.id[i]]['list'].length
          second_total = this.state.source_list[this.props.id[i]]['list'][length-2].Total
          last_total = this.state.source_list[this.props.id[i]]['list'][length-1].Total
        } catch (e) {

        } finally {
        }

        var change = 0
        if(second_total !== 0){
          change =   Math.round(((last_total-second_total)/second_total)*100 * 100) / 100
        }
        var total = this.state.source_list[this.props.id[i]]['count']['platinum_list']+this.state.source_list[this.props.id[i]]['count']['gold_list']+this.state.source_list[this.props.id[i]]['count']['silver_list']+this.state.source_list[this.props.id[i]]['count']['bronze_list']


        total_list.push(<Grid.Column className={'text-center-pad'}>
           {total}<span style={{'color':change < 0?'red':"green",'display':change === 0?'none':""}}>  <Icon name={change < 0?'caret down':"caret up"}/>&nbsp; {Math.abs(change)}{!this.props.type.includes('rank')?"%":""}</span>
        </Grid.Column>)

        platinum_list.push(<Grid.Column className={'text-center-pad mid font'}>
           {this.state.source_list[this.props.id[i]]['count']['platinum_list']}
        </Grid.Column>)

        gold_list.push(<Grid.Column className={'text-center-pad mid font'}>
           {this.state.source_list[this.props.id[i]]['count']['gold_list']}
        </Grid.Column>)

        silver_list.push(<Grid.Column className={'text-center-pad mid font'}>
           {this.state.source_list[this.props.id[i]]['count']['silver_list']}
        </Grid.Column>)

        bronze_list.push(<Grid.Column className={'text-center-pad mid font'}>
           {this.state.source_list[this.props.id[i]]['count']['bronze_list']}
        </Grid.Column>)
      }





    }




    return (
      <Grid columns={2+this.props.count}  stackable celled>

      <Grid.Row style={{"background": "#91badd2b"}}>
             <Grid.Column className={'text-center-pad mid'}>
             Publications  &nbsp;&nbsp;<HelpText data={'Shows Count of  Publications from ' +moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")+' to '+ moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY")}/>
             </Grid.Column>
              {total_list.length ===0?load_list:total_list}
      </Grid.Row>

      <Grid.Row>
             <Grid.Column className={'text-center-pad mid font'}>
             Platinum
             </Grid.Column>
              {platinum_list.length ===0?load_list:platinum_list}
      </Grid.Row>

      <Grid.Row>
             <Grid.Column className={'text-center-pad mid font'}>
             Gold
             </Grid.Column>
              {gold_list.length ===0?load_list:gold_list}
      </Grid.Row>

      <Grid.Row>
             <Grid.Column className={'text-center-pad mid font'}>
             Silver
             </Grid.Column>
             {silver_list.length ===0?load_list:silver_list}
      </Grid.Row>

      <Grid.Row>
             <Grid.Column className={'text-center-pad mid font'}>
             Bronze
             </Grid.Column>
              {bronze_list.length ===0?load_list:bronze_list}
      </Grid.Row>
      </Grid>
    )
  }
}

export default PubCard;
