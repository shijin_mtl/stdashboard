import React, { Component } from 'react'
import { Grid} from 'semantic-ui-react'
import { socialStore,postFetch } from '../../store/socialStore'





class PostSocial extends Component {
  constructor(props) {
    super(props)
    this.storeTotal = socialStore()
    this.state = {
      posts:""
    }
  }


  componentWillMount() {
    this.storeTotal.subscribe(this.onTotalProcess.bind(this))
    if(this.props.type === 'facebook'){
      this.storeTotal.dispatch(postFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,'id':this.props.id}))
    }
    if(this.props.type === 'twitter'){
      this.storeTotal.dispatch(postFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,'id':this.props.id}))
    }
  }


  componentWillReceiveProps(nextProps)
  {
    if(this.props.type === 'facebook')
    {
    this.storeTotal.dispatch(postFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,'id':nextProps.id}))
    }
    if(this.props.type === 'twitter'){
      this.storeTotal.dispatch(postFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,'id':nextProps.id}))
    }
  }



  onTotalProcess=()=>{
    const state = this.storeTotal.getState()
    if(state.success){

      this.setState({
        posts:state.data.posts
      })

    }
  }


  render() {

    var post_total =0,  post_videos=0,   post_images =0, post_status = 0

    var like_total =0,  like_videos =0,  like_images =0,  like_status = 0

    var comment_total =0,  comment_videos =0,  comment_images =0,  comment_status = 0

    var share_total =0,   share_videos =0,  share_images =0,  share_status = 0


    for (var i in this.state.posts)
    {

      if(this.state.posts[i].network === this.props.type){
        post_total = post_total+1
        like_total = like_total +this.state.posts[i].likes
        comment_total = comment_total + this.state.posts[i].comments
        share_total = share_total + this.state.posts[i].shares

        if(this.state.posts[i].type === "video"){
          post_videos = post_videos +1
          like_videos = like_videos + this.state.posts[i].likes
          comment_videos = comment_videos + this.state.posts[i].comments
          share_videos = share_videos + this.state.posts[i].shares

        }

        else if (this.state.posts[i].type === "photo") {
          post_images = post_images +1
          like_images = like_images + this.state.posts[i].likes
          comment_images = comment_images + this.state.posts[i].comments
          share_images = share_images + this.state.posts[i].shares

        }

        else if (this.state.posts[i].type === "status") {
          post_status = post_status+1
          like_status = like_status + this.state.posts[i].likes
          comment_status = comment_status + this.state.posts[i].comments
          share_status = share_status + this.state.posts[i].shares

        }

        else {
          ;
        }

      }

    }

      return (
        <Grid.Column className={'text-center-pad omega'}>
        <Grid.Row>
        <Grid columns={2}>
        <Grid.Column>
          Total :
        </Grid.Column>
        <Grid.Column>
         {post_total}
        </Grid.Column>
        </Grid>
        </Grid.Row>
        <Grid.Row>
        <Grid columns={2}>
        <Grid.Column>
          Images :
        </Grid.Column>
        <Grid.Column>
         {post_images}
        </Grid.Column>
        </Grid>
        </Grid.Row>
        <Grid.Row>
        <Grid columns={2}>
        <Grid.Column>
          Videos :
        </Grid.Column>
        <Grid.Column>
         {post_videos}
        </Grid.Column>
        </Grid>
        </Grid.Row>
        <Grid.Row>
        <Grid columns={2}>
        <Grid.Column>
          Status :
        </Grid.Column>
        <Grid.Column>
         {post_status}
        </Grid.Column>
        </Grid>
        </Grid.Row>
        {/*<Grid.Row>
        {like_total} - {like_images} - {like_videos} - {like_status} <br/>
        </Grid.Row>
        <Grid.Row>
        {comment_total} - {comment_images} - {comment_videos} - {comment_status} <br/>
        </Grid.Row>
        <Grid.Row>
        {share_total} - {share_images} - {share_videos} - {share_status} <br/>
        </Grid.Row>*/}
        </Grid.Column>
      )
  }
}

export default PostSocial;
