import React, { Component } from 'react'
import { Popup } from 'semantic-ui-react'

class FormHelp extends Component {
  render() {
    return (
      <Popup
        trigger={this.props.input}
        header='Lorem ipsum'
        content='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incidid'
        on='focus'
      />
    )
  }
}

export default FormHelp;
