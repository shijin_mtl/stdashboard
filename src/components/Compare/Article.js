import React, { Component } from 'react'
import { Grid,Item } from 'semantic-ui-react'
import moment from 'moment'
import Truncate from 'react-truncate';







class Articles extends Component {
  render() {
    return (
      <Grid.Column width={15} floated='centered' style={{'padding-bottom':'3em'}}>
      <Item.Group>
            <Item>
              <Item.Image size='tiny' src={'https://s3.ap-south-1.amazonaws.com/newskeeper-sources/'+this.props.data.source+'.jpg'} />
              <Item.Content>
                <Item.Header as='a' href={this.props.data.url} target="_blank">{this.props.data.title}</Item.Header>
                <Item.Meta>{moment(this.props.data.date, "YYYY-MM-DD").format("MMM D, YYYY")}   {this.props.data.name} </Item.Meta>
                <Item.Description>

                <Truncate lines={5} ellipsis={<span>... <a href={this.props.data.url}>Read more</a></span>}>
                    {this.props.data.content}
                </Truncate>

                </Item.Description>
              </Item.Content>
            </Item>
          </Item.Group>
          </Grid.Column>
    )
  }
}

export default Articles;
