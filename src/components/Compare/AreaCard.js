import React, { Component } from 'react'
import { Grid,Icon} from 'semantic-ui-react'
import { newspaperStore,getVolume } from '../../store/newspaperStore'
import HelpText from "../HelpText"
import moment from 'moment'


class AreaCard extends Component {
  constructor(props) {
    super(props)
    this.storeVolume = newspaperStore()
    this.state = {
      main:false,
      help_text:"Change View"
    }
  }


  componentWillMount() {
    this.storeVolume.subscribe(this.onNewsProcess.bind(this))
    this.storeVolume.dispatch(getVolume({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":this.props.type,'id':this.props.id,"multiple":true}))
  }

  componentWillReceiveProps(nextProps)
  {
    this.storeVolume.dispatch(getVolume({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
    "type":nextProps.type,'id':nextProps.id,"multiple":true}))
  }

  onNewsProcess(response)
  {
          const state2 = this.storeVolume.getState()
          switch(state2.type) {
          case "volume":
                if(state2.success)
                {
                  this.setState({
                    volume: state2.data
                  })
                }
              break;
          case "source_volume":
                if(state2.success)
                {

                  this.setState({
                    source_volume:state2.data
                  })
                }
              break;
          default:
              ;
      }
  }


  render() {



    var total_list = []

    var platinum_list = []


    var gold_list = []


    var silver_list = []

    var bronze_list = []


    for (var i in this.props.id) {


      var list = []
      var gold = 0
      var platinum = 0
      var silver = 0
      var bronze = 0
      var total = 0
      var second_total = 0
      var last_total = 0




      if(this.props.type === 'newspaper' && this.state.volume)
      {
      var keys,last,temp_data
      for (let key in this.state.volume[this.props.id[i]] ){
         keys = Object.keys(this.state.volume[this.props.id[i]]);
         last = keys[keys.length-1];
        let data = this.state.volume[this.props.id[i]][key]
         temp_data = {}
        gold = gold+data.newspaper_storyvolume_gold
        platinum = platinum+data.newspaper_storyvolume_platinum
        bronze = bronze+data.newspaper_storyvolume_bronze
        silver = silver+data.newspaper_storyvolume_silver
        temp_data['Total'] = data.newspaper_storyvolume_gold+data.newspaper_storyvolume_platinum+data.newspaper_storyvolume_silver+data.newspaper_storyvolume_bronze
        total = total+temp_data['Total']
        list.push(temp_data)
        if(parseInt(key,10) === parseInt(last-1,10)){
          second_total = temp_data['Total']
        }
        else if (parseInt(key,10) === parseInt(last,10) && second_total ===0) {
          last_total = temp_data['Total']
        }
        else {
          ;
        }
      }
    }

    else if (this.props.type === 'digitalnews' && this.state.volume) {
      for (let key in this.state.volume[this.props.id[i]] ){
         keys = Object.keys(this.state.volume[this.props.id[i]]);
         last = keys[keys.length-1];
        let data = this.state.volume[this.props.id[i]][key]
        temp_data = {}
        gold = gold+data.digitalnews_storyvolume_gold
        platinum = platinum+data.digitalnews_storyvolume_platinum
        bronze = bronze+data.digitalnews_storyvolume_bronze
        silver = silver+data.digitalnews_storyvolume_silver
        temp_data['Total'] = data.digitalnews_storyvolume_gold+data.digitalnews_storyvolume_platinum+data.digitalnews_storyvolume_silver+data.digitalnews_storyvolume_bronze
        total = total+temp_data['Total']
        list.push(temp_data)
        if(parseInt(key,10) === parseInt(last-1,10) && second_total ===0){
          second_total = temp_data['Total']
        }
        else if (parseInt(key,10) === parseInt(last,10)) {
          last_total = temp_data['Total']
        }
        else {
          ;
        }
      }

    }
    else if(this.state.volume)  {
      for (let key in this.state.volume[this.props.id[i]] ){
         keys = Object.keys(this.state.volume[this.props.id[i]]);
        last = keys[keys.length-1];
        let data = this.state.volume[this.props.id[i]][key]
         temp_data = {}
        gold = gold+data.viralnews_storyvolume_gold
        platinum = platinum+data.viralnews_storyvolume_platinum
        bronze = bronze+data.viralnews_storyvolume_bronze
        silver = silver+data.viralnews_storyvolume_silver
        temp_data['Total'] = data.viralnews_storyvolume_gold+data.viralnews_storyvolume_platinum+data.viralnews_storyvolume_silver+data.viralnews_storyvolume_bronze
        total = total+temp_data['Total']
        list.push(temp_data)
        if(parseInt(key,10) === parseInt(last-1,10) && second_total ===0){
          second_total = temp_data['Total']
        }
        else if (parseInt(key,10) === parseInt(last,10)) {
          last_total = temp_data['Total']
        }
        else {
          ;
        }
      }
    }
    else {
      ;
    }

    var change  = ""
    if(last_total-second_total && second_total){
      change =   Math.round(((last_total-second_total)/second_total)*100 * 100) / 100
    }

    total_list.push(<Grid.Column className={'text-center-pad'}>
       {total}<span style={{'color':change < 0?'red':"green",'display':change === 0?'none':""}}>  <Icon name={change < 0?'caret down':"caret up"}/>&nbsp; {Math.abs(change)}{!this.props.type.includes('rank')?"%":""}</span>
    </Grid.Column>)

    platinum_list.push(<Grid.Column className={'text-center-pad mid font'}>
       {platinum}
    </Grid.Column>)

    gold_list.push(<Grid.Column className={'text-center-pad mid font'}>
       {gold}
    </Grid.Column>)

    silver_list.push(<Grid.Column className={'text-center-pad mid font'}>
       {silver}
    </Grid.Column>)

    bronze_list.push(<Grid.Column className={'text-center-pad mid font'}>
       {bronze}
    </Grid.Column>)
    }
    var diff = (moment(this.props.date.end, "YYYY-MM-DD")).diff(moment(this.props.date.start, "YYYY-MM-DD"), 'days')
    var diff_date = moment(this.props.date.start, "YYYY-MM-DD").subtract(diff, "days").format("MMM D, YYYY")

    return (
      <Grid columns={2+this.props.count}  stackable celled>
      <Grid.Row style={{"background": "#91badd2b"}}>
             <Grid.Column className={'text-center-pad mid'}>
             Stories &nbsp;&nbsp;<HelpText data={' Showing stories from ' +moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")+' to '+ moment(this.props.date.end, "YYYY-MM-DD").format("MMM D, YYYY")+" in comparison with stories from "+diff_date+" to "+moment(this.props.date.start, "YYYY-MM-DD").format("MMM D, YYYY")}/>
             </Grid.Column>
             {total_list}
      </Grid.Row>

      <Grid.Row>
             <Grid.Column className={'text-center-pad mid font'}>
             Platinum
             </Grid.Column>
             {platinum_list}
      </Grid.Row>

      <Grid.Row>
             <Grid.Column className={'text-center-pad mid font'}>
             Gold
             </Grid.Column>
             {gold_list}
      </Grid.Row>

      <Grid.Row>
             <Grid.Column className={'text-center-pad mid font'}>
             Silver
             </Grid.Column>
             {silver_list}
      </Grid.Row>

      <Grid.Row>
             <Grid.Column className={'text-center-pad mid font'}>
             Bronze
             </Grid.Column>
             {bronze_list}
      </Grid.Row>
      </Grid>

    )
  }
}

export default AreaCard;
