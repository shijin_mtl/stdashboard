import React, { Component } from 'react'
import { Grid } from 'semantic-ui-react'
import { ResponsiveContainer,
PieChart,Pie,Cell } from 'recharts';
import HelpText from "../HelpText"


const colors = ["#f1c835","#a8689c","#f89565","#8bd3eb","#606b6d"]
const data02 = [
  { name: 'Salman Khan', value: 557 },
  { name: 'Amitabh Bachchan', value: 125 },
  { name: 'Ranveer Singh', value: 124 },
  { name: 'Deepika Padukone', value: 245 },
];

const renderLabelContent = (props) => {
  const { value, x, y, midAngle,name } = props;

  return (
    <g transform={`translate(${x}, ${y})`} textAnchor={ (midAngle < -90 || midAngle >= 90) ? 'end' : 'start'}>
      <text fill="#5b6779" fontWeight="bold" x={0} y={0}>{`${name}: ${value}`}</text>
    </g>
  );
};

class PiePeople extends Component {
  render() {
    return (
      <Grid.Column>
      <div className="og-div"><span className="card-span-small">People who are most talked about in my articles<HelpText data='Shows the persons who are most mentioned in stories about you in given period of time'/></span></div>
          <ResponsiveContainer width='100%' aspect={4.0/2.5}>
          <PieChart margin={{ top:10, right: 0, left: 0, bottom:0 }}>
            <Pie
              data={data02}
              dataKey="value"
              startAngle={180}
              endAngle={-180}
              innerRadius={0}
              outerRadius={100}
              label={renderLabelContent}
              paddingAngle={0}
            >
              {
                data02.map((entry, index) => (
                  <Cell key={`slice-${index}`} fill={colors[index % 10]}/>
                ))
              }
            </Pie>
          </PieChart>
          </ResponsiveContainer>
      </Grid.Column>
    )
  }
}

export default PiePeople;
