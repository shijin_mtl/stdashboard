import React, { Component } from 'react'
import { Header,Table,Image } from 'semantic-ui-react'
import avatar from '../../public/img/sample.png'





class Paper extends Component {



  peopleClick=(id)=>
  {
    this.props.history.push("/read/"+localStorage.saveData+"/"+id+"/3/")
  }


  render() {
    var data = this.props.format
    return (
      <Table.Row>
        <Table.Cell>
        <Image  src={data.name !=="Not Available"?'https://s3.ap-south-1.amazonaws.com/newskeeper-sources/'+data.source_id+'.jpg':avatar} rounded size='mini' />
        </Table.Cell>
        <Table.Cell className={"border-none"}>
          <Header as='h3' image>
            <Header.Content style={{'textTransform':'capitalize','fontSize':'1.3em'}}>
                {data.name}
            </Header.Content>
          </Header>
        </Table.Cell>
        <Table.Cell style={{'textTransform':'capitalize','fontSize':'1em'}} onClick={this.peopleClick.bind(this,data.source_id)}>
            {data.count} Articles
        </Table.Cell>
      </Table.Row>
    )
  }
}

export default Paper;
