import React, { Component } from 'react'
import { Grid, } from 'semantic-ui-react'
import {  LineChart, Line, XAxis, YAxis, Tooltip, ResponsiveContainer} from 'recharts';
import {Gmaps, Marker, InfoWindow, Circle} from 'react-gmaps';
import HelpText from "../HelpText"



const coords = {
  lat: 28.7041,
  lng: 77.1025
};

const params = {v: '3.exp', key: 'AIzaSyAfVpCjHaJLsZ1KZfMluUoDUL5tVxGImzY'};





class LineCard extends Component {

  onMapCreated(map) {
    map.setOptions({
      disableDefaultUI: true
    });
  }

  onDragEnd(e) {
    console.log('onDragEnd', e);
  }

  onCloseClick() {
    console.log('onCloseClick');
  }

  onClick(e) {
    console.log('onClick', e);
  }





  render() {
    var stylez = [
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.neighborhood",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.business",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
]

    return (
            <Grid.Column>
              <Grid.Row>
              <div className="og-div"><span className="card-span-small">{this.props.format.title}<HelpText data='Shows the number of regions in which you have news influence'/></span></div>
              <div className="og-div hike"><span className="digit-span-card">{this.props.format.value}</span> <span style={{color:this.props.format.color}}>({this.props.format.change})</span></div>
              </Grid.Row>
              <Grid.Row>
              <ResponsiveContainer width='100%' aspect={4.0/1}>
                <LineChart data={this.props.format.data}
                        namemargin={{ top:0, right: 0, left: 0, bottom:0 }}
                      >
                    <XAxis dataKey="date" hide={true}/>
                    <YAxis dataKey="Story"  hide={true}/>
                    <Tooltip  cursor={false} />
                    <Line
                      type="monotone"
                      dataKey="Story"
                      stroke="green"
                      strokeWidth="3"
                      dot={false}
                    />
                  </LineChart>
              </ResponsiveContainer>
              </Grid.Row>
              <Grid.Row className="og-card">
              <Gmaps
              width={'100%'}
              height={'180px'}
              styles={stylez}
              lat={coords.lat}
              lng={coords.lng}
              zoom={5}
              loadingMessage={'Be happy'}
              params={params}
              onMapCreated={this.onMapCreated}>
              <Marker
                lat={coords.lat}
                lng={coords.lng}
                draggable={true}
                onDragEnd={this.onDragEnd} />
              <InfoWindow
                lat={coords.lat}
                lng={coords.lng}
                content={'12544'}
                onCloseClick={this.onCloseClick} />
              <Circle
                lat={coords.lat}
                lng={coords.lng}
                radius={500}
                onClick={this.onClick} />
            </Gmaps>
              </Grid.Row>
            </Grid.Column>
    )
  }
}

export default LineCard;
