import React, { Component } from 'react'
import { Table,Grid } from 'semantic-ui-react'
import HelpText from "../HelpText"





class Topic extends Component {
  render() {
    return (
            <Grid.Column>
            <div className="og-div"><span className="card-span-small">Topics which Iam being mentioned<HelpText data='Shows the major topics in which you were mentioned in given period of time'/></span></div>
                <Table striped>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell width={12}>Topics</Table.HeaderCell>
                        <Table.HeaderCell>Percentage</Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>

                    <Table.Body>
                      <Table.Row>
                        <Table.Cell>Business</Table.Cell>
                        <Table.Cell>3</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Entertainment</Table.Cell>
                        <Table.Cell>80</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Sports</Table.Cell>
                        <Table.Cell>1</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Health</Table.Cell>
                        <Table.Cell>5</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Technology</Table.Cell>
                        <Table.Cell>11</Table.Cell>
                      </Table.Row>
                    </Table.Body>
                  </Table>
            </Grid.Column>
    )
  }
}

export default Topic;
