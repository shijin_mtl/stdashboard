import React, { Component } from 'react'
import { Item,Grid } from 'semantic-ui-react'
import HelpText from "../HelpText"






class People extends Component {
  render() {
    return (
      <Grid.Column className={"story"}>
      <div className="og-div"><span className="card-span-small">People who are mostly associated with me <HelpText data='Shows the persons with whom you are mostly associated with in the given period of time'/></span></div>
        <Item.Group link>
          <Item>
            <Item.Image size='tiny' src='http://www.bollywoodlife.com/wp-content/uploads/2017/05/706931.jpg' />
            <Item.Content>
              <Item.Header>Salman Khan </Item.Header>
              <Item.Description> 2542 Articles </Item.Description>
            </Item.Content>
          </Item>
          <Item>
            <Item.Image size='tiny' src='https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cg_face%2Cq_80%2Cw_300/MTE1ODA0OTcxOTg4MDU5NjYx/raavan---uk-film-premiere-red-carpet-arrivals.jpg' />
            <Item.Content>
              <Item.Header>Amitabh Bachchan</Item.Header>
              <Item.Description> 985 Articles</Item.Description>
            </Item.Content>
          </Item>
          <Item>
            <Item.Image size='tiny' src='http://images.desimartini.com/media/uploads/2017-5/ranveer-singh_148231652900.jpg' />
            <Item.Content>
              <Item.Header>Ranveer Singh</Item.Header>
              <Item.Description> 422 Articles</Item.Description>
            </Item.Content>
          </Item>
          <Item>
            <Item.Image size='tiny' src='http://www.freepressjournal.in/wp-content/uploads/2013/05/c-deepika1.jpg' />
            <Item.Content>
              <Item.Header>Deepika Padukone</Item.Header>
              <Item.Description> 356 Articles</Item.Description>
            </Item.Content>
          </Item>
          <Item>
            <Item.Image size='tiny' src='https://www.biography.com/.image/t_share/MTMyNzczNjA0ODM5NDUyNjQy/priyanka-chopra-shutterstock_303288878_1290jpg.jpg' />
            <Item.Content>
              <Item.Header>Priyanka Chopra</Item.Header>
              <Item.Description> 236 Articles</Item.Description>
            </Item.Content>
          </Item>
          <Item>
            <Item.Image size='tiny' src='https://i.pinimg.com/736x/8f/3e/5f/8f3e5faffa29945d2612c4a088e7f336--alia-bhatt-hairdos.jpg' />
            <Item.Content>
              <Item.Header>Alia Bhatt</Item.Header>
              <Item.Description> 150 Articles</Item.Description>
            </Item.Content>
          </Item>
        </Item.Group>
      </Grid.Column>
    )
  }
}

export default People;
