import React, { Component } from 'react'
import { Grid,Table,Button } from 'semantic-ui-react'
import MainRank from "./MainGraph"
import AreaCard from "./AreaCard"
import BarCard from "./BarCard"
import Story from "./TopNews"
import Paper from "./TopPaper"
import HelpText from "../HelpText"
import { viralNewsStore, getNewsSource,getArticle,getMain,getVolume,getSourceVolume } from '../../store/viralNewsStore'
import InfiniteScroll from 'react-infinite-scroller'






class ViralNews extends Component {

  constructor(props) {
    super(props)
    this.store = viralNewsStore()
    this.state = {
      loading: false,
      newspaper_source:[],
      people_source:[],
      article_source:[],
      main:[],
      volume:[],
      track:[],
      source_volume:[]
    }
  }



  showMore =()=> {
    this.props.history.push("/read/")
  }



  componentWillMount() {
    this.store.subscribe(this.onNewsProcess.bind(this))
    this.store.dispatch(getNewsSource({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":"viralnews"}))
    this.store.dispatch(getArticle({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":"viralnews","page":0}))
    this.store.dispatch(getMain({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":"viralnews"}))
    this.store.dispatch(getVolume({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":"viralnews"}))
    this.store.dispatch(getSourceVolume({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    "type":"viralnews"}))
  }



  componentDidMount() {

  }

  componentWillReceiveProps(nextProps)
  {


    this.setState({
      date:nextProps.date,
      track:[],
      }, () => {
          this.forceUpdate()
          // console.log(this.state.track);
          setTimeout(function(){ this.store.dispatch(getArticle({"start_date":nextProps.date.start,"end_date":nextProps.date.end,
          "type":"viralnews",'page':0})) }.bind(this), 1000);
      });




      this.store.dispatch(getNewsSource({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
      "type":"viralnews"}))
      this.store.dispatch(getArticle({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
      "type":"viralnews","page":0}))
      this.store.dispatch(getMain({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
      "type":"viralnews"}))
      this.store.dispatch(getVolume({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
      "type":"viralnews"}))
      this.store.dispatch(getSourceVolume({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,
      "type":"viralnews"}))
  }

  onNewsProcess(response){
          const state = this.store.getState()
          switch(state.type) {
          case "newspaper":
                if(state.success)
                {
                  this.setState({
                    newspaper_source:state.data.source
                  })
                }
              break;
              case "article":
              if(state.success)
              {
                  var tracks=this.state.track
                  if(state.data.articles){
                      state.data.articles.map((data) => {
                                tracks.push(data)
                                return true;
                            });
                    this.setState({
                      track:tracks,
                      hasMoreItems:true
                      })
                  }
                  if(state.data.articles === []){
                  }
              }
              else {
                this.setState({
                  hasMoreItems:false
                })
              }
                  break;
          case "main":
                if(state.success)
                {
                  this.setState({
                    main:state.data.viralnews
                  })
                }
              break;
          case "volume":
                if(state.success)
                {
                  this.setState({
                    volume:state.data.data
                  })
                }
              break;
          case "source_volume":
                if(state.success)
                {

                  this.setState({
                    source_volume:state.data
                  })
                }
              break;
          default:
              ;
      }
  }

  loadItems(page){

    // setTimeout(function () {
    //   this.setState({
    //     hasMoreItems:false
    //   });
    //   setTimeout(function(){ this.store.dispatch(getArticle({ "start_date":this.props.date.start,"end_date":this.props.date.end,
    //   "type":"viralnews","page":page})) }.bind(this), 1000);
    // }.bind(this), 1000);

  }






  render() {
      var story2 = {"title":"Number of publications that talk about me","data":this.state.source_volume.list,'count':this.state.source_volume.count}
    var source_list = [<Table.Row>
      <Table.Cell style={{ 'text-align': 'left','font-weight': 'bold' }}>

      </Table.Cell>
        <Table.Cell style={{ 'text-align': 'left','font-weight': 'bold' }} className={"border-none"}>
          Source
        </Table.Cell>
        <Table.Cell style={{ 'text-align': 'left','font-weight': 'bold' }}>
          Articles
        </Table.Cell>
      </Table.Row>]
    var article_list = []
    if(this.state.newspaper_source){
      this.state.newspaper_source.map((data,index) => {
                            source_list.push(<Paper format={data} key={index} history={this.props.history} />)
                            return true;
                        });
    }

    // if(this.state.article_source){
    //   this.state.article_source.map((data,index) => {
    //                         article_list.push(<Story format={data} key={"article"+index}/>)
    //                         return true;
    //                     });
    // }

    var duplicate = []
    this.state.track.map((data,index) => {
                          if(! duplicate.includes(data.id)){
                            // duplicate.push(data.id)
                            article_list.push(<Story format={data} key={"key"+index}/>)
                          }
                          return true;
                      });
    const loader = <div key={"load"}></div>



    return (
      <div className="side-pad">
        <Grid stackable celled>
            <Grid.Row>
              <MainRank data={this.state.main}/>
            </Grid.Row>
        </Grid>
        <Grid stackable celled columns='three'>
          <Grid.Row>
            <AreaCard format={this.state.volume}/>
            <BarCard format={story2}/>
            <Grid.Column className={"story"}>
            <div className="og-div"><span className="card-span-small">Publications that talk most about me<HelpText data='Shows the most popular publications that writes about you'/></span></div>
            <Table basic='very' celled collapsing style={{'width':'100%'}}>
            <Table.Body>
              {source_list}
            </Table.Body>
          </Table>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Grid stackable celled>
          <Grid.Column width={16} className={"story-hg"}>
          <div className="og-div"><span className="card-span-small">Most Popular Stories<HelpText data='Shows your most popular stories from selected period of time'/></span></div>
          <InfiniteScroll
              pageStart={0}
              loadMore={this.loadItems.bind(this)}
              hasMore={this.state.hasMoreItems}
              loader={loader}>
                  {article_list}
          </InfiniteScroll>


          <div className="filter-div">
            <Button onClick={this.showMore.bind(this)}>Show More</Button>
          </div>


          </Grid.Column>
        </Grid>

      </div>
    )
  }
}

export default ViralNews;
