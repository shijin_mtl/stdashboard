import React, { Component } from 'react'
import { Grid,Table,Checkbox,Popup,Icon } from 'semantic-ui-react'
import {  BarChart, Bar, XAxis, YAxis, Tooltip, ResponsiveContainer} from 'recharts';
import HelpText from "../HelpText"





class BarCard extends Component {

  constructor(props) {
    super(props)
    this.state = {
      main:true,
      help_text:"Change View"
    }
  }
  changeGraph=()=>{
    this.setState({
      main:!this.state.main
    })
  }

  render() {





    if(this.props.format.data){
      console.log(this.props.format);
      var keys = Object.keys(this.props.format.data);
      var last = keys[keys.length-1];


      var platinum =0
      var gold =0
      var silver =0
      var bronze = 0
      // var second_total = 0
      var last_total = 0
      platinum =this.props.format.count.platinum_list
      gold = this.props.format.count.gold_list
      silver = this.props.format.count.silver_list
      bronze = this.props.format.count.bronze_list
      var change = 0

      this.props.format.data.map((data,index) => {

                            if(parseInt(index,10) === parseInt(last-1,10)){
                              // second_total = data.Total
                            }
                            else if (parseInt(index,10) === parseInt(last,10)) {
                              last_total = data.Total
                            }
                            return true
                        });
                        change =  this.props.format.count.prev

    }




    return (
            <Grid.Column>
              <Grid.Row>
              <div className="og-div">
              <Popup
                trigger={<Checkbox onClick={this.changeGraph.bind(this)} slider big style={{ "float": "left"}}/>}
                content= {this.state.help_text}
                position='top center'
              />
              <span className="card-span-small" >{this.props.format.title} <HelpText data='Shows the number of publications that covers your stories over the selected period of time'/></span></div>
              <div className="og-div hike"><span className="digit-span-card">{last_total}</span> <span style={{'color':change < 0?'red':"green"}}><Icon name={change < 0?'caret down':"caret up"}/>{change}%</span></div>
              </Grid.Row>
              <Grid.Row>
              <ResponsiveContainer width='100%' aspect={4.0/1}>
                {this.state.main?<BarChart data={this.props.format.data}
                        namemargin={{ top:0, right: 0, left: 0, bottom:0 }}
                      >
                    <XAxis dataKey="Date" hide={true}/>
                    <YAxis dataKey="Total"   hide={true}/>
                    <Tooltip  cursor={false} />
                    <Bar
                      type="monotone"
                      dataKey="Total"
                      stroke="green"
                      strokeWidth="0"
                      fillOpacity=".5"
                      fill="green"
                    />
                  </BarChart>:
                  <BarChart data={this.props.format.data}
                          namemargin={{ top:0, right: 0, left: 0, bottom:0 }}
                        >
                      <XAxis dataKey="Date" hide={true}/>
                      <YAxis dataKey="Platinum"  hide={true}/>
                      <Tooltip  cursor={false} />
                      <Bar
                        type="monotone"
                        dataKey="Platinum"
                        stroke="red"
                        strokeWidth="0"
                        fillOpacity=".5"
                        fill="red"
                        stackId="a"
                      />
                      <Bar
                        type="monotone"
                        dataKey="Gold"
                        stroke="blue"
                        strokeWidth="0"
                        fillOpacity=".5"
                        fill="blue"
                        stackId="a"
                      />
                      <Bar
                        type="monotone"
                        dataKey="Silver"
                        stroke="black"
                        strokeWidth="0"
                        fillOpacity=".5"
                        fill="green"
                        stackId="a"
                      />
                      <Bar
                        type="monotone"
                        dataKey="Bronze"
                        stroke="green"
                        strokeWidth="0"
                        fillOpacity=".5"
                        fill="black"
                        stackId="a"
                      />
                    </BarChart>}
              </ResponsiveContainer>
              </Grid.Row>
              <Grid.Row className="og-card">
              <Table basic='very'>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell>Platinum</Table.Cell>
                      <Table.Cell>{platinum}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Gold</Table.Cell>
                      <Table.Cell>{gold}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Silver</Table.Cell>
                      <Table.Cell>{silver}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Bronze</Table.Cell>
                      <Table.Cell>{bronze}</Table.Cell>
                    </Table.Row>
                  </Table.Body>
                  </Table>
              </Grid.Row>
            </Grid.Column>
    )
  }
}

export default BarCard;
