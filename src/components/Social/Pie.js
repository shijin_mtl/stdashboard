import React, { Component } from 'react'
import { Grid } from 'semantic-ui-react'
import { ResponsiveContainer,
PieChart,Pie,Cell } from 'recharts';
import HelpText from "../HelpText"


const colors = ["#8bd3eb","#606b6d","#f1c835","#a8689c","#f89565"]
const data02 = [
  { name: 'Twitter', value: 17.3 },
  { name: 'Facebook', value: 70 },
];

const renderLabelContent = (props) => {
  const { value, percent, x, y, midAngle,name } = props;

  return (
    <g transform={`translate(${x}, ${y})`} textAnchor={ (midAngle < -90 || midAngle >= 90) ? 'end' : 'start'}>
      <text fill="#5b6779" fontWeight="bold" x={0} y={0}>{`${name}: ${value}`}</text>
      <text fill="#5b6779" fontWeight="bold"  x={0} y={20}>{`${(percent * 100).toFixed(2)}%`}</text>
    </g>
  );
};

class PieRank extends Component {
  onPieEnter = (data, index, e) => {
    this.setState({
      activeIndex: index,
    });
  };

  state = {
    activeIndex: 0,
    animation: false,
  };

  render() {
    return (
      <Grid.Column width={5}>
          <div className="og-div"><span className="card-span-small">Social Media Score Distribution <HelpText data='Shows how score in each social media category contributes to your current Newspaper ST Score'/></span></div>
          <ResponsiveContainer width='100%' aspect={4.0/2.5}>
          <PieChart margin={{ top: 5, right: 10, bottom: 0, left: 10 }}>
            <Pie
              data={data02}
              dataKey="value"
              startAngle={180}
              endAngle={-180}
              innerRadius={70}
              outerRadius={100}
              label={renderLabelContent}
              paddingAngle={5}
              isAnimationActive={this.state.animation}
            >
              {
                data02.map((entry, index) => (
                  <Cell key={`slice-${index}`} fill={colors[index % 10]}/>
                ))
              }
            </Pie>
          </PieChart>
          </ResponsiveContainer>
      </Grid.Column>
    )
  }
}

export default PieRank;
