import React, { Component } from 'react'
import { Grid } from 'semantic-ui-react'
import {  AreaChart, Area, XAxis, YAxis, Tooltip, ResponsiveContainer} from 'recharts';
import HelpText from "../HelpText"





class AreaCard extends Component {
  render() {
    return (
            <Grid.Column>
              <Grid.Row>
              <div className="og-div"><span className="card-span-small">{this.props.format.title} <HelpText data='Shows the number of stories about you over the selected period of time'/></span></div>
              <div className="og-div hike"><span className="digit-span-card">{this.props.format.value}</span> <span style={{color:this.props.format.color}}>({this.props.format.change})</span></div>
              </Grid.Row>
              <Grid.Row>
              <ResponsiveContainer width='100%' aspect={4.0/1}>
                <AreaChart data={this.props.format.data}
                        namemargin={{ top:0, right: 0, left: 0, bottom:0 }}
                      >
                      <defs>
                    <linearGradient id="MyAreaGradient" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="10%" stopColor="#3b5999" />
                    <stop offset="95%" stopColor="#55acee" />
                    </linearGradient>
                  </defs>

                    <XAxis dataKey="date" hide={true}/>
                    <YAxis dataKey="Score"  hide={true}/>
                    <Tooltip  cursor={false} />
                    <Area
                      type="monotone"
                      dataKey="Score"
                      stroke="green"
                      strokeWidth="0"
                      fillOpacity=".5"
                      fill="url(#MyAreaGradient)"
                    />
                  </AreaChart>
              </ResponsiveContainer>
              </Grid.Row>
            </Grid.Column>
    )
  }
}

export default AreaCard;
