import React, { Component } from 'react'
import { Grid } from 'semantic-ui-react'
import LineCard from "./LineCard"
import BarCard from "./BarCard"
import Timeline from "./Timeline"
import { socialStore,mainFetch,detailFetch,postFetch } from '../../store/socialStore'




class Social extends Component {

  constructor(props) {
    super(props)
    this.store = socialStore()
    this.state = {
      loading: false,
      fb_main:[],
      tw_main:[],
      fb_detail:[],
      tw_detail:[],
      post:[]
    }
  }

  componentWillMount() {
    this.store.subscribe(this.onProcess.bind(this))
    this.store.dispatch(mainFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"type":'facebook'}))
    this.store.dispatch(mainFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"type":'twitter'}))
    this.store.dispatch(detailFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"type":'facebook'}))
    this.store.dispatch(detailFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"type":'twitter'}))
    this.store.dispatch(postFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"type":'twitter'}))
  }
  componentDidMount() {

  }

  componentWillReceiveProps(nextProps)
  {
    this.setState({
      date:nextProps.date,
      })
      this.store.dispatch(mainFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"type":'facebook'}))
      this.store.dispatch(mainFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"type":'twitter'}))
      this.store.dispatch(detailFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"type":'facebook'}))
      this.store.dispatch(detailFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"type":'twitter'}))
      this.store.dispatch(postFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"type":'twitter'}))
  }

  onProcess(response){
    const state = this.store.getState()
    if(state.success)
      {
        if(state.data.twitter){
          this.setState({
            tw_main:state.data.twitter,
          })
        }
        else if(state.data.facebook){
          this.setState({
            fb_main:state.data.facebook
          })
        }
        else if(state.type === "post")
        {
          this.setState({
            post:state.data.posts
          })
        }
        else {
          var detail_data = state.data.data
          if(detail_data.facebook_followers){
            this.setState({
              fb_detail:detail_data
            })
          }
          if(detail_data.twitter_followers){
            this.setState({
              tw_detail:detail_data
            })
          }
        }


      }
      }
  render() {

    var line1 = {"title":"Facebook Posts","data":this.state.fb_detail.facebook_post_number,"text":"Shows Facebook posts over selected period of time"}
    var line2 = {"title":"Facebook Shares","data":this.state.fb_detail.facebook_shares,"text":"Shows Facebook shares over selected period of time"}
    var line3 = {"title":"Facebook Likes","data":this.state.fb_detail.facebook_post_likes,"text":"Shows Facebook likes over selected period of time"}
    var line4 = {"title":"Twitter Posts","data":this.state.tw_detail.twitter_tweets,"text":"Shows Twitter tweets over selected period of time" }
    var line5 = {"title":"Twitter Retweets","data":this.state.tw_detail.twitter_retweets, "text":"Shows Twitter Retweets over selected period of time"}
    var line6 = {"title":"Twitter Favourites","data":this.state.tw_detail.twitter_favorites, "text":"Shows Twitter Favourites over selected period of time"}


    var fb = {"title":"Facebook ST Score","data":this.state.fb_main}
    var tw = {"title":"Twitter ST Score","data":this.state.tw_main}



    return (
      <div className="side-pad">
        {/*<Grid stackable celled>
            <Grid.Row>
              <MainRank/>
              <PieRank/>
            </Grid.Row>
        </Grid>*/}
        <Grid stackable celled columns='two'>
        <Grid.Column>
            <Grid.Row>
                <BarCard format={fb}/>
            </Grid.Row>
            <Grid.Row>
                <LineCard format={line1}/>
            </Grid.Row>
            <Grid.Row>
                <LineCard format={line3}/>
            </Grid.Row>
            <Grid.Row>
                <LineCard format={line2}/>
            </Grid.Row>
        </Grid.Column>
        <Grid.Column>
            <Grid.Row>
                <BarCard format={tw}/>
            </Grid.Row>
            <Grid.Row>
                <LineCard format={line4}/>
            </Grid.Row>
            <Grid.Row>
                <LineCard format={line5}/>
            </Grid.Row>
            <Grid.Row>
                <LineCard format={line6}/>
            </Grid.Row>
        </Grid.Column>
        </Grid>

          {/*<FeedFb/>
          <FeedTwitter/>*/}
          <Timeline data={this.state.post}/>
      </div>
    )
  }
}

export default Social;
