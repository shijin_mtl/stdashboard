import React, { Component } from 'react'
import { Grid } from 'semantic-ui-react'
import { ResponsiveContainer, LineChart, Line, XAxis, YAxis,
  Tooltip, Legend,CartesianGrid } from 'recharts';


const data = [
  { date: 'sep 07, 2017', Score: 77.6 ,Rank:2},
  { date: 'sep 14, 2017', Score: 49.44 ,Rank:3 },
  { date: 'sep 21, 2017', Score: 60.32 ,Rank:3},
  { date: 'sep 28, 2017', Score: 44.11 ,Rank:4 },
  { date: 'oct 05, 2017', Score: 33.35,Rank:1 },
  { date: 'oct 12, 2017', Score: 73.6 ,Rank:3},
  { date: 'oct 19, 2017', Score: 63.9209  ,Rank:1 },
  { date: 'oct 26, 2017', Score: 53.9209 ,Rank:2},
];

class MainRank extends Component {
  state = {
    activeIndex: 0,
    animation: false,
  };
  render() {
    return (
            <Grid.Column width={16}>
            {/*<div className="og-div"><span className="rank-span">Social Media Rank<HelpText data='Shows your Social media Rank and Social media ST Score over the selected time period'/></span> <span>#2&nbsp;<span className="og-div hike">(+2)</span></span></div>*/}
            <div className="og-div"><span className="rank-span">Social Media ST Score</span> <span className="digit-span">65.5&nbsp; <span className="og-div down">(-6.57%)</span></span></div>

            <ResponsiveContainer width='100%' aspect={4.0/1.0}>
            <LineChart
                  data={data}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                <defs>
              <linearGradient id="MyGradient" x1="0" y1="0" x2="0" y2="1">
                <stop offset="10%" stopColor="rgba(0, 136, 254, 0.8)" />
                <stop offset="80%" stopColor="#ed712c70" />
                <stop offset="95%" stopColor="red" />
              </linearGradient>
            </defs>

                  <Legend />
                  <XAxis dataKey='date'/>
                  <CartesianGrid  horizontal={true} vertical={false} verticalFill={['#555555', '#444444']} fillOpacity={0.1} />
                  <YAxis dataKey='Score' orientation='left' yAxisId={1}  hide={true}/>
                  {/*<YAxis dataKey="Rank" orientation='right' yAxisId={2} hide={true}/>*/}
                  <Tooltip cursor={false} />
                  <Line  dataKey='Score' type='monotone' strokeWidth={3} stroke='#3f84f7' dot={true} yAxisId={1}  />
                  {/*<Line type='monotone' dataKey='Rank' stroke='#f4b532' strokeWidth={2} dot={true} yAxisId={2}/>*/}
            </LineChart>
            </ResponsiveContainer>
            </Grid.Column>
    )
  }
}

export default MainRank;
