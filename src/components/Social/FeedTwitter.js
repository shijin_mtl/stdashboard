import React, { Component } from 'react'
import { Feed,Icon,Segment,Menu } from 'semantic-ui-react'
import { socialStore,getSocial} from '../../store/socialStore'
import {  LineChart, Line, XAxis, Tooltip, ResponsiveContainer} from 'recharts';
import moment from 'moment'





class FeedTwitter extends Component {

  constructor(props) {
    super(props)
    this.store = socialStore()
    this.state = {
      loading: false,
      content:this.props.data.content,
      post:this.props.data.content,
      active:'post'
    }
  }

  componentWillMount() {
    this.store.subscribe(this.onProcess.bind(this))

  }


  onProcess(response){
    const state = this.store.getState()
    if(state.success)
      {
        var list = []
        if(state.data.likes){

          let keys = Object.keys(state.data.likes)
          keys.sort(function(a,b){
            return new Date(a) - new Date(b);
          });

          for (let key in keys) {
                list.push({ Date: moment(keys[key], "YYYY-MM-DD").format("MMM D, YYYY"), Count:state.data.likes[keys[key]]},)
          }
          this.setState({
            active:'likes'
            })

        }
        else if(state.data.comments){
          let keys = Object.keys(state.data.comments)
          keys.sort(function(a,b){
            return new Date(a) - new Date(b);
          });

          for (let key in keys) {
                list.push({ Date: moment(keys[key], "YYYY-MM-DD").format("MMM D, YYYY"), Count:state.data.comments[keys[key]]},)
          }
          this.setState({
            active:'comments'
            })
        }
        else if(state.data.shares){
          let keys = Object.keys(state.data.shares)
          keys.sort(function(a,b){
            return new Date(a) - new Date(b);
          });

          for (let key in keys) {
                list.push({ Date: moment(keys[key], "YYYY-MM-DD").format("MMM D, YYYY"), Count:state.data.shares[keys[key]]},)
          }
          this.setState({
            active:'shares'
            })
        }
        else {
          ;
        }


        this.setState({
          content:[
            <ResponsiveContainer width='100%' aspect={4.0/1} key>
              <LineChart data={list}
                margin={{ top: 10, right: 30, left: 30, bottom: 30 }}>
                  <Tooltip />
                  <XAxis dataKey="Date" hide={true}  />
                  <Line type='monotone' dataKey='Count' strokeWidth={2}/>
                </LineChart>
            </ResponsiveContainer>]
        })
      }
      }



      clickSocial(post,filter){
        if(filter === "post"){
            this.setState({
              content:this.state.post,
              active:"post"
            })
        }
        else {
          this.store.dispatch(getSocial({'post':post,'filter':filter }))
        }

      }

  render() {


    const TabExampleAttachedBottom = () => (
      <div>
        <Segment attached>
        <Feed.Summary className="post-head">
        <Icon name='twitter' className="twitter-post" /> {moment(this.props.data.date_posted.slice(0, 10) , "YYYY-MM-DD").format("MMM D, YYYY")} | Type: {this.props.data.type}
        </Feed.Summary>
        <Feed.Extra text><b>
          {this.state.content}</b>
                </Feed.Extra>
        </Segment>

        <Menu attached='bottom' fluid widths={6}>
          <Menu.Item
            name='section1'
          >
            <span className="social-span">{this.props.data.likes<500?this.props.data.likes:Math.round( this.props.data.likes/1000  ) / 10  +"K"} </span>&nbsp;  Favourites
          </Menu.Item>

          <Menu.Item
            name='section2'
          >
            <span className="social-span">{this.props.data.shares<500?this.props.data.shares:Math.round( this.props.data.shares/1000  ) / 10  +"K"}   </span> &nbsp;Retweets
          </Menu.Item>

          <Menu.Item
            name='section2'
          >
             <span className="social-span">{this.props.data.comments<500?this.props.data.comments:Math.round( this.props.data.comments/1000  ) / 10  +"K"} </span>&nbsp; Comments
          </Menu.Item>
          <Menu.Item
            name='section2'
          >
             <span className="social-span">{this.props.data.engagement_score<500?this.props.data.engagement_score:Math.round( this.props.data.engagement_score/1000  ) / 10  +"K"}</span> &nbsp;Engagement Score
          </Menu.Item>
          <Menu.Item
            name='section2'
            target="blank" href={this.props.data.permalink}

          >
            Link
          </Menu.Item>
          <Dropdown item icon='bar graph' simple>
        <Dropdown.Menu>
          <Dropdown.Item active={this.state.active==="likes"} onClick={this.clickSocial.bind(this,this.props.data.post_id,'likes')}>Favorites</Dropdown.Item>
          <Dropdown.Item active={this.state.active==="shares"} onClick={this.clickSocial.bind(this,this.props.data.post_id,'shares')}>Retweets</Dropdown.Item>
          <Dropdown.Item active={this.state.active==="comments"} onClick={this.clickSocial.bind(this,this.props.data.post_id,'comments')}>Comments</Dropdown.Item>
          {this.state.active!=="post"?<Dropdown.Item active={this.state.active==="post"} onClick={this.clickSocial.bind(this,this.props.data.post_id,'post')}>Tweet</Dropdown.Item>:""}
        </Dropdown.Menu>
      </Dropdown>
        </Menu>
      </div>

    )




    return (
      <Feed>
  {/*<Feed.Event>
    <Feed.Content>

      <Feed.Summary className="post-head">
      <Icon name='facebook' className="fb-post" /> {moment(this.props.data.date_posted.slice(0, 10) , "YYYY-MM-DD").format("MMM D, YYYY")} | Type: {this.props.data.type}
      </Feed.Summary>
      <Feed.Extra text><b>
        {this.state.content}</b>
              </Feed.Extra>
              {this.state.content === this.state.post?<Feed.Extra images>
                {this.props.data.type==="photo"?<a><img alt='img' src={this.props.data.media_link} /></a>:""}
              </Feed.Extra>:""}
              <Button.Group basic stackable>
                  {this.state.active!=="post"?<Button  active={this.state.active==="post"} onClick={this.clickSocial.bind(this,this.props.data.post_id,'post')} >Post</Button>:""}
                  <Button active={this.state.active==="likes"} onClick={this.clickSocial.bind(this,this.props.data.post_id,'likes')}>
                  {this.props.data.likes} Likes</Button>
                  <Button active={this.state.active==="shares"} onClick={this.clickSocial.bind(this,this.props.data.post_id,'shares')}>
                  {this.props.data.shares} Shares</Button>
                  <Button active={this.state.active==="comments"} onClick={this.clickSocial.bind(this,this.props.data.post_id,'comments')} >
                  {this.props.data.comments} Comments</Button>
                  <Button target="blank" href={this.props.data.permalink}><Icon name='external' />
                  Link</Button>
                </Button.Group>



            </Feed.Content>
          </Feed.Event>*/}


            <TabExampleAttachedBottom/>


          <br/>
          </Feed>
            )
          }
        }

export default FeedTwitter;
