import React, { Component } from 'react'
import { Grid } from 'semantic-ui-react'
import {  BarChart, Bar, XAxis, YAxis, Tooltip, ResponsiveContainer} from 'recharts';
import HelpText from "../HelpText"
import moment from 'moment'






class BarCard extends Component {
  render() {

    var list = []
    if(this.props.format.data !==[] && this.props.format.data !== undefined)
    {
      for (var key in this.props.format.data.scores) {
            list.push({ date: moment(key, "YYYY-MM-DD").format("MMM D, YYYY"), Score: this.props.format.data.scores[key]},)
            var score = this.props.format.data.scores[key]
      }
    }
    var change =this.props.format.data?this.props.format.data.change:""

    return (
            <Grid.Column>
              <Grid.Row>
              <div className="og-div"><span className="card-span-small">{this.props.format.title} <HelpText data='Shows score over selected period of time'/></span></div>
              <div className="og-div hike"><span className="digit-span-card">{score}</span> <span style={{'color':change < 0?'red':"green"}}>({ Math.round(change * 100) / 100 +"%"})</span></div>
              </Grid.Row>
              <Grid.Row>
              <ResponsiveContainer width='100%' aspect={4.0/1}>
                <BarChart data={list}
                        namemargin={{ top:0, right: 0, left: 0, bottom:0 }}
                      >
                      <defs>
                    <linearGradient id="MyBarGradient" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="10%" stopColor="#3b5999" />
                    <stop offset="95%" stopColor="#55acee" />
                    </linearGradient>
                  </defs>
                    <XAxis dataKey="date"/>
                    <YAxis dataKey="Score"  hide={true}/>
                    <Tooltip  cursor={false} />
                    <Bar
                      type="monotone"
                      dataKey="Score"
                      stroke="black"
                      strokeWidth="0"
                      fillOpacity="1"
                      fill="url(#MyBarGradient)"
                      background={{ fill: '#eee' }}
                    />
                  </BarChart>
              </ResponsiveContainer>
              </Grid.Row>
            </Grid.Column>
    )
  }
}

export default BarCard;
