import React, { Component } from 'react'
import { Grid,Icon } from 'semantic-ui-react'
import { ResponsiveContainer, LineChart, Line, XAxis, YAxis,
  Tooltip, Legend,CartesianGrid } from 'recharts';
import HelpText from "../HelpText"
import moment from 'moment'


class MainRank extends Component {
  state = {
    activeIndex: 0,
    animation: false,
  };
  render() {

    var list =[]
    var change = 0
    var last = 0
    var first = 0
    var count = 0
    if(this.props.data !==[] && this.props.data !==undefined)
    {
      for (var key in this.props.data.scores) {
            list.push({ date: moment(key, "YYYY-MM-DD").format("MMM D, YYYY"), Score: this.props.data.scores[key]},)
            var score = this.props.data.scores[key]
            if(count ===0){
              first = this.props.data.scores[key]
              count =1
            }
            last = this.props.data.scores[key]
      }
      change = last - first


    }



    return (
            <Grid.Column width={16}>
            <div className="og-div"><span className="rank-span">Newspaper ST Score</span> <span >{score}&nbsp; <span className="og-div down" style={{'color':change < 0?'red':"green",'display':change === 0?'none':""}}><Icon name={change < 0?'caret down':"caret up"}/>{ Math.round(change * 100) / 100 }</span></span><HelpText data='Shows your Newspaper Rank and Newspaper ST Score over the selected time period'/></div>

            <ResponsiveContainer width='100%' aspect={4.0/1.0}>
            <LineChart
                  data={list}
                  margin={{ top: 5, right: 50, bottom: 0, left: 50 }}
                  syncId="mainGraph"
                >
                <defs>
              <linearGradient id="MyGradient" x1="0" y1="0" x2="0" y2="1">
                <stop offset="10%" stopColor="rgba(0, 136, 254, 0.8)" />
                <stop offset="80%" stopColor="#ed712c70" />
                <stop offset="95%" stopColor="red" />
              </linearGradient>
            </defs>
                  <Legend />
                  <XAxis dataKey='date'/>
                  <CartesianGrid  horizontal={true} vertical={false} verticalFill={['#555555', '#444444']} fillOpacity={0.1} />
                  <YAxis dataKey='Score' orientation='left' yAxisId={1} hide={true}/>
                  <Tooltip cursor={false} />
                  <Line  type="monotone" dataKey='Score' strokeWidth={3} stroke='#f4b532' dot={true} yAxisId={1} />
            </LineChart>
            </ResponsiveContainer>
            </Grid.Column>
    )
  }
}

export default MainRank;
