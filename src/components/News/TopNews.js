import React, { Component } from 'react'
import { Feed,Image,Accordion,Grid } from 'semantic-ui-react'
import moment from 'moment'
import Truncate from 'react-truncate';



class Story extends Component {

  state = { activeIndex: 1 }

  handleClick = (e, titleProps) => {
    const { index } = titleProps
    const { activeIndex } = this.state
    const newIndex = activeIndex === index ? 0 : index

    this.setState({ activeIndex: newIndex })
  }



  render() {
    return (


          <Accordion styled style={{'width': '100%'}}>
              <Accordion.Title active={true} index={0} onClick={this.handleClick}>
              <Grid>

              <Grid.Column width={1}>
                <Image src={"https://s3.ap-south-1.amazonaws.com/newskeeper-sources/"+this.props.format.source+".jpg"} size='small' floated='left' className={"small-img"} />
              </Grid.Column>

              <Grid.Column width={15}>
                <Feed.Summary >
                  <h3  as='a' href={this.props.format.url} target="_blank" style={{"color": "#4183c4"}}><b>{this.props.format.title?this.props.format.title:"No Title"}</b></h3>
                  <span>{this.props.format.source_name} &nbsp; &nbsp; {moment(this.props.format.date, "YYYY-MM-DD").format("MMM D, YYYY")}</span>
                </Feed.Summary>
              </Grid.Column>
              </Grid>
              </Accordion.Title>
              <Accordion.Content active={true}>
                <p>
                <Truncate lines={5} ellipsis={<span>... <a href={this.props.format.url}>Read more</a></span>}>
                    {this.props.format.content}
                </Truncate>
                </p>
              </Accordion.Content>
            </Accordion>

    )
  }
}


export default Story;
