import React, { Component } from 'react'
import { Item,Grid,Table,Header,Image } from 'semantic-ui-react'
import HelpText from "../HelpText"
import avatar from '../../public/img/sample.png'



class People extends Component {

peopleClick=(id)=>
{
  this.props.history.push("/read/"+id)
}

  render() {
    var list = []
    var table = [

      <Table.Row>
        <Table.Cell style={{ 'text-align': 'left','font-weight': 'bold' }}>
          Celebrity
        </Table.Cell>
        <Table.Cell style={{ 'text-align': 'left','font-weight': 'bold' }}>
          Articles
        </Table.Cell>
      </Table.Row>

  ]


    this.props.format.map((data,index) => {
                          list.push(<Item>
                            <Item.Image size='tiny' src={'https://s3-us-west-2.amazonaws.com/interface-images/'+data.nub_id+'.jpg'} />
                            <Item.Content>
                              <Item.Header style={{'textTransform':'capitalize'}}>{data.name} </Item.Header>
                              <Item.Description> {data.count} Articles </Item.Description>
                            </Item.Content>
                          </Item>)
                          return true;
                      });

      this.props.format.map((data,index) => {
                            table.push(<Table.Row>
                              <Table.Cell>
                                <Header as='h3' image>
                                  <Image src={data.name !=="Not Available"?'https://s3-us-west-2.amazonaws.com/interface-images/'+data.nub_id+'.jpg':avatar} rounded size='mini' />
                                  <Header.Content style={{'textTransform':'capitalize','fontSize':'1.3em'}}>
                                      {data.name}
                                  </Header.Content>
                                </Header>
                              </Table.Cell>
                              <Table.Cell style={{'textTransform':'capitalize','fontSize':'1em'}} onClick={this.peopleClick.bind(this,data.nub_id)}>
                                  {data.count} Articles
                              </Table.Cell>
                            </Table.Row>)
                            return true;
                        });

    return (
      <Grid.Column >
      <div className="og-div"><span className="card-span-small">People who are mostly associated with me <HelpText data='Shows the persons with whom you are mostly associated with in the given period of time'/></span></div>
        {/*<Item.Group link>
          {list}
        </Item.Group>*/}


        <Table basic='very' celled collapsing style={{'width':'100%'}}>
    <Table.Body>
      {table}
    </Table.Body>
  </Table>




      </Grid.Column>
    )
  }
}

export default People;
