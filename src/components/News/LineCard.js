import React, { Component } from 'react'
import { Grid,Table,Icon } from 'semantic-ui-react'
import { Modal } from 'semantic-ui-react'
import {    XAxis, YAxis, Tooltip, ResponsiveContainer,BarChart, Bar} from 'recharts';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import  InfoBox  from "react-google-maps/lib/components/addons/InfoBox"
import HelpText from "../HelpText"




class MarkerHere extends Component {


  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
    }
  }
  onToggleOpen=() =>{
    this.setState({
      isOpen:!this.state.isOpen
    })
  }

  render() {

    return (

      <Marker
        position={{ lat: parseInt(this.props.data.lat,10), lng: parseInt(this.props.data.long,10) }}
        onClick={this.onToggleOpen}
        // icon= {"https://d30y9cdsu7xlg0.cloudfront.net/png/4096-200.png"}
      >

      {this.state.isOpen &&   <InfoBox
              options={{ closeBoxURL: ``, enableEventPropagation: true }}
              onCloseClick={this.onToggleOpen}
            >
              <div style={{ backgroundColor: `white`, opacity: 1, padding: `12px`,textTransform: 'capitalize' }}>
                <div style={{ fontSize: `16px`, fontColor: `#08233B` }}>
                  {this.props.data.Place} Count:{this.props.data.Count}
                </div>
              </div>
            </InfoBox>
          }
      </Marker>
    )
  }
}



class LineCard extends Component {
  state = { open: false }
  show = dimmer => () => this.setState({ dimmer, open: true })
  close = () => this.setState({ open: false })
  render() {
  const { open, dimmer } = this.state
  var table =[]
  var info = []

if(this.props.data){
  this.props.data.map((data,index) => {
                        if(data.lat){
                          table.push(<Table.Row>
                            <Table.Cell><Icon  name='dot circle outline' color="red" />{data.Place} </Table.Cell>
                            <Table.Cell>{data.Count} Articles</Table.Cell>
                          </Table.Row>)

                          info.push(
                            <MarkerHere data={data} />
                          )
                        }
                        return true;
                    });
}

const MyMapComponent = withScriptjs(withGoogleMap (
    (props) =>
  <GoogleMap
    defaultZoom={4.8}
    defaultCenter={{ lat:20.5937, lng: 78.9629 }}
  >
  {props.list}
  </GoogleMap>
))

    return (
            <Grid.Column>
              <Grid.Row>
              <div className="og-div"><span className="card-span-small">{this.props.format.title}<HelpText data='Shows regions in which you have news influence'/></span></div>
              </Grid.Row>
              <Grid.Row>
              <ResponsiveContainer width='100%' aspect={4.0/1}>
                <BarChart data={this.props.data}
                        namemargin={{ top:0, right: 0, left: 0, bottom:0 }}
                      >
                    <XAxis dataKey="Place" hide={true}/>
                    <YAxis dataKey="Count"  hide={true}/>
                    <Tooltip  cursor={false} />
                    <Bar
                      dataKey="Count"
                      dot={false}
                      fillOpacity=".5"
                      fill="green"
                      name="Article Count"
                    />
                  </BarChart>
              </ResponsiveContainer>
              </Grid.Row>
              <Grid.Row className="og-card map">
              <Table basic='very' onClick={this.show('blurring')} >
                <Table.Body>
                {table}
                </Table.Body>
              </Table>
          {  /*<Gmaps
            width={'100%'}
            height={'180px'}
            styles={stylez}
            onClick={this.show('blurring')}
            lat={coords.lat}
            lng={coords.lng}
            zoom={5}
            loadingMessage={'Please Wait'}
            params={params}
            onMapCreated={this.onMapCreated}>
            {list}
            <Circle
              lat={coords.lat}
              lng={coords.lng}
              radius={500}
               />
          </Gmaps>*/}
              </Grid.Row>


        <Modal dimmer={dimmer} open={open} onClose={this.close}>
          <Modal.Content>
            <Modal.Description>
            <MyMapComponent
              isMarkerShown
              googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyDRrZKh-Hv0uOm6v_YxoLRELRORTuYVkBQ"
              loadingElement={<div style={{ height: `100%` }} />}
              containerElement={<div style={{ height: `700px` }} />}
              mapElement={<div style={{ height: `100%` }} />}
              list={info}
              />
            </Modal.Description>
          </Modal.Content>
        </Modal>
            </Grid.Column>
    )
  }
}

export default LineCard;
