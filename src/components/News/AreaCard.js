import React, { Component } from 'react'
import { Grid,Table, Checkbox ,Popup,Icon} from 'semantic-ui-react'
import {  AreaChart, Area, XAxis, YAxis, Tooltip, ResponsiveContainer} from 'recharts';
import HelpText from "../HelpText"
import moment from 'moment'






class AreaCard extends Component {

  constructor(props) {
    super(props)
    this.state = {
      main:true,
      help_text:"Change View"
    }
  }
  changeGraph=()=>{
    this.setState({
      main:!this.state.main,
    })
  }


  render() {
    var list = []
    var gold = 0
    var platinum = 0
    var silver = 0
    var bronze = 0
    var total = 0
    var second_total = 0
    var last_total = 0

    var count = 0



    for (var key in this.props.format ){

      var keys = Object.keys(this.props.format);
      var last = keys[keys.length-1];

      let data = this.props.format[key]
      var temp_data = {}
      temp_data['Gold'] = data.newspaper_storyvolume_gold
      gold = gold+data.newspaper_storyvolume_gold
      temp_data['Platinum'] = data.newspaper_storyvolume_platinum
      platinum = platinum+data.newspaper_storyvolume_platinum
      temp_data['Bronze'] = data.newspaper_storyvolume_bronze
      bronze = bronze+data.newspaper_storyvolume_bronze
      temp_data['Silver'] = data.newspaper_storyvolume_silver
      silver = silver+data.newspaper_storyvolume_silver
      temp_data['Date'] = moment(data.date.slice(0, 10), "YYYY-MM-DD").format("MMM D, YYYY")
      temp_data['Total'] = data.newspaper_storyvolume_gold+data.newspaper_storyvolume_platinum+data.newspaper_storyvolume_silver+data.newspaper_storyvolume_bronze
      total = total+temp_data['Total']
      list.push(temp_data)
      if(count ===0){
        second_total = temp_data['Total']
        count = 1
      }
      else if (parseInt(key,10) === parseInt(last,10)) {
        last_total = temp_data['Total']
      }
      else {
        ;
      }
    }
    var change =   Math.round(((last_total-second_total)/second_total)*100 * 100) / 100
    return (
            <Grid.Column>
              <Grid.Row>
              <div className="og-div">
              <Popup
                trigger={<Checkbox onClick={this.changeGraph.bind(this)} slider big style={{ "float": "left"}}/>}
                content= {this.state.help_text}
                position='top center'
              />
              <span className="card-span-small" >Number of Stories <HelpText data='Shows the number of stories about you over the selected period of time'/></span></div>
              <div className="og-div hike"><span className="digit-span-card">{total}</span> <span style={{'color':change < 0?'red':"green",'display':change === 0?'none':""}}><Icon name={change < 0?'caret down':"caret up"}/>{change}%</span></div>
              </Grid.Row>
              <Grid.Row>
              <ResponsiveContainer width='100%' aspect={4.0/1}>
                {this.state.main?<AreaChart data={list}
                        namemargin={{ top:0, right: 0, left: 0, bottom:0 }}
                      >
                    <XAxis dataKey="Date" hide={true}/>
                    <YAxis dataKey="Total"  hide={true}/>
                    <Tooltip  cursor={false} />
                    <Area
                      type="monotone"
                      dataKey="Total"
                      stroke="green"
                      strokeWidth="0"
                      fillOpacity=".5"
                      fill="green"
                    />
                  </AreaChart>:
                  <AreaChart data={list}
                          namemargin={{ top:0, right: 0, left: 0, bottom:0 }}
                        >
                      <XAxis dataKey="Date" hide={true}/>
                      <YAxis dataKey="Platinum"  hide={true} />
                      <Tooltip  cursor={false} />
                      <Area
                        type="monotone"
                        dataKey="Platinum"
                        stroke="red"
                        strokeWidth="0"
                        fillOpacity=".5"
                        fill="red"
                      />
                      <Area
                        type="monotone"
                        dataKey="Gold"
                        stroke="blue"
                        strokeWidth="0"
                        fillOpacity=".3"
                        fill="blue"
                      />
                      <Area
                        type="monotone"
                        dataKey="Silver"
                        stroke="green"
                        strokeWidth="0"
                        fillOpacity=".3"
                        fill="green"
                      />
                      <Area
                        type="monotone"
                        dataKey="Bronze"
                        stroke="black"
                        strokeWidth="0"
                        fillOpacity=".3"
                        fill="black"
                      />
                    </AreaChart>}

              </ResponsiveContainer>
              </Grid.Row>
              <Grid.Row className="og-card">
              <Table basic='very'>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell><Icon  name='dot circle outline' color="red" />Story Volume Platinum</Table.Cell>
                      <Table.Cell>{platinum}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell><Icon  name='dot circle outline' color="blue" />Story Volume Gold</Table.Cell>
                      <Table.Cell>{gold}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell><Icon  name='dot circle outline' color="green" />Story Volume Silver</Table.Cell>
                      <Table.Cell>{silver}</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell><Icon  name='dot circle outline' color="black" />Story Volume Bronze</Table.Cell>
                      <Table.Cell>{bronze}</Table.Cell>
                    </Table.Row>
                  </Table.Body>
                  </Table>
              </Grid.Row>
            </Grid.Column>
    )
  }
}

export default AreaCard;
