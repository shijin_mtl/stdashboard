import React, { Component } from 'react'
import { Menu,Dropdown,Image } from 'semantic-ui-react'
import avatar from '../../public/img/avatar.png';





class SubHeader extends Component {
  componentDidMount() {
    if(this.props.path.includes("news")){
      this.setState({ activeItem: "News" })
    }
    if(this.props.path.includes("head"))
    {
      this.setState({ activeItem: "Head" })
    }
    else if (this.props.path.includes("social")) {
      this.setState({ activeItem: "Social Media" })
    }
    else if (this.props.path.includes("viral")) {
      this.setState({ activeItem: "Viral News" })
    }
    else {
      this.setState({ activeItem: "Total" })
    }
  }



  handleItemClick(e, { name }){
    this.setState({ activeItem: name })
    switch(name) {
    case "Total":
        this.props.urlChange("/")
        break;
    case "News":
          this.props.urlChange("/news/")
        break;
    case "Social Media":
        this.props.urlChange("/social/")
        break;
    case "Viral News":
        this.props.urlChange("/viral/")
        break;
    case "Digital News":
        this.props.urlChange("/digital/")
        break;
    default:
      break;
       }
  }

  render() {

    var friendOptions = [
    {
      text: 'Jenny 1',
      value: 'Jenny 1',
      image: { avatar: true, src: avatar },
    },
    {
      text: 'Jenny 2',
      value: 'Jenny 2',
      image: { avatar: true, src: avatar },
    },
    {
      text: 'Jenny 3',
      value: 'Jenny 3',
      image: { avatar: true, src: avatar },
    },
    {
      text: 'Jenny 4',
      value: 'Jenny 5',
      image: { avatar: true, src: avatar },
    },
    {
      text: 'Jenny 6',
      value: 'Jenny 6',
      image: { avatar: true, src: avatar },
    },
    {
      text: 'Jenny 6',
      value: 'Jenny 6',
      image: { avatar: true, src: avatar },
    },

  ]
   var subheader =""
    if(this.props.item === "dashboard"){
       subheader = (
        <Menu stackable >
          <Menu.Item name='Total' active={this.props.path === '/'} onClick={this.handleItemClick.bind(this)} />
          <Dropdown.Item name='News'  active={this.props.path.includes('news')} onClick={this.handleItemClick.bind(this)}>Newspaper</Dropdown.Item>
          <Dropdown.Item name='Viral News'  active={this.props.path.includes('viral')} onClick={this.handleItemClick.bind(this)}>Viral News</Dropdown.Item>
          <Dropdown.Item name='Digital News'  active={this.props.path.includes('digital')} onClick={this.handleItemClick.bind(this)}>Digital News</Dropdown.Item>
          <Menu.Item name='Social Media' active={this.props.path.includes('social')} onClick={this.handleItemClick.bind(this)} />
        </Menu>
      )
    }
    else if(this.props.item === "insights"){
       subheader = (
        <Menu>
           <Menu.Item className={"menu-width"}>
             <Dropdown placeholder='Select Competitors' fluid multiple search selection options={friendOptions} />
           </Menu.Item>
           <Menu.Item className={"img-a"}>
           <Image avatar src={avatar} />
           </Menu.Item>
           <Menu.Item className={"img-a"}>
           <Image avatar src={avatar} />
           </Menu.Item >
           <Menu.Item className={"img-a"}>
           <Image avatar src={avatar} />
           </Menu.Item>
           <Menu.Item className={"img-a"}>
           <Image avatar src={avatar} />
           </Menu.Item>
         </Menu>
      )
    }
    else if(this.props.item === "read" || this.props.item === "stories" || this.props.item === "head"){
       subheader =[]
    }
    else {
       subheader = (
        <Menu>
           <Menu.Item className={"menu-width"}>
             <Dropdown placeholder='Select Competitors' fluid multiple search selection options={friendOptions} />
           </Menu.Item>
           <Menu.Item className={"img-a"}>
           <Image avatar src={avatar} />
           </Menu.Item>
         </Menu>
       )
    }

    if(this.props.history.location.pathname.includes('/read/'))
    {
      subheader =[]
    }



    return (
      <div>
        {subheader}
      </div>
    )
  }
}

export default SubHeader;
