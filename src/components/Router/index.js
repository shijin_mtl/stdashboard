import React, { Component } from 'react'
import {
  Switch,
  Route,
} from 'react-router-dom'
import Header from '../Header';

import Total from '../Total';
import News from '../News'
import ReadNews from '../Read'
import HeadToHead from '../Compare'
import NotFound from '../404'
import Login from '../Login'
import SocialDash  from '../SocialDash'
import ViralNews from '../ViralNews'
import DigitalNews from '../DigitalNews'
import moment from 'moment'


class RouterConfig extends Component {
  constructor(props) {
    super(props)
    this.state = {
      date:{"start":moment().subtract(3, 'week').format('YYYY-MM-DD'),"end":moment().format('YYYY-MM-DD')},
    }
  }

  dateChange=(startDate, endDate )=>{
    if(startDate && endDate){
      this.setState({
        date:{"start":startDate.format('YYYY-MM-DD'),"end":endDate.format('YYYY-MM-DD')}
      })
    }
  }

  render() {
    return (
      <div>
      <Route render={(props) => <Header {...props} dateChange={this.dateChange} date={this.state.date} />}/>
      <Switch>
          <Route exact={true} path='/news/' render={(props) => <News {...props} date={this.state.date}/>}/>
          <Route exact={true} path='/read/:entity?/:source?/:type?/' render={(props) => <ReadNews {...props} date={this.state.date}/>}/>
          <Route exact={true} path='/login' render={(props) => <Login {...props} date={this.state.date}/>}/>
          <Route exact={true} path='/social/' render={(props) => <SocialDash {...props} date={this.state.date}/>} />
          <Route exact={true} path='/viral/'  render={(props) => <ViralNews {...props} date={this.state.date}/>}/>
          <Route exact={true} path='/digital/' render={(props) => <DigitalNews {...props} date={this.state.date}/>}/>
          <Route exact={true} path='/head-to-head/'  render={(props) => <HeadToHead {...props} date={this.state.date}/>}/>
          <Route exact={true} path='/'  render={(props) => <Total {...props} date={this.state.date}/>}/>
          <Route path='*' component={NotFound}/>
      </Switch>
      </div>
    )
  }
}

export default RouterConfig;
