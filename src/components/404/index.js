import React, { Component } from 'react'
import { Image,Grid } from 'semantic-ui-react'
import src from '../../public/img/404.jpeg';



class NotFound extends Component {
  render() {
    return (
      <Grid divided='vertically' stackable className="top-pad">
        <Grid.Row columns={2}>
          <Grid.Column width={7}>
          <div className="error-template">
                    <h1>
                        Oops!</h1>
                    <h2>
                        404 Not Found</h2>
                    <div className="error-details">
                        Sorry, an error has occured, Requested page not found!

                    </div>
                </div>
          </Grid.Column>
          <Grid.Column width={9}>
          <Image src={src} size='large' />
          </Grid.Column>
      </Grid.Row>
      </Grid>
    )
  }
}

export default NotFound;
