import React, { Component } from 'react'
import { Form,Grid,Button} from 'semantic-ui-react'
import { newsStore, newsFetch,entityFetch,sourceFetch } from '../../store/newsStore'
import Articles from './Article'
import InfiniteScroll from 'react-infinite-scroller'



class ReadNews extends Component {
  constructor(props) {
    super(props)
    this.store = newsStore()
    this.state = {
      loading: false,
      data:[],
      track: [],
      hasMoreItems:true,
      content:"",
      type:this.props.match.params.type==='3'?"viralnews":(this.props.match.params.type==='2'?'digital_news':"newspaper"),
      sort:"desc",
      entity:[],
      list_text:"",
      source_list:[],
      entity_value:this.props.match.params.entity?[parseInt(this.props.match.params.entity,10)]:[],
      source_value:this.props.match.params.source?[parseInt(this.props.match.params.source,10)]:[],
    }
  }



  componentDidMount() {
    this.store.subscribe(this.onFetchProcess.bind(this))
    this.store.dispatch(newsFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"content":this.state.content,"entity":this.state.entity_value,
    "sort":this.state.sort,"type":this.state.type,"start":0, "source":this.state.source_value}))
    this.store.dispatch(entityFetch())
    this.store.dispatch(sourceFetch())
  }



  componentWillReceiveProps(nextProps)
  {
      this.setState({
      date:nextProps.date,
      track: [],
      list_text:""
      })
      this.store.dispatch(newsFetch({ "start_date":nextProps.date.start,"end_date":nextProps.date.end,"content":this.state.content,"entity":this.state.entity_value,
      "sort":this.state.sort,"type":this.state.type,"start":0,"source":this.state.source_value}))
  }


  onFetchProcess = () => {
    const state = this.store.getState()
    if(state.success === true)
    {

      if(state.type === "entity")
      {
        var trim = JSON.parse(JSON.stringify(state.data).split('"id":').join('"value":').split('"name":').join('"text":'));
        this.setState({
          entity:trim
        })
      }

      if(state.type === "read")
      {
        this.setState({
          source_list:state.data.list
        })
      }


      if(state.type === "news")
      {
      var tracks=this.state.track


      if(state.data.articles)
      {
          state.data.articles.map((data) => {
                    tracks.push(data)
                    return true;
                });

        this.setState({
          track:tracks,
          hasMoreItems:true
          })

          if(state.data.articles.length === 0){
            this.setState({
              hasMoreItems:false,
              list_text:"No More Articles"
              })
          }

      }
      else
      {
        this.setState({
          hasMoreItems:false
        })
      }
    }
    }
  }


  loadItems(page)
  {
    setTimeout(function () {
      this.setState({
        hasMoreItems:false
      });
      setTimeout(function(){ this.store.dispatch(newsFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"content":this.state.content,"entity":this.state.entity_value,
      "sort":this.state.sort,"type":this.state.type,"start":page,"source":this.state.source_value}))}.bind(this), 1000);
    }.bind(this), 1000);
  }




  contentUpdate = (type,event) =>{

    this.setState({
          [type]:event.target.value
    })
  }



  handleChangeDrop = (key,e, { value }) =>{
    this.setState({ [key]: value })
  }



  searchClick=()=>{
    this.setState({
    track: [],
    list_text:""
    })
    this.store.dispatch(newsFetch({ "start_date":this.props.date.start,"end_date":this.props.date.end,"content":this.state.content,"entity":this.state.entity_value,
    "sort":this.state.sort,"type":this.state.type,"start":0,"source":this.state.source_value}))
  }


  render() {






    const loader = <div key={"load"}>Loading......</div>
    var article_list = []
    var duplicate = []



    this.state.track.map((data,index) => {
                          if(! duplicate.includes(data.id)){
                            article_list.push(<Articles data={data}/>)
                            return true;
                          }
                          return true;
                      });



    const type = [
      { key: 'newspaper', text: 'Newsaper', value: 'newspaper' },
      { key: 'digital_news', text: 'Digital News', value: 'digital_news' },
      { key: 'viralnews', text: 'Viral News', value: 'viralnews' },
    ]

    const date = [
      { key: 'ascending', text: 'Ascending', value: 'asc' },
      { key: 'descending', text: 'Descending', value: 'desc' },
    ]

    return (
      <div>
      <Grid stackable>
        <Grid.Column floated='centered' width={3} className={"filter-box"}>
        <Form>
          <Form.Group widths='equal'>
            <Form.Input id='form-subcomponent-shorthand-input-last-name' label='Search for' onChange={this.contentUpdate.bind(this,"content")} value={this.state.content}/>
          </Form.Group>
           <Form.Group widths='equal'>

             <Form.Dropdown placeholder='Select Entity' label='Entities' multiple fluid selection search  options={this.state.entity}
                  onChange={this.handleChangeDrop.bind(this,"entity_value")} value={this.state.entity_value}/>

           </Form.Group>

           <Form.Group widths='equal'>

             <Form.Dropdown placeholder='Select Source' label='Source' multiple fluid selection search  options={this.state.source_list}
                  onChange={this.handleChangeDrop.bind(this,"source_value")} value={this.state.source_value}/>
           </Form.Group>

           <Form.Group widths='equal'>
           <Form.Dropdown  label='News Type' fluid selection search  options={type}
                onChange={this.handleChangeDrop.bind(this,"type")} value={this.state.type}/>
           </Form.Group>
           <Form.Group widths='equal'>
           <Form.Dropdown  label='Date Sort'  fluid selection search  options={date}
                onChange={this.handleChangeDrop.bind(this,"sort")} value={this.state.sort}/>
           </Form.Group>
           <Button animated='fade' style={{'width':'100%'}} onClick={this.searchClick.bind(this)}>
            <Button.Content visible>
              Search
            </Button.Content>
            <Button.Content hidden>
              Search
            </Button.Content>
          </Button>
         </Form>
        </Grid.Column>
        <Grid.Column floated='centered' width={13}>
        <InfiniteScroll
            pageStart={0}
            loadMore={this.loadItems.bind(this)}
            hasMore={this.state.hasMoreItems}
            loader={loader}>
                {article_list}
      </InfiniteScroll>
      {this.state.list_text}
        </Grid.Column>
        </Grid>
      </div>
    )
  }
}

export default ReadNews;
