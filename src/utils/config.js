function getServerLocation(){
	return window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '') + '/';
}


// module.exports.numRound = function(labelValue)
// {
//     // Nine Zeroes for Billions
//     return Math.abs(Number(labelValue)) >= 1.0e+9
//
//     ? Math.abs(Number(labelValue)) / 1.0e+9 + "B"
//     // Six Zeroes for Millions
//     : Math.abs(Number(labelValue)) >= 1.0e+6
//
//     ? Math.abs(Number(labelValue)) / 1.0e+6 + "M"
//     // Three Zeroes for Thousands
//     : Math.abs(Number(labelValue)) >= 1.0e+3
//
//     ? Math.abs(Number(labelValue)) / 1.0e+3 + "K"
//
//     : Math.abs(Number(labelValue));
// }




module.exports = {
    server:"https://dashboard.staging.scoretrends.com/",
		interface:"https://interface.scoretrends.com/dash/api/v1/",
    id:null,
    ip:getServerLocation(),
		s3static:process.env.STATIC_URL,
		numRound:function(labelValue)
		{
		    // Nine Zeroes for Billions
		    return Math.abs(Number(labelValue)) >= 1.0e+9

		    ? Math.abs((Number(labelValue)) / 1.0e+9).toFixed(2) + "B"
		    // Six Zeroes for Millions
		    : Math.abs(Number(labelValue)) >= 1.0e+6

		    ? Math.abs((Number(labelValue)) / 1.0e+6).toFixed(2) + "M"
		    // Three Zeroes for Thousands
		    : Math.abs(Number(labelValue)) >= 1.0e+3

		    ? Math.abs((Number(labelValue)) / 1.0e+3).toFixed(2) + "K"

		    : Math.abs(Number(labelValue));
		}

}
