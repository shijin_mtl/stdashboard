export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILED = 'LOGIN_FAILED'
export const GOOGLE_REQUEST = 'GOOGLE_REQUEST'
export const GOOGLE_SUCCESS = 'GOOGLE_SUCCESS'
export const GOOGLE_FAILED = 'GOOGLE_FAILED'



export function loginRequest(text) {
  return { type: LOGIN_REQUEST, text }
}

export function loginSuccess(index) {
  return { type: LOGIN_SUCCESS, index }
}

export function loginFail(filter) {
  return { type: LOGIN_FAILED, filter }
}


export function googleRequest(text) {
  return { type: GOOGLE_REQUEST, text }
}

export function googleSuccess(index) {
  return { type: GOOGLE_SUCCESS, index }
}

export function googleFail(filter) {
  return { type: GOOGLE_FAILED, filter }
}
