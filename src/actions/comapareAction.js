export const SCORE_REQUEST = 'SCORE_REQUEST'
export const SCORE_SUCCESS = 'SCORE_SUCCESS'
export const SCORE_FAILED = 'SCORE_FAILED'



export function scoreRequest(text) {
  return { type: SCORE_REQUEST, text }
}

export function scoreSuccess(index) {
  return { type: SCORE_SUCCESS, index }
}

export function scoreFail(filter) {
  return { type: SCORE_FAILED, filter }
}
