export const SOURCE_REQUEST = 'SOURCE_REQUEST'
export const SOURCE_SUCCESS = 'SOURCE_SUCCESS'
export const SOURCE_FAILED = 'SOURCE_FAILED'

export const ARTICLE_REQUEST = 'ARTICLE_REQUEST'
export const ARTICLE_SUCCESS = 'ARTICLE_SUCCESS'
export const ARTICLE_FAILED = 'ARTICLE_FAILED'


export const MAIN_REQUEST = 'MAIN_REQUEST'
export const MAIN_SUCCESS = 'MAIN_SUCCESS'
export const MAIN_FAILED = 'MAIN_FAILED'

export const VOLUME_REQUEST = 'VOLUME_REQUEST'
export const VOLUME_SUCCESS = 'VOLUME_SUCCESS'
export const VOLUME_FAILED = 'VOLUME_FAILED'




export const VOLUME_REQUEST = 'S_VOLUME_REQUEST'
export const VOLUME_SUCCESS = 'S_VOLUME_SUCCESS'
export const VOLUME_FAILED = 'S_VOLUME_FAILED'





export function sourceRequest(text) {
  return { type: SOURCE_REQUEST, text }
}

export function sourceSuccess(index) {
  return { type: SOURCE_SUCCESS, index }
}

export function sourceFail(filter) {
  return { type: SOURCE_FAILED, filter }
}


export function articleRequest(text) {
  return { type: ARTICLE_REQUEST, text }
}

export function articleSuccess(index) {
  return { type: ARTICLE_SUCCESS, index }
}

export function articleFail(filter) {
  return { type: ARTICLE_FAILED, filter }
}


export function mainRequest(text) {
  return { type: MAIN_REQUEST, text }
}

export function mainSuccess(index) {
  return { type: MAIN_SUCCESS, index }
}

export function mainFail(filter) {
  return { type: MAIN_FAILED, filter }
}


export function volumeRequest(text) {
  return { type: VOLUME_REQUEST, text }
}

export function volumeSuccess(index) {
  return { type: VOLUME_SUCCESS, index }
}

export function volumeFail(filter) {
  return { type: VOLUME_FAILED, filter }
}


export function sourceVolumeRequest(text) {
  return { type: S_VOLUME_REQUEST, text }
}

export function sourceVolumeSuccess(index) {
  return { type: S_VOLUME_SUCCESS, index }
}

export function sourceVolumeFail(filter) {
  return { type: S_VOLUME_FAILED, filter }
}
