export const DATA_REQUEST = 'DATA_REQUEST'
export const DATA_SUCCESS = 'DATA_SUCCESS'
export const DATA_FAILED = 'DATA_FAILED'



export const FB_REQUEST = 'FB_REQUEST'
export const FB_SUCCESS = 'FB_SUCCESS'
export const FB_FAILED = 'FB_FAILED'


export const TW_REQUEST = 'TW_REQUEST'
export const TW_SUCCESS = 'TW_SUCCESS'
export const TW_FAILED = 'TW_FAILED'


export const NEWS_REQUEST = 'NEWS_REQUEST'
export const NEWS_SUCCESS = 'NEWS_SUCCESS'
export const NEWS_FAILED = 'NEWS_FAILED'





export function dataRequest(text) {
  return { type: DATA_REQUEST, text }
}

export function dataSuccess(index) {
  return { type: DATA_SUCCESS, index }
}

export function dataFail(filter) {
  return { type: DATA_FAILED, filter }
}




export function fbRequest(text) {
  return { type: FB_REQUEST, text }
}

export function fbSuccess(index) {
  return { type: FB_SUCCESS, index }
}

export function fbFail(filter) {
  return { type: FB_FAILED, filter }
}



export function twRequest(text) {
  return { type: TW_REQUEST, text }
}

export function twSuccess(index) {
  return { type: TW_SUCCESS, index }
}

export function twFail(filter) {
  return { type: TW_FAILED, filter }
}



export function newsRequest(text) {
  return { type: NEWS_REQUEST, text }
}

export function newsSuccess(index) {
  return { type: NEWS_SUCCESS, index }
}

export function newsFail(filter) {
  return { type: NEWS_FAILED, filter }
}
