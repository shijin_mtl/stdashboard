export const NEWS_REQUEST = 'NEWS_REQUEST'
export const NEWS_SUCCESS = 'NEWS_SUCCESS'
export const NEWS_FAILED = 'NEWS_FAILED'


export const ENTITY_REQUEST = 'ENTITY_REQUEST'
export const ENTITY_SUCCESS = 'ENTITY_SUCCESS'
export const ENTITY_FAILED = 'ENTITY_FAILED'



export const READ_REQUEST = 'READ_REQUEST'
export const READ_SUCCESS = 'READ_SUCCESS'
export const READ_FAILED = 'READ_FAILED'




export function newsRequest(text) {
  return { type: NEWS_REQUEST, text }
}

export function newsSuccess(index) {
  return { type: NEWS_SUCCESS, index }
}

export function newsFail(filter) {
  return { type: NEWS_FAILED, filter }
}


export function entityRequest(text) {
  return { type: ENTITY_REQUEST, text }
}

export function entitySuccess(index) {
  return { type: ENTITY_SUCCESS, index }
}

export function entityFail(filter) {
  return { type: ENTITY_FAILED, filter }
}


export function readRequest(text) {
  return { type: READ_REQUEST, text }
}

export function readSuccess(index) {
  return { type: READ_SUCCESS, index }
}

export function readFail(filter) {
  return { type: READ_FAILED, filter }
}
