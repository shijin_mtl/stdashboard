export const MAIN_REQUEST = 'MAIN_REQUEST'
export const MAIN_SUCCESS = 'MAIN_SUCCESS'
export const MAIN_FAILED = 'MAIN_FAILED'


export const DETAILL_REQUEST = 'DETAIL_REQUEST'
export const DETAIL_SUCCESS = 'DETAIL_SUCCESS'
export const DETAIL_FAILED = 'DETAIL_FAILED'


export const POST_REQUEST = 'POST_REQUEST'
export const POST_SUCCESS = 'POST_SUCCESS'
export const POST_FAILED = 'POST_FAILED'


export const SOCIAL_REQUEST = 'SOCIAL_REQUEST'
export const SOCIAL_SUCCESS = 'SOCIAL_SUCCESS'
export const SOCIAL_FAILED = 'SOCIAL_FAILED'



export function mainRequest(text) {
  return { type: MAIN_REQUEST, text }
}

export function mainSuccess(index) {
  return { type: MAIN_SUCCESS, index }
}

export function mainFail(filter) {
  return { type: MAIN_FAILED, filter }
}


export function detailRequest(text) {
  return { type: DETAIL_REQUEST, text }
}

export function detailSuccess(index) {
  return { type: DETAIL_SUCCESS, index }
}

export function detailFail(filter) {
  return { type: DETAIL_FAILED, filter }
}

export function postRequest(text) {
  return { type: POST_REQUEST, text }
}

export function postSuccess(index) {
  return { type: POST_SUCCESS, index }
}

export function postFail(filter) {
  return { type: POST_FAILED, filter }
}



export function socialRequest(text) {
  return { type: SOCIAL_REQUEST, text }
}

export function socialSuccess(index) {
  return { type: SOCIAL_SUCCESS, index }
}

export function socialFail(filter) {
  return { type: SOCIAL_FAILED, filter }
}
