import React from 'react';
import ReactDOM from 'react-dom';
import './public/semantic/dist/semantic.min.css';
import './public/css/app.css';
import 'react-dates/lib/css/_datepicker.css';
import SidebarLeftPush from './components/App';
import registerServiceWorker from './registerServiceWorker';
import 'react-dates/initialize';


ReactDOM.render(<SidebarLeftPush />, document.getElementById('root'));
registerServiceWorker();
