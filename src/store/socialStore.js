import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import socialReducer from '../reducers/socialReducer'
import axios from 'axios';
import config from '../utils/config';


const api = axios.create({
  timeout: 20000,
  withCredentials: true,
  headers:{
  	crossDomain:true
  }
});


export function mainFetch(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'MAIN_REQUEST'}, loadData))
    api.get(config.interface+"selected_scores/?nub_id="+localStorage.id+"&start_date="+data.start_date+"&end_date="+data.end_date+"&type="+data.type)
    .then(data => {
      dispatch(Object.assign({ type: 'MAIN_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'MAIN_FAILED',
        error: e
      }))
    })
  }
}

export function detailFetch(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'DETAIL_REQUEST'}, loadData))
    api.get(config.interface+"score_detail/?nub_id="+localStorage.id+"&start_date="+data.start_date+"&end_date="+data.end_date+"&type="+data.type)
    .then(data => {
      dispatch(Object.assign({ type: 'DETAIL_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'DETAIL_FAILED',
        error: e
      }))
    })
  }
}


export function postFetch(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'POST_REQUEST'}, loadData))
    api.get(config.server+"api/v1/social_posts/?nub_id="+localStorage.id+"&start_date="+data.start_date+"&end_date="+data.end_date+"&type="+data.type)
    .then(data => {
      dispatch(Object.assign({ type: 'POST_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'POST_FAILED',
        error: e
      }))
    })
  }
}

export function getSocial(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'SOCIAL_REQUEST'}, loadData))
    api.get('https://watchtower.staging.scoretrends.com/api/v1/post_all_details/?post_id=' + data.post)
    .then(data => {
      dispatch(Object.assign({ type: 'SOCIAL_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'SOCIAL_FAILED',
        error: e
      }))
    })
  }
}


export function socialStore() {
  let store = createStore(
    socialReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunkMiddleware)
  )
  window.ss = store
  return store
}
