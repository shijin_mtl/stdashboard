import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import newspaperReducer from '../reducers/digitalReducer'
import axios from 'axios';
import config from '../utils/config';


const api = axios.create({
  timeout: 15000,
  withCredentials: true,
  headers:{
  	crossDomain:true
  }
});



export function getNewsSource(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'SOURCE_REQUEST'}, loadData))
    api.get(config.server+'api/v1/top_source/?start_date='+data.start_date+"&end_date="+data.end_date+"&type="+data.type+"&page"+data.page)
    .then(data => {
      dispatch(Object.assign({ type: 'SOURCE_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'SOURCE_FAILED',
        error: e
      }))
    })
  }
}

export function getPeople(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'PEOPLE_REQUEST'}, loadData))
    api.get(config.server+'api/v1/associated_persons/?start_date='+data.start_date+"&end_date="+data.end_date)
    .then(data => {
      dispatch(Object.assign({ type: 'PEOPLE_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'PEOPLE_FAILED',
        error: e
      }))
    })
  }
}


export function getArticle(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'ARTICLE_REQUEST'}, loadData))
    api.get(config.server+'api/v1/news_articles/?start_date='+data.start_date+"&end_date="+data.end_date+"&type="+data.type+"&sort_field=article_score&sort=desc&page="+data.page)
    .then(data => {
      dispatch(Object.assign({ type: 'ARTICLE_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'ARTICLE_FAILED',
        error: e
      }))
    })
  }
}

export function getMain(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'MAIN_REQUEST'}, loadData))
    api.get(config.interface+"selected_scores/?nub_id="+localStorage.id+"&start_date="+data.start_date+"&end_date="+data.end_date+"&type="+data.type)
    .then(data => {
      dispatch(Object.assign({ type: 'MAIN_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'MAIN_FAILED',
        error: e
      }))
    })
  }
}

export function getVolume(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'VOLUME_REQUEST'}, loadData))
    api.get(config.interface+"score_detail/?nub_id="+localStorage.id+"&start_date="+data.start_date+"&end_date="+data.end_date+"&type="+data.type)
    .then(data => {
      dispatch(Object.assign({ type: 'VOLUME_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'VOLUME_FAILED',
        error: e
      }))
    })
  }
}

export function getSourceVolume(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'S_VOLUME_REQUEST'}, loadData))
    api.get(config.server+'api/v1/top_volume/?start_date='+data.start_date+"&end_date="+data.end_date+"&type="+data.type)
    .then(data => {
      dispatch(Object.assign({ type: 'S_VOLUME_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'S_VOLUME_FAILED',
        error: e
      }))
    })
  }
}








export function viralNewsStore() {
  let store = createStore(
    newspaperReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunkMiddleware)
  )
  window.ss = store
  return store
}
