import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import logoutReducer from '../reducers/logoutReducer'
import axios from 'axios';
import config from '../utils/config';



const loginApi = axios.create({
  timeout: 20000,
  withCredentials: true,
  headers:{
  	crossDomain:true
  }
});


export function logout() {
  return async function(dispatch) {
    loginApi.get(config.server+'logout/')
    .then(data => {
      dispatch(Object.assign({ type: 'LOGOUT_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'LOGOUT_FAILED',
        error: e
      }))
    })
  }
}


export function logoutStore() {
  let store = createStore(
    logoutReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunkMiddleware)
  )
  window.ss = store
  return store
}
