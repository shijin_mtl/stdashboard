import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import compareReducer from '../reducers/compareReducer'
import axios from 'axios';



const Api = axios.create({
  timeout: 20000,
  withCredentials: true,
  headers:{
  	crossDomain:true
  }
});


export function scoreFetch(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'SCORE_REQUEST'}, loadData))
    Api.get('https://interface.scoretrends.com/api/v1/score/?exact=true&chart=true&score_type=total&start_date='+data.start_date+'&end_date='+data.end_date+'&entity='+data.entity)
    .then(data => {
      dispatch(Object.assign({ type: 'SCORE_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'SCORE_FAILED',
        error: e
      }))
    })
  }
}



export function compareStore() {
  let store = createStore(
    compareReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunkMiddleware)
  )
  window.ss = store
  return store
}
