import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import newsReducer from '../reducers/newsReducer'
import axios from 'axios';
import config from '../utils/config';


const Api = axios.create({
  timeout: 20000,
  withCredentials: true,
  headers:{
  	crossDomain:true
  }
});



export function newsFetch(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'NEWS_REQUEST'}, loadData))
    Api.get(config.server+"api/v1/entity_news/?start_date="+data.start_date+"&end_date="+data.end_date+"&sort="+data.sort+"&type="+data.type+"&entity="+data.entity+
    "&content="+data.content+"&start="+data.start+"&source=" + data.source)
    .then(data => {
      dispatch(Object.assign({ type: 'NEWS_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'NEWS_FAILED',
        error: e
      }))
    })
  }
}


export function entityFetch(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'ENTITY_REQUEST'}, loadData))
    Api.get("https://interface.scoretrends.com/api/v1/entity/?nopaginate=true")
    .then(data => {
      dispatch(Object.assign({ type: 'ENTITY_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'ENTITY_FAILED',
        error: e
      }))
    })
  }
}


export function sourceFetch(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'READ_REQUEST'}, loadData))
    Api.get( config.server+"api/v1/source_list/")
    .then(data => {
      dispatch(Object.assign({ type: 'READ_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'READ_FAILED',
        error: e
      }))
    })
  }
}





export function newsStore() {
  let store = createStore(
    newsReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunkMiddleware)
  )
  window.ss = store
  return store
}
