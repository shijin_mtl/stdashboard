import { createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import loginReducer from "../reducers/loginReducer";
import axios from "axios";
import config from "../utils/config";

const loginApi = axios.create({
  timeout: 20000,
  withCredentials: true,
  headers: {
    crossDomain: true
  }
});

export function login(data) {
  return async function(dispatch) {
    let loadData = { success: false };
    dispatch(Object.assign({ type: "LOGIN_REQUEST" }, loadData));
    loginApi
      .post(config.server + "login/", data)
      .then(data => {
        dispatch(Object.assign({ type: "LOGIN_SUCCESS" }, data));
      })
      .catch(e => {
        dispatch(
          Object.assign({
            type: "LOGIN_FAILED",
            error: e
          })
        );
      });
  };
}

export function googlelogin(data) {
  return async function(dispatch) {
    let loadData = { success: false };
    dispatch(Object.assign({ type: "GOOGLE_REQUEST" }, loadData));
    loginApi
      .post(config.server + "api/v1/social/", data)
      .then(data => {
        dispatch(Object.assign({ type: "GOOGLE_SUCCESS" }, data));
      })
      .catch(e => {
        dispatch(
          Object.assign({
            type: "GOOGLE_FAILED",
            error: e
          })
        );
      });
  };
}

export function loginStore() {
  let store = createStore(
    loginReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
      window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunkMiddleware)
  );
  window.ss = store;
  return store;
}

