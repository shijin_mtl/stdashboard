import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import totalReducer from '../reducers/totalReducer'
import axios from 'axios';
import config from '../utils/config';



const loginApi = axios.create({
  timeout: 20000,
  withCredentials: true,
  headers:{
  	crossDomain:true
  }
});


export function totalFetch(data) {
  return async function(dispatch) {
    let id = data.id ?data.id:localStorage.id

    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'DATA_REQUEST'}, loadData))
    loginApi.get(config.interface+"scores/?start_date="+data.start_date+"&end_date="+data.end_date+"&nub_id="+id,data)
    .then(data => {
      dispatch(Object.assign({ type: 'DATA_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'DATA_FAILED',
        error: e
      }))
    })
  }
}

export function fbFetch(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'FB_REQUEST'}, loadData))
    var url = "https://interface.scoretrends.com/dash/api/v1/score_detail/?start_date="+data.start_date+"&end_date="+data.end_date+"&nub_id="+(data.id?data.id:localStorage.id)+"&type=facebook&formatted=true"

    if(data.multi){
      url = config.server+"api/v1/social_posts_multi/?start_date="+data.start_date+"&end_date="+data.end_date+"&type="+data.type+"&id="+(data.id?data.id:localStorage.id)
    }

    loginApi.get(url)
    .then(data => {
      dispatch(Object.assign({ type: 'FB_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'FB_FAILED',
        error: e
      }))
    })
  }
}


export function twFetch(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'TW_REQUEST'}, loadData))
    loginApi.get("https://interface.scoretrends.com/dash/api/v1/score_detail/?start_date="+data.start_date+"&end_date="+data.end_date+"&nub_id="+(data.id?data.id:localStorage.id)+"&type=twitter&formatted=true")
    .then(data => {
      dispatch(Object.assign({ type: 'TW_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'TW_FAILED',
        error: e
      }))
    })
  }
}


export function newsFetch(data) {
  return async function(dispatch) {
    let loadData ={'success':false}
    dispatch(Object.assign({ type: 'NEWS_REQUEST'}, loadData))
    loginApi.get("https://interface.scoretrends.com/dash/api/v1/score_detail/?start_date="+data.start_date+"&end_date="+data.end_date+"&nub_id="+localStorage.id+"&type="+data.type+"&formatted=true")
    .then(data => {
      dispatch(Object.assign({ type: 'NEWS_SUCCESS'}, data))
    })
    .catch(e => {
      dispatch(Object.assign({
        type: 'NEWS_FAILED',
        error: e
      }))
    })
  }
}








export function totalStore() {
  let store = createStore(
    totalReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunkMiddleware)
  )
  window.ss = store
  return store
}
